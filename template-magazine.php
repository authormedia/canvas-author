<?php
/**
 * Template Name: Magazine
 *
 * The magazine page template displays your posts with a "magazine"-style
 * content slider at the top and a grid of posts below it. 
 *
 * @package WooFramework
 * @subpackage Template
 */

 global $woo_options, $post; 
 get_header();

 if ( is_paged() ) $is_paged = true; else $is_paged = false; 

// the following line is not needed 
// $page_template = woo_get_page_template();
 

?>
    <!-- #content Starts -->
	<?php woo_content_before(); ?>
    <div id="content" class="col-full magazine"> 

   		<?php woo_homepage_blocks(); ?>
    
    	<div id="main-sidebar-container">

            <!-- #main Starts -->
            <?php woo_main_before(); ?>

            <div id="main">
			
				<?php if( $woo_options['woo_homeblog_enable'] == 'true' ) { ?>
						
				<?php // Add post more again
				add_action( 'woo_post_inside_after', 'woo_post_more' );	?>
	
				<?php get_template_part( 'loop', 'blog' ); ?>  
            </div><!-- /#main -->
			<?php } else { 
					echo '</div>';
				  }
			 ?>
		
			
          <?php woo_main_after(); ?>
    
          <?php // conditional sidebar
		  if( isset($woo_options['woo_homesidebar_enable']) && $woo_options['woo_homesidebar_enable'] == 'true' ) { 
			  get_sidebar();
		   }
		    ?>
            
		</div><!-- /#main-sidebar-container -->         

  

    </div><!-- /#content -->
	<?php woo_content_after(); ?>
    
		
<?php get_footer(); ?>