<?php
/*
 Altered copy of the theme options - by Jim Camomile
1. Many of the normal theme options from canvas have been removed. See the copy in Canvas for all the options that ship with Canvas
2. When updating the framework, check the change log for any significant changes to this file.

*/ 

if (!function_exists('woo_options')) {
function woo_options() {

global $userlevel;
//$userlevel = 4;


// THEME VARIABLES
$themename = "Authormedia";
$themeslug = "authormedia";

// STANDARD VARIABLES. DO NOT TOUCH!
$shortname = "woo";
$manualurl = 'http://www.woothemes.com/support/theme-documentation/'.$themeslug.'/';

//Access the WordPress Categories via an Array

$woo_categories = array();  
$woo_categories_obj = get_categories('hide_empty=0');
foreach ($woo_categories_obj as $woo_cat) {
    $woo_categories[$woo_cat->cat_ID] = $woo_cat->cat_name;}
$categories_tmp = array_unshift($woo_categories, "Select a category:");    

echo '
<style type="text/css">
#woo_container .controls .woo-radio-img-selected {
    border: 5px solid #FF88AA;
}
</style>
';

 
//Access the WordPress Pages via an Array
$woo_pages = array();
$woo_pages_obj = get_pages('sort_column=post_parent,menu_order');
foreach ($woo_pages_obj as $woo_page) {
    $woo_pages[$woo_page->ID] = $woo_page->post_name; }
$woo_pages_tmp = array_unshift($woo_pages, "Select a page:");


// Gravity forms list
//get all forms
if(class_exists('RGFormsModel')){
	$myforms = RGFormsModel::get_forms(1);
	$activeforms = array();
	$i=0;
	foreach ($myforms as $myform) {
		//loop through the forms and write out form title and id from gravity forms
		$activeforms[$myform->id] = $myform->title;
		//$i++;
	}
}


//Stylesheets Reader
$alt_stylesheet_path = TEMPLATEPATH . '/styles/';
$alt_stylesheets = array();

if ( is_dir($alt_stylesheet_path) ) {
    if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) { 
        while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) {
            if(stristr($alt_stylesheet_file, ".css") !== false) {
                $alt_stylesheets[] = $alt_stylesheet_file;
            }
        }    
    }
}


	
//More Options
$options_pixels = array();
$other_entries = array( __( 'Select a number:', 'woothemes' ), '0' );
$other_entries_2 = array( __( 'Select a number:', 'woothemes' ) );
$total_possible_numbers = intval( apply_filters( 'woo_total_possible_numbers', 20 ) );
for ( $i = 1; $i <= $total_possible_numbers; $i++ ) {
	$options_pixels[] = $i . 'px';
	$other_entries[] = $i;
	$other_entries_2[] = $i;
}

$tumblog_options = array( __( 'Disabled', 'woothemes' ), __( 'Before', 'woothemes' ), __( 'After', 'woothemes' ) );
$options_image_link_to = array( 'image' => __( 'The Image', 'woothemes' ), 'post' => __( 'The Post', 'woothemes' ) );

// Setup an array of slide-page terms for a dropdown.
/*$args = array( 'echo' => 0, 'hierarchical' => 1, 'taxonomy' => 'slide-page' );
$cats_dropdown = wp_dropdown_categories( $args );
$cats = array();

// Quick string hack to make sure we get the pages with the indents.
$cats_dropdown = str_replace( "<select name='cat' id='cat' class='postform' >", '', $cats_dropdown );
$cats_dropdown = str_replace( '</select>', '', $cats_dropdown );
$cats_split = explode( '</option>', $cats_dropdown );

$cats[] = __( 'Select a Slide Group:', 'woothemes' );

foreach ( $cats_split as $k => $v ) {
    $id = '';
    // Get the ID value.
    preg_match( '/value="(.*?)"/i', $v, $matches );

    if ( isset( $matches[1] ) ) {
        $id = $matches[1];
        $cats[$id] = trim( strip_tags( $v ) );
    }
}

$slide_groups = $cats;*/

// Below are the various theme options.
/* General Settings */

$options = array();   



/*************************/	
/*                       */
/*        Layout         */	
/*                       */
/*************************/		

$options[] = array( "name" => __( 'Layout', 'woothemes' ),
					"icon" => "layout",
					"type" => "heading");    


/****************************/	
/*                          */
/*          HEADER          */	
/*                          */
/****************************/	
					
$options[] = array( "name" => __( 'Header', 'woothemes' ),
					'type' => 'subheading' );
					
$options[] = array( "name" => __( 'Header Tracking Codes', 'woothemes' ),
					"desc" => __( 'Paste your Google Webmaster Tools, Bing Webmaster Tools (or other) codes that must be in the header here. This will be added into the header of your theme.', 'woothemes' ),
					"id" => $shortname."_header_codes",
					"std" => "",
					"type" => "textarea");   

$options[] = array( "name" => __( 'Display logo on header', 'woothemes' ),
					"desc" => __( 'if checked, you will be able to upload a logo for your site that will be displayed in the top-left of the site header/banner. ', 'woothemes' ),
					"id" => $shortname."_logo_show",
					"std" => "true",
					'class' => 'collapsed', 
					"type" => "checkbox");					

$options[] = array( "name" => __( 'Custom Logo', 'woothemes' ),
					"desc" => __( 'Upload a logo for your theme, or specify an image URL directly.', 'woothemes' ),
					"id" => $shortname."_logo",
					"std" => "",
					'class' => "hidden last", 
					"type" => "upload");

$options[] = array( "name" => __( 'Display Site Name on header', 'woothemes' ),
					"desc" => __( 'if checked, the website name (defined in settings >> general tab) will be displayed in the site header/banner. ', 'woothemes' ),
					"id" => $shortname."_sitename_show",
					"std" => "false",
					"type" => "checkbox");		
					
$options[] = array( "name" => __( 'Display Site Tagline (Description) on header', 'woothemes' ),
					"desc" => __( 'if checked, the website tagline (description) (defined in settings >> general tab) will be displayed in the site header/banner. ', 'woothemes' ),
					"id" => $shortname."_sitedescription_show",
					"std" => "false",
					"type" => "checkbox");				

					
$options[] = array( "name" => __( 'Custom Favicon', 'woothemes' ),
					"desc" => __( 'Upload a 16px x 16px Png/Gif image that will represent your website\'s favicon.', 'woothemes' ),
					"id" => $shortname."_custom_favicon",
					"std" => "",
					"type" => "upload");
									               

/****************************/	
/*                          */
/*          FOOTER          */	
/*                          */
/****************************/	
$child_images_dir =  get_stylesheet_directory_uri() . '/images/themestyles/';					

$options[] = array( 'name' => __( 'Footer', 'woothemes' ),
					'type' => 'subheading' );

$url =  get_template_directory_uri() . '/functions/images/';
$options[] = array( 'name' => __( 'Footer Widget Areas', 'woothemes' ),
    				'desc' => __( 'Select how many footer widget areas you want to display.', 'woothemes' ),
    				'id' => $shortname . '_footer_sidebars',
    				'std' => '3',
    				'type' => 'images',
    				'options' => array(
    					'0' => $url . 'layout-off.png',
    					'1' => $url . 'footer-widgets-1.png',
    					'2' => $url . 'footer-widgets-2.png',
    					'3' => $url . 'footer-widgets-3.png',
    					'4' => $url . 'footer-widgets-4.png' )
    				);
				
$options[] = array( "name" => __( 'Tracking Code', 'woothemes' ),
					"desc" => __( 'Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme.', 'woothemes' ),
					"id" => $shortname."_google_analytics",
					"std" => "",
					"type" => "textarea");   


$options[] = array( 'name' => __( 'Enable Footer Left', 'woothemes' ),
    				'desc' => __( 'Activate to add the custom text below to the theme footer.', 'woothemes' ),
    				'id' => $shortname . '_footer_left',
    				'std' => 'true',
    				'type' => 'checkbox' );

$options[] = array( 'name' => __( 'Custom Text Left', 'woothemes' ),
    				'desc' => __( 'Custom HTML and Text that will appear in the footer of your theme.', 'woothemes' ),
    				'id' => $shortname . '_footer_left_text',
    				'std' => 'All Rights Reserved. Site by <a target="_blank" href="http://www.authormedia.com"> Author Media</a>',
    				'type' => 'textarea' );
				
					
$options[] = array( 'name' => __( 'Enable Footer Right', 'woothemes' ),
    				'desc' => __( 'Activate to add the custom text below to the theme footer.', 'woothemes' ),
    				'id' => $shortname . '_footer_right',
    				'std' => 'false',
    				'type' => 'checkbox' );
					

$options[] = array( 'name' => __( 'Custom Text Right', 'woothemes' ),
    				'desc' => __( 'Custom HTML and Text that will appear in the footer of your theme.', 'woothemes' ),
    				'id' => $shortname . '_footer_right_text',
    				'std' => '',
    				'type' => 'textarea' );

/* Full width Layout */

$options[] = array( 'name' => __( 'Full Width Layout', 'woothemes' ),
					'type' => 'subheading' );

$options[] = array( "name" => __( 'Full Width Layout', 'woothemes' ),
					"desc" => "",
					"id" => $shortname."_full_width_notice",
					"std" => __( 'Below you can enable full width header and footer areas and set the background. You can set the styling options for the full width navigation under the Primary Navigation options. Please note that Boxed Layout must be disabled.', 'woothemes' ),
					"type" => "info");

$options[] = array( "name" => __( 'Enable Full Width Header', 'woothemes' ),
					"desc" => __( 'Set header container to display full width.', 'woothemes' ),
					"id" => $shortname."_header_full_width",
					"std" => "false",
					"type" => "checkbox" );

$options[] = array( "name" => __( 'Header Background Color', 'woothemes' ),
					"desc" => __( 'Select the background color you want for your full width header.', 'woothemes' ),
					"id" => $shortname."_full_header_full_width_bg",
					"std" => "",
					"type" => "color");

$options[] = array( "name" => __( 'Header Background Image', 'woothemes' ),
					"desc" => __( 'Upload a background image, or specify the image address of your image (http://yoursite.com/image.png). <br/>Image should be same width as your site width.', 'woothemes' ),
					"id" => $shortname."_full_header_bg_image",
					"std" => "",
					"type" => "upload");

$options[] = array( "name" => __( 'Header Background Image Repeat', 'woothemes' ),
					"desc" => __( 'Select how you want your background image to display.', 'woothemes' ),
					"id" => $shortname."_full_header_bg_image_repeat",
					"type" => "select",
					"options" => array("No Repeat" => "no-repeat", "Repeat" => "repeat","Repeat Horizontally" => "repeat-x", "Repeat Vertically" => "repeat-y",) );

$options[] = array( "name" => __( 'Enable Full Width Footer', 'woothemes' ),
					"desc" => __( 'Set footer widget area and footer container to display full width.', 'woothemes' ),
					"id" => $shortname."_footer_full_width",
					"std" => "false",
					"type" => "checkbox" );

$options[] = array( "name" => __( 'Footer Widget Area Background Color', 'woothemes' ),
					"desc" => __( 'Select the background color you want for your full width widget area.', 'woothemes' ),
					"id" => $shortname."_foot_full_width_widget_bg",
					"std" => "#f0f0f0",
					"type" => "color");

$options[] = array( "name" => __( 'Footer Background Color', 'woothemes' ),
					"desc" => __( 'Select the background color you want for your full width footer.', 'woothemes' ),
					"id" => $shortname."_footer_full_width_bg",
					"std" => "#222222",
					"type" => "color");


// General Layout
	
$options[] = array( 'name' => __( 'General Layout', 'woothemes' ),
					'type' => 'subheading' );
					
	
$options[] = array( "name" => __( 'Site Width', 'woothemes' ),
					"desc" => "Set the width (in px) that you would like your content column to be (recommended max-width is 1600px)",
					"id" => $shortname."_layout_width",
					"std" => "960",
					"min" => "600",
					"max" => "1600",
					"increment" => "10",
					"type" => 'slider' );     

	                                          
$images_dir =  get_template_directory_uri() . '/functions/images/';


$options[] = array( "name" => __( 'Main Layout', 'woothemes' ),
						"desc" => __( 'Select main content and sidebar alignment. Choose between 1, 2 or 3 column layout.', 'woothemes' ),
						"id" => $shortname . "_layout",
						"std" => "two-col-left",
						"type" => "images",
					"options" => array(
						'one-col' => $images_dir . '1c.png',
						'two-col-left' => $images_dir . '2cl.png',
						'two-col-right' => $images_dir . '2cr.png')
					);
						
$options[] = array( "name" => __( 'Display Breadcrumbs', 'woothemes' ),
					"desc" => __( 'Display dynamic breadcrumbs on each page of your website.', 'woothemes' ),
					"id" => $shortname."_breadcrumbs_show",
					"std" => "false",
					"class" => "collapsed",
					"type" => "checkbox");
					
$options[] = array( "name" => __( 'Breadcrumbs Type', 'woothemes' ),
					"desc" => __( 'Select which Breadcrumbs to Use. Note: Wordpress (Yoast) SEO Breadcrumbs tend to have more accurate breadcrumbs. To use Yoast Breadcrumbs, 1) Wordpress SEO by Yoast must be installed and activated and 2) Breadcrumbs must be activated in Wordpress SEO by going to admin menu [SEO] and selecting [Internal Links] and then check box [Enable Breadcrumbs]', 'woothemes' ),
					"id" => $shortname."_breadcrumbs_type",
					"std" => "alignleft",
					"type" => "radio",
					"class" => "hidden last",
					"options" => array("yoast" => "Wordpress (Yoast) SEO Breadcrumbs","woo" => "Woo Framework Breadcrumbs")); 					

/*	
		$options[] = array( "name" => __( 'Category Exclude - Homepage', 'woothemes' ),
							"desc" => __( 'Specify a comma seperated list of category IDs or slugs that you\'d like to exclude from your homepage (eg: uncategorized).', 'woothemes' ),
							"id" => $shortname."_exclude_cats_home",
							"std" => "",
							"type" => "text" );
		
		$options[] = array( "name" => __( 'Category Exclude - Blog Page Template', 'woothemes' ),
							"desc" => __( 'Specify a comma seperated list of category IDs or slugs that you\'d like to exclude from your \'Blog\' page template (eg: uncategorized).', 'woothemes' ),
							"id" => $shortname."_exclude_cats_blog",
							"std" => "",
							"type" => "text" );
*/		
	
	$options[] = array( "name" => __( 'Post/Page Comments', 'woothemes' ),
						"desc" => __( 'Select if you want to comments on posts and/or pages.', 'woothemes' ),
						"id" => $shortname."_comments",
						"type" => "select2",
						"options" => array( "post" => __( 'Posts Only', 'woothemes' ), "page" => __( 'Pages Only', 'woothemes' ), "both" => __( 'Pages / Posts', 'woothemes' ), "none" => __( 'None', 'woothemes' ) ) );                                                          



					    										
/*************************/	
/*                       */
/*   HOMEPAGE SETTING    */	
/*                       */
/*************************/	
	
$options[] = array( "name" => __( 'Homepage Settings', 'woothemes' ),
					"icon" => "homepage",
                    "type" => "heading");

/* Homepage Slides */
	$options[] = array( 'name' => __( 'Featured Slider', 'woothemes' ),
					'type' => 'subheading' );
	
	$options[] = array( "name" => __( 'Featured Slider', 'woothemes' ),
					"desc" => "",
					"id" => $shortname."_homeslides_info",
					"std" => __( 'The Slides and Controls for the Slider are in the left menu: "Slideshows".', 'woothemes' ),
					"type" => "info");	

	$options[] = array( "name" => __( 'Enable Featured Slider', 'woothemes' ),
						"desc" => __( 'Enable the featured slider.', 'woothemes' ),
						"id" => $shortname."_homeslides_enable",
						"std" => "false",
						"type" => "checkbox");
						
$options[] = array( "name" => __( 'Featured Slider Priority', 'woothemes' ),
					"desc" => __( 'The lower the number, the higher up on the page this box will load.', 'woothemes' ),
					"id" => $shortname."_homeslides_priority",
					"type" => "select2",
					"options" => array("10" => __( 'Priority 1', 'woothemes' ),"20" => __( 'Priority 2', 'woothemes' ),"30" => __( 'Priority 3', 'woothemes' ), "40" => __( 'Priority 4', 'woothemes' ), "50" => __( 'Priority 5', 'woothemes' ), "60" => __( 'Priority 6', 'woothemes' ) ) );	 
					
$options[] = array( "name" => __( 'Featured Slider Type', 'woothemes' ),
					"desc" => __( 'Choose between two types of sliders. If using the first option, use the editor of the slide to create content and layout. If using the second option, use the custom fields to add editor and text', 'woothemes' ),
					"id" => $shortname."_homeslides_type",
					"type" => "select2",
					"options" => array("normal" => __( 'Arrange Slide in Editor', 'woothemes' ),"overlay" => __( 'Image is Background and text is overlay', 'woothemes' ) ) );		 
						
				
/* Tagline Banner */
$options[] = array( 'name' => __( 'Homepage Tagline Banner', 'woothemes' ),
					'type' => 'subheading' );
					
					
$options[] = array( "name" => __( 'Enable Tagline Banner', 'woothemes' ),
					"desc" => __( 'Enable the Homepage Intro. ', 'woothemes' ),
					"id" => $shortname."_hometagline_enable",
					"std" => "false",
					"class" => "collapsed",
					"type" => "checkbox");
					
$options[] = array( "name" => __( 'Homepage Tagline Banner Priority', 'woothemes' ),
					"desc" => __( 'The lower the number, the higher up on the page this box will load.', 'woothemes' ),
					"id" => $shortname."_hometagline_priority",
					"type" => "select2",
					"options" => array("10" => __( 'Priority 1', 'woothemes' ),"20" => __( 'Priority 2', 'woothemes' ),"30" => __( 'Priority 3', 'woothemes' ), "40" => __( 'Priority 4', 'woothemes' ), "50" => __( 'Priority 5', 'woothemes' ), "60" => __( 'Priority 6', 'woothemes' ) ) );	
					
$options[] = array( "name" => __( 'Homepage Tagline Banner Title', 'woothemes' ),
					"desc" => __( 'Write a catchy tag line title (optional)', 'woothemes' ),
					"id" => $shortname."_hometagline_title",
					"std" => "",
					"class" => "hidden",
					"type" => "text");	
					
$options[] = array( "name" => __( 'Homepage Tagline Banner Text', 'woothemes' ),
					"desc" => __( 'Write a catchy tag line to use as your tagline banner', 'woothemes' ),
					"id" => $shortname."_hometagline_text",
					"std" => "",
					"class" => "hidden",
					"type" => "textarea");	

/* CTA Banner */
$options[] = array( 'name' => __( 'Homepage CTA Banner', 'woothemes' ),
					'type' => 'subheading' );
					
					
$options[] = array( "name" => __( 'Enable CTA Banner', 'woothemes' ),
					"desc" => __( 'Enable the Homepage CTA Banner. ', 'woothemes' ),
					"id" => $shortname."_homecta_enable",
					"std" => "false",
					"class" => "collapsed",
					"type" => "checkbox");
					
$options[] = array( "name" => __( 'Homepage CTA Banner Priority', 'woothemes' ),
					"desc" => __( 'The lower the number, the higher up on the page this box will load.', 'woothemes' ),
					"id" => $shortname."_homecta_priority",
					"type" => "select2",
					"options" => array("10" => __( 'Priority 1', 'woothemes' ),"20" => __( 'Priority 2', 'woothemes' ),"30" => __( 'Priority 3', 'woothemes' ), "40" => __( 'Priority 4', 'woothemes' ), "50" => __( 'Priority 5', 'woothemes' ), "60" => __( 'Priority 6', 'woothemes' ) ) );	
					
$options[] = array( "name" => __( 'Homepage CTA Banner Image', 'woothemes' ),
					"desc" => __( '"Upload an image to use as the background of the CTA Banner.  For best results, make the image the same width in pixels as the total width of the site."', 'woothemes' ),
					"id" => $shortname."_homecta_image",
					"std" => "",
					"class" => "hidden",
					"type" => "upload");


$options[] = array( "name" => __( 'Homepage CTA Banner Title', 'woothemes' ),
					"desc" => __( 'Optional: Write a catchy tag line to use as your CTA banner', 'woothemes' ),
					"id" => $shortname."_homecta_title",
					"std" => "",
					"class" => "hidden",
					"type" => "text");	
					
$options[] = array( "name" => __( 'Homepage CTA Banner Message', 'woothemes' ),
					"desc" => __( 'Optional: Write the main message for your CTA banner', 'woothemes' ),
					"id" => $shortname."_homecta_message",
					"std" => "",
					"class" => "hidden fullwidth",
					"type" => "textarea");
								
$options[] = array( "name" => __( 'Select Homepage CTA Banner link', 'woothemes' ),
					"desc" => __( 'select the page you want to link to for this Call to Action', 'woothemes' ),
					"id" => $shortname."_homecta_link",
					"std" => "",
					"class" => "hidden",
					"type" => "text");					
					

$options[] = array( "name" => __( 'Homepage CTA Button Text', 'woothemes' ),
					"desc" => __( 'What the CTA Button should say', 'woothemes' ),
					"id" => $shortname."_homecta_buttontext",
					"std" => "",
					"class" => "hidden",
					"type" => "text");
					
			
/*intro */					
$options[] = array( 'name' => __( 'Homepage Intro', 'woothemes' ),
					'type' => 'subheading' );
					
					
$options[] = array( "name" => __( 'Enable Intro', 'woothemes' ),
					"desc" => __( 'Enable the Homepage Intro. ', 'woothemes' ),
					"id" => $shortname."_homeintro_enable",
					"std" => "false",
					"class" => "collapsed",
					"type" => "checkbox");
					
$options[] = array( "name" => __( 'Homepage Intro Priority', 'woothemes' ),
					"desc" => __( 'The lower the number, the higher up on the page this box will load.', 'woothemes' ),
					"id" => $shortname."_homeintro_priority",
					"type" => "select2",
					"options" => array("10" => __( 'Priority 1', 'woothemes' ),"20" => __( 'Priority 2', 'woothemes' ),"30" => __( 'Priority 3', 'woothemes' ), "40" => __( 'Priority 4', 'woothemes' ), "50" => __( 'Priority 5', 'woothemes' ), "60" => __( 'Priority 6', 'woothemes' ) ) );	
					

$options[] = array( "name" => __( 'Homepage Heading', 'woothemes' ),
					"desc" => __( 'Write a catchy tag line to show up before the Homepage Intro', 'woothemes' ),
					"id" => $shortname."_homeintro_title",
					"std" => "",
					"class" => "hidden",
					"type" => "text");	
					
					
$options[] = array( "name" => __( 'Intro Image', 'woothemes' ),
					"desc" => __( '"Upload an image to go in the top left of the homepage intro.  For best results, make the image between 250 pixels and 350 pixels wide."', 'woothemes' ),
					"id" => $shortname."_homeintro_image",
					"std" => "",
					"class" => "hidden",
					"type" => "upload");
					
$options[] = array( "name" => __( 'Intro Image Size', 'woothemes' ),
					"desc" => __( 'Select the size you prefer for the homepage intro image.', 'woothemes' ),
					"id" => $shortname."_homeintro_image_size",
					"class" => "hidden",
					"type" => "select2",
					"options" => array("" => __( 'do not resize', 'woothemes' ), "200" => __( 'Small (200px wide)', 'woothemes' ), "250" => __( 'Small-ish (250px wide)', 'woothemes' ), "300" => __( 'Medium (300px wide)', 'woothemes' ), "350" => __( 'Large (350px wide)', 'woothemes' ) ) );					    												


$options[] = array( "name" => __( 'Home Intro Text', 'woothemes' ),
                    "desc" => __( '', 'woothemes' ),
                    "id" => $shortname."_authorpubbox_content",
                    "std" => "",
					"class" => "hidden fullwidth",
                    "type" => "textarea");

/* Homepage Video  */					
$options[] = array( 'name' => __( 'Homepage Video', 'woothemes' ),
					'type' => 'subheading' );
					
					
$options[] = array( "name" => __( 'Enable Homepage Video', 'woothemes' ),
					"desc" => __( 'Enable the Homepage Video. ', 'woothemes' ),
					"id" => $shortname."_homevideo_enable",
					"std" => "false",
					"class" => "collapsed",
					"type" => "checkbox");

$options[] = array( "name" => __( 'Homepage Video Priority', 'woothemes' ),
					"desc" => __( 'The lower the number, the higher up on the page this box will load.', 'woothemes' ),
					"id" => $shortname."_homevideo_priority",
					"type" => "select2",
					"options" => array("10" => __( 'Priority 1', 'woothemes' ),"20" => __( 'Priority 2', 'woothemes' ),"30" => __( 'Priority 3', 'woothemes' ), "40" => __( 'Priority 4', 'woothemes' ), "50" => __( 'Priority 5', 'woothemes' ), "60" => __( 'Priority 6', 'woothemes' ) ) );	

$options[] = array( "name" => __( 'Home Intro Video', 'woothemes' ),
					"desc" => __( 'paste in iframe for video', 'woothemes' ),
					"id" => $shortname."_home_video",
					"std" => "",
					"class" => "hidden last",
					"type" => "textarea");



/* Homepage Mini Features */

	$options[] = array( 'name' => __( 'Homepage Mini Features', 'woothemes' ),
						'type' => 'subheading' );
						
	
	$options[] = array( "name" => __( 'Enable Homepage Feature Boxes', 'woothemes' ),
						"desc" => __( 'Check the box to enable the 1-3 min--features which will appear on the homepage.', 'woothemes' ),
						"id" => $shortname."_featureboxes_enable",
						"std" => "false",
						"class" => "collapsed",
						"type" => "checkbox");   
						
	$options[] = array( "name" => __( 'Homepage Feature Boxes Priority', 'woothemes' ),
						"desc" => __( 'The lower the number, the higher up on the page this box will load.', 'woothemes' ),
						"id" => $shortname."_featureboxes_priority",
						"type" => "select2",
						"options" => array("10" => __( 'Priority 1', 'woothemes' ),"20" => __( 'Priority 2', 'woothemes' ),"30" => __( 'Priority 3', 'woothemes' ), "40" => __( 'Priority 4', 'woothemes' ), "50" => __( 'Priority 5', 'woothemes' ), "60" => __( 'Priority 6', 'woothemes' ) ) );	
				

	$options[] = array( "name" => __( 'Home Features Section Heading', 'woothemes' ),
						"desc" => __( '(optional) Title that shows above the block of feature boxes', 'woothemes' ),
						"id" => $shortname."_featureboxes_title",
						"std" => "",
						"class" => "hidden",
						"type" => "text");	

	
	/* Feature 1 */			 					
	
	$options[] = array( "name" => __( 'Home Feature 1', 'woothemes' ),
						"desc" => "",
						"id" => $shortname."_homefeature1_info",
						"std" => __( 'Add your info for your #1 Feature below.', 'woothemes' ),
						"class" => "hidden",
						"type" => "info");	
						
	$options[] = array( "name" => __( 'Home Feature 1 Heading', 'woothemes' ),
						"desc" => __( 'Write a catchy tag line to show up before the Homepage Feature 1', 'woothemes' ),
						"id" => $shortname."_feature1_title",
						"std" => "",
						"class" => "hidden",
						"type" => "text");	
						
						
	$options[] = array( "name" => __( 'Feature 1 Image', 'woothemes' ),
						"desc" => __( 'Upload and image to go in the top of the feature 1 box', 'woothemes' ),
						"id" => $shortname."_feature1_image",
						"std" => "",
						"class" => "hidden",
						"type" => "upload");    					
											
	$options[] = array( "name" => __( 'Home Feature 1 Text', 'woothemes' ),
						"desc" => __( 'The Body of feature 1. Keep this to 200 words or less.', 'woothemes' ),
						"id" => $shortname."_feature1_content",
						"std" => "",
						"class" => "hidden fullwidth",
						"type" => "textarea");
	
						
	$options[] = array( "name" => __( 'Home Feature 1 Link URL', 'woothemes' ),
						"desc" => __( 'put in a link to a page or category for Feature 1 (e.g. /about-us/)', 'woothemes' ),
						"id" => $shortname."_feature1_link",
						"std" => "",
						"class" => "hidden",
						"type" => "text");	

	$options[] = array( "name" => __( 'Home Feature 1 Link Text', 'woothemes' ),
						"desc" => __( 'Enter the button text for the Feature 1 link', 'woothemes' ),
						"id" => $shortname."_feature1_linktext",
						"std" => "",
						"class" => "hidden",
						"type" => "text");	
																
	/* Feature 2 */  	
	
	$options[] = array( "name" => __( 'Home Feature 2', 'woothemes' ),
						"desc" => "",
						"id" => $shortname."_homefeature2_info",
						"std" => __( 'Add your info for your #2 Feature below.', 'woothemes' ),
						"class" => "hidden",
						"type" => "info");	
						
	$options[] = array( "name" => __( 'Home Feature 2 Heading', 'woothemes' ),
						"desc" => __( 'Write a catchy tag line to show up before the Homepage Feature 2', 'woothemes' ),
						"id" => $shortname."_feature2_title",
						"std" => "",
						"class" => "hidden",
						"type" => "text");	


	$options[] = array( "name" => __( 'Feature 2 Image', 'woothemes' ),
						"desc" => __( 'Upload and image to go in the top of the feature 2 box', 'woothemes' ),
						"id" => $shortname."_feature2_image",
						"std" => "",
						"class" => "hidden",
						"type" => "upload");    					
											
	$options[] = array( "name" => __( 'Home Feature 2 Text', 'woothemes' ),
						"desc" => __( 'The Body of feature 2. Keep this to 200 words or less.', 'woothemes' ),
						"id" => $shortname."_feature2_content",
						"std" => "",
						"class" => "hidden fullwidth",
						"type" => "textarea");
						
						
	$options[] = array( "name" => __( 'Home Feature 2 Link URL', 'woothemes' ),
						"desc" => __( 'put in a link to a page or category for Feature 2 (e.g. /about-us/)', 'woothemes' ),
						"id" => $shortname."_feature2_link",
						"std" => "",
						"class" => "hidden",
						"type" => "text");	

	$options[] = array( "name" => __( 'Home Feature 2 Link Text', 'woothemes' ),
						"desc" => __( 'Enter the button text for the Feature 2 link', 'woothemes' ),
						"id" => $shortname."_feature2_linktext",
						"std" => "",
						"class" => "hidden",
						"type" => "text");	

	
	/* Feature 3 */				
	
	$options[] = array( "name" => __( 'Home Feature 3', 'woothemes' ),
						"desc" => "",
						"id" => $shortname."_homefeature3_info",
						"std" => __( 'Add your info for your #3 Feature below.', 'woothemes' ),
						"class" => "hidden",
						"type" => "info");	
						
	$options[] = array( "name" => __( 'Home Feature 3 Heading', 'woothemes' ),
						"desc" => __( 'Write a catchy tag line to show up before the Homepage Feature 3', 'woothemes' ),
						"id" => $shortname."_feature3_title",
						"std" => "",
						"class" => "hidden",

						"type" => "text");	
					
	$options[] = array( "name" => __( 'Feature 3 Image', 'woothemes' ),
						"desc" => __( 'Upload and image to go in the top of the feature 3 box', 'woothemes' ),
						"id" => $shortname."_feature3_image",
						"std" => "",
						"class" => "hidden",
						"type" => "upload");    					
											
	$options[] = array( "name" => __( 'Home Feature 3 Text', 'woothemes' ),
						"desc" => __( 'The Body of feature 3. Keep this to 200 words or less.', 'woothemes' ),
						"id" => $shortname."_feature3_content",
						"std" => "",
						"class" => "hidden fullwidth",
						"type" => "textarea");
						
					
	$options[] = array( "name" => __( 'Home Feature 3 Link URL', 'woothemes' ),
						"desc" => __( 'put in a link to a page or category for Feature 3 (e.g. /about-us/)', 'woothemes' ),
						"id" => $shortname."_feature3_link",
						"std" => "",
						"class" => "hidden",
						"type" => "text");	

	$options[] = array( "name" => __( 'Home Feature 3 Link Text', 'woothemes' ),
						"desc" => __( 'Enter the button text for the Feature 3 link', 'woothemes' ),
						"id" => $shortname."_feature3_linktext",
						"std" => "",
						"class" => "hidden",
						"type" => "text");	

						
/* Homepage widgets */

	$options[] = array( 'name' => __( 'Homepage Widgets', 'woothemes' ),
						'type' => 'subheading' ); 

	$options[] = array( "name" => __( 'Enable Homepage Widgets', 'woothemes' ),
						"desc" => "",
						"id" => $shortname."_homewidgets_info",
						"std" => __( 'Add a box to show 3 Widget columns on your homepage.', 'woothemes' ),
						"type" => "info");	

					
	$options[] = array( "name" => __( 'Enable Homepage Widgets', 'woothemes' ),
						"desc" => __( 'Check the box to enable the 3 homepage widget sections. The Widgets can be configured in the Appeaance >> Widgets section of admin.', 'woothemes' ),
						"id" => $shortname."_homewidgets_enable",
						"std" => "false",
						"type" => "checkbox");   		
					 


	$options[] = array( "name" => __( 'Homepage Widgets Priority', 'woothemes' ),
						"desc" => __( 'The lower the number, the higher up on the page this box will load.', 'woothemes' ),
						"id" => $shortname."_homewidgets_priority",
						"type" => "select2",
						"options" => array("10" => __( 'Priority 1', 'woothemes' ),"20" => __( 'Priority 2', 'woothemes' ),"30" => __( 'Priority 3', 'woothemes' ), "40" => __( 'Priority 4', 'woothemes' ), "50" => __( 'Priority 5', 'woothemes' ), "60" => __( 'Priority 6', 'woothemes' ) ) );	

				

/*     Homepage Blog     */	
										
	$options[] = array( 'name' => __( 'Homepage Blog', 'woothemes' ),
						'type' => 'subheading' );
						
	$options[] = array( "name" => __( 'About Homepage Blog Features', 'woothemes' ),
						"desc" => "",
						"id" => $shortname."_about_homeblog",
						"std" => __( 'You can display blog posts on your homepage. If you are also showing a homepage intro, it will show on the page first, then the blog posts. Below, you can select the number of posts to show, which category to choose from and a few other settings', 'woothemes' ),
						"type" => "info");	
											
	$options[] = array( "name" => __( 'Enable Blog on Homepage', 'woothemes' ),
						"desc" => __( 'Check the box to enable the blog posts on the homepage. If disabled they will not appear.', 'woothemes' ),
						"id" => $shortname."_homeblog_enable",
						"std" => "true",
						"class" => "collapsed",
						"type" => "checkbox");   
						
	$per_page = array();
	for ( $i = 1; $i <= 20; $i++ ) {
		$per_page[] = $i;
	}
		
	$options[] = array( "name" => __( 'Number of Posts To Display', 'woothemes' ),
						"desc" => __( 'The number of featured posts to display on the blog homepage.', 'woothemes' ),
						"id" => $shortname."_magazine_limit",
						"std" => get_option( 'posts_per_page' ),
						"type" => "select",
						"class" => "hidden",
						"options" => $per_page );						
						
						

/*     Homepage Sidebar     */	
										
$options[] = array( 'name' => __( 'Homepage Sidebar', 'woothemes' ),
					'type' => 'subheading' );
						
$options[] = array( "name" => __( 'About Homepage Sidebar', 'woothemes' ),
					"desc" => "",
					"id" => $shortname."_about_homesidebar",
					"std" => __( 'You can have a sidebar on the home page, but it may not work well with some other homepage features. Try and experiment. The sidebar will always load beneath the featured slider, if that is active. ', 'woothemes' ),
					"type" => "info");	
											
$options[] = array( "name" => __( 'Enable Sidebar on Homepage', 'woothemes' ),
					"desc" => __( 'Check the box to enable the sidebar on the homepage. If disabled they will not appear.', 'woothemes' ),
					"id" => $shortname."_homesidebar_enable",
					"std" => "true",
					"class" => "collapsed",
					"type" => "checkbox");   									
					
$options[] = array( "name" => __( 'Homepage Sidebar to Use', 'woothemes' ),
					"desc" => __( 'Choose which sidebar to use on the homepage', 'woothemes' ),
					"id" => $shortname."_homesidebar_choice",
					"class" => "hidden last",
					"type" => "select2",
					"options" => array("primary" => __( 'Primary Sidebar', 'woothemes' ),"homepage" => __( 'Homepage Only Sidebar', 'woothemes' )) );	
/*
$options[] = array( "name" => __( 'Homepage Sidebar Position', 'woothemes' ),
					"desc" => __( 'Choose where to place sidebar on the homepage', 'woothemes' ),
					"id" => $shortname."_homesidebar_placement",
					"class" => "hidden last",
					"type" => "select2",
					"options" => array("full" => __( 'Full Height Sidebar', 'woothemes' ),"partial" => __( 'Partial Sidebar', 'woothemes' ),"blog" => __( 'Blog Only Sidebar', 'woothemes' ) ) );	
*/
	
/****************************/	
/*                          */
/*        BLOG POSTS        */	
/*                          */
/****************************/	

	
	$options[] = array( "name" => __( 'Blog Posts', 'woothemes' ),
						"icon" => "post",
						"type" => "heading");
		
	$options[] = array( 'name' => __( 'Blog Setup', 'woothemes' ),
					'type' => 'subheading' );

		
	$per_page = array();
	for ( $i = 1; $i <= 20; $i++ ) {
		$per_page[] = $i;
	}
		
	$options[] = array( "name" => __( 'Number of Posts To Display', 'woothemes' ),
						"desc" => __( 'The number of featured posts to display on the blog homepage.', 'woothemes' ),
						"id" => $shortname."_blog_limit",
						"std" => get_option( 'posts_per_page' ),
						"type" => "select",
						"options" => $per_page );

			
	$options[] = array( "name" => __( 'Post Content', 'woothemes' ),
						"desc" => __( 'Select if you want to show the full content or the excerpt on posts.', 'woothemes' ),
						"id" => $shortname."_post_content",
						"type" => "select2",
						"options" => array("excerpt" => __( 'The Excerpt', 'woothemes' ), "content" => __( 'Full Content', 'woothemes' ) ) );                                                          

						
	$options[] = array( "name" => __( 'Archives Pagination Style', 'woothemes' ),
						"desc" => __( 'Select the style of paging links you would like to use on the blog. These are at the bottom of blog archives when there is more than 1 page of excerpts.', 'woothemes' ),
						"id" => $shortname."_pagination_type",
						"type" => "select2",
						"options" => array("paginated_links" => __( 'Numbers', 'woothemes' ), "simple" => __( 'Next/Previous', 'woothemes' ) ) );
						
	$options[] = array( "name" => __( 'Read More Label', 'woothemes' ),
					"desc" => __( 'what to say after a post excerpt..', 'woothemes' ),
					"id" => $shortname."_postmeta_readmore",
					"std" => "Read More",
					"type" => "text");	
					
	$options[] = array( "name" => __( 'Show the full content of the first post in an archive', 'woothemes' ),
					"desc" => __( 'Clicking this will make the first post in any list of posts appear with the full content rather than an excerpt.', 'woothemes' ),
					"id" => $shortname."_mullet_enable",
					"std" => "false",
					"type" => "checkbox");				
	
	$options[] = array( "name" => __( 'Disable Post Author', 'woothemes' ),
					"desc" => __( 'Disable post author below post?', 'woothemes' ),
					"id" => $shortname."_disable_post_author",
					"std" => "true",
					"type" => "checkbox");    

$options[] = array( 'name' => __( 'Single Post Arrangement', 'woothemes' ),
					'type' => 'subheading' ); 

$options[] = array( "name" => __( 'Arranging the Single Post Elements', 'woothemes' ),
					"desc" => "",
					"id" => $shortname."_post_arrangement",
					"std" => __( 'Change the order of where Post title, image and meta display by changing the priority of each below', 'woothemes' ),
					"type" => "info");	


$options[] = array( "name" => __( 'Single Post Title Priority', 'woothemes' ),
					"desc" => __( 'The lower the number, the higher up on the post this element will load.', 'woothemes' ),
					"id" => $shortname."_posttitle_priority",
					"type" => "select2",
					"options" => array("10" => __( 'Priority 1', 'woothemes' ),"20" => __( 'Priority 2', 'woothemes' ),"30" => __( 'Priority 3', 'woothemes' ), "40" => __( 'Priority 4', 'woothemes' ), "50" => __( 'Priority 5', 'woothemes' ), "60" => __( 'Priority 6', 'woothemes' ) ) );	 
					
$options[] = array( "name" => __( 'Single Post Image Priority', 'woothemes' ),
					"desc" => __( 'The lower the number, the higher up on the post this element will load.', 'woothemes' ),
					"id" => $shortname."_postimage_priority",
					"type" => "select2",
					"options" => array("10" => __( 'Priority 1', 'woothemes' ),"20" => __( 'Priority 2', 'woothemes' ),"30" => __( 'Priority 3', 'woothemes' ), "40" => __( 'Priority 4', 'woothemes' ), "50" => __( 'Priority 5', 'woothemes' ), "60" => __( 'Priority 6', 'woothemes' ) ) );	 
					
$options[] = array( "name" => __( 'Single Post Meta Priority', 'woothemes' ),
					"desc" => __( 'The lower the number, the higher up on the post this element will load.', 'woothemes' ),
					"id" => $shortname."_postmeta_priority",
					"type" => "select2",
					"options" => array("10" => __( 'Priority 1', 'woothemes' ),"20" => __( 'Priority 2', 'woothemes' ),"30" => __( 'Priority 3', 'woothemes' ), "40" => __( 'Priority 4', 'woothemes' ), "50" => __( 'Priority 5', 'woothemes' ), "60" => __( 'Priority 6', 'woothemes' ) ) );	 
				
$options[] = array( 'name' => __( 'Post Excerpt Arrangement', 'woothemes' ),
					'type' => 'subheading' ); 

$options[] = array( "name" => __( 'Arranging the Post Excerpt Elements', 'woothemes' ),
					"desc" => "",
					"id" => $shortname."_postexp_arrangement",
					"std" => __( 'Change the order of where Post title, image and meta display by changing the priority of each below', 'woothemes' ),
					"type" => "info");	


$options[] = array( "name" => __( 'Post Excerpt Title Priority', 'woothemes' ),
					"desc" => __( 'The lower the number, the higher up on the post this element will load.', 'woothemes' ),
					"id" => $shortname."_arctitle_priority",
					"type" => "select2",
					"options" => array("10" => __( 'Priority 1', 'woothemes' ),"20" => __( 'Priority 2', 'woothemes' ),"30" => __( 'Priority 3', 'woothemes' ), "40" => __( 'Priority 4', 'woothemes' ), "50" => __( 'Priority 5', 'woothemes' ), "60" => __( 'Priority 6', 'woothemes' ) ) );	 
					
$options[] = array( "name" => __( 'Post Excerpt Image Priority', 'woothemes' ),
					"desc" => __( 'The lower the number, the higher up on the post this element will load.', 'woothemes' ),
					"id" => $shortname."_arcimage_priority",
					"type" => "select2",
					"options" => array("10" => __( 'Priority 1', 'woothemes' ),"20" => __( 'Priority 2', 'woothemes' ),"30" => __( 'Priority 3', 'woothemes' ), "40" => __( 'Priority 4', 'woothemes' ), "50" => __( 'Priority 5', 'woothemes' ), "60" => __( 'Priority 6', 'woothemes' ) ) );	 
					
$options[] = array( "name" => __( 'Post Excerpt Meta Priority', 'woothemes' ),
					"desc" => __( 'The lower the number, the higher up on the post this element will load.', 'woothemes' ),
					"id" => $shortname."_arcmeta_priority",
					"type" => "select2",
					"options" => array("10" => __( 'Priority 1', 'woothemes' ),"20" => __( 'Priority 2', 'woothemes' ),"30" => __( 'Priority 3', 'woothemes' ), "40" => __( 'Priority 4', 'woothemes' ), "50" => __( 'Priority 5', 'woothemes' ), "60" => __( 'Priority 6', 'woothemes' ) ) );	 
					
										
$options[] = array( 'name' => __( 'Images', 'woothemes' ),
					'type' => 'subheading' ); 

$options[] = array( "name" => __( 'Blog Thumbnail Dimensions', 'woothemes' ),
					"desc" => __( 'Enter an integer value i.e. 250 for the desired size which will be used when dynamically creating the images.', 'woothemes' ),
					"id" => $shortname."_image_dimensions",
					"std" => "",
					"type" => array( 
									array(  'id' => $shortname. '_thumb_w',
											'type' => 'text',
											'std' => "150",
											'meta' => __( 'Width', 'woothemes' ) ),
									array(  'id' => $shortname. '_thumb_h',
											'type' => 'text',
											'std' => 150,
											'meta' => __( 'Height', 'woothemes' ) )
								  ));
                                                                                                
$options[] = array( "name" => __( 'Blog Thumbnail Alignment', 'woothemes' ),
					"desc" => __( 'Select how to align your thumbnails with posts.', 'woothemes' ),
					"id" => $shortname."_thumb_align",
					"std" => "alignleft",
					"type" => "radio",
					"options" => array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center")); 

$options[] = array( "name" => __( 'Single Post - Show Thumbnail', 'woothemes' ),
					"desc" => __( 'Show the thumbnail in the single post page.', 'woothemes' ),
					"id" => $shortname."_thumb_single",
					"std" => "false",
					"type" => "checkbox");    

$options[] = array( "name" => __( 'Single Post - Thumbnail Dimensions', 'woothemes' ),
					"desc" => __( 'Enter an integer value i.e. 250 for the image size.', 'woothemes' ),
					"id" => $shortname."_image_dimensions",
					"std" => "",
					"type" => array( 
									array(  'id' => $shortname. '_single_w',
											'type' => 'text',
											'std' => 200,
											'meta' => __( 'Width', 'woothemes' ) ),
									array(  'id' => $shortname. '_single_h',
											'type' => 'text',
											'std' => 200,
											'meta' => __( 'Height', 'woothemes' ) )
								  ));

$options[] = array( "name" => __( 'Single Post - Thumbnail Alignment', 'woothemes' ),
					"desc" => __( 'Select how to align your thumbnails with single posts.', 'woothemes' ),
					"id" => $shortname."_thumb_align_single",
					"std" => "alignright",
					"type" => "radio",
					"options" => array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center")); 


$options[] = array( "name" => __( 'Add thumbnail to RSS feed', 'woothemes' ),
					"desc" => __( 'Add the the image uploaded via your Custom Settings to your RSS feed', 'woothemes' ),
					"id" => $shortname."_rss_thumb",
					"std" => "true",
					"type" => "checkbox");    

	
	$options[] = array( 'name' => __( 'Posts Meta Info', 'woothemes' ),
					'type' => 'subheading' );
						
	$options[] = array( "name" => __( 'Include in the Post Meta', 'woothemes' ),
					"desc" => "",
					"id" => $shortname."_postmeta_details",
					"std" => __( 'Post Meta is the extra info about a post that usually shows up right under the post title. It usually includes the author, date, categories and tags of a post. Below you can select what shows up in the post meta line.', 'woothemes' ),
					"type" => "info");

	$options[] = array( "name" => __( 'Show Post Meta', 'woothemes' ),
					"desc" => __( 'Check the box to show post meta', 'woothemes' ),
					"id" => $shortname."_show_postmeta",
					"std" => "true",
					"type" => "checkbox"); 
							
	$options[] = array( "name" => __( 'Show the Post Author', 'woothemes' ),
					"desc" => __( 'Check the box to show the post author', 'woothemes' ),
					"id" => $shortname."_show_postmeta_by",
					"std" => "true",
					"type" => "checkbox"); 
						
	$options[] = array( "name" => __( 'Show the Post Date', 'woothemes' ),
					"desc" => __( 'Check the box to show the post date', 'woothemes' ),
					"id" => $shortname."_show_postmeta_on",
					"std" => "true",
					"type" => "checkbox");
						
	$options[] = array( "name" => __( 'Show the Post Categories', 'woothemes' ),
					"desc" => __( 'Check the box to show the post categories', 'woothemes' ),
					"id" => $shortname."_show_postmeta_incats",
					"std" => "true",
					"type" => "checkbox"); 
					
	$options[] = array( "name" => __( 'Show the Post Tags', 'woothemes' ),
					"desc" => __( 'Check the box to show the post tags', 'woothemes' ),
					"id" => $shortname."_show_postmeta_intags",
					"std" => "true",
					"type" => "checkbox"); 
					
	$options[] = array( "name" => __( 'Show the Post Comments', 'woothemes' ),
				"desc" => __( 'Check the box to show the post comments', 'woothemes' ),
				"id" => $shortname."_show_postmeta_incomments",
				"std" => "true",
				"type" => "checkbox"); 				
	
	$options[] = array( "name" => __( 'Post Meta Appearance', 'woothemes' ),
					"desc" => "",
					"id" => $shortname."_postmeta_appearance",
					"std" => __( 'below are some options for how your post meta will display', 'woothemes' ),
					"type" => "info");
	
	$child_images_dir =  get_stylesheet_directory_uri() . '/images/';
						
	$options[] = array( "name" => __( 'Show Labels next to post meta', 'woothemes' ),
					"desc" => __( 'Check the box to show labels next to author, date, categories, etc.. in post meta. <div style="padding: 5px; border: 1px solid #AEAEAE;">Example:<br /><img src="'.$child_images_dir.'postmeta-labels.png" width="500" /></div>', 'woothemes' ),
					"id" => $shortname."_postmeta_labels",
					"std" => "true",
					"class" => 'collapsed',
					"type" => "checkbox"); 
					
	$options[] = array( "name" => __( 'Author Label', 'woothemes' ),
					"desc" => __( 'what to say next to the author - e.g. "By"', 'woothemes' ),
					"id" => $shortname."_postmeta_authortag",
					"std" => "By",
					'class' => 'hidden', 
					"type" => "text");	
	
	$options[] = array( "name" => __( 'Date Label', 'woothemes' ),
				"desc" => __( 'what to say next to the date - e.g. "On"', 'woothemes' ),
				"id" => $shortname."_postmeta_datetag",
				"std" => "On",
				'class' => 'hidden', 
				"type" => "text");
						
	$options[] = array( "name" => __( 'Catagories Label', 'woothemes' ),
					"desc" => __( 'what to say next to the categories - e.g. "In Categories"', 'woothemes' ),
					"id" => $shortname."_postmeta_cattag",
					"std" => "In",
					'class' => 'hidden', 
					"type" => "text");
	
	$options[] = array( "name" => __( 'Tags Label', 'woothemes' ),
					"desc" => __( 'what to say next to the tags - e.g. "On"', 'woothemes' ),
					"id" => $shortname."_postmeta_tagtag",
					"std" => "",
					'class' => 'hidden last', 
					"type" => "text");


/****************************/	
/*                          */
/*     DYNAMIC IMAGES       */	
/*                          */
/****************************/	
// 
$options[] = array( "name" => __( 'Dynamic Images', 'woothemes' ),
					"icon" => "image",
				    "type" => "heading");    

	$options[] = array( "name" => __( 'Dynamic Image Resizing', 'woothemes' ),
						"desc" => "",
						"id" => $shortname."_wpthumb_notice",
						"std" => __( 'There are two alternative methods of dynamically resizing the thumbnails in the theme, <strong>WP Post Thumbnail</strong> or <strong>TimThumb - Custom Settings panel</strong>. We recommend using WP Post Thumbnail option.', 'woothemes' ),
						"type" => "info");					
	
	$options[] = array( "name" => __( 'WP Post Thumbnail', 'woothemes' ),
						"desc" => __( 'Use WordPress post thumbnail to assign a post thumbnail. Will enable the <strong>Featured Image panel</strong> in your post sidebar where you can assign a post thumbnail.', 'woothemes' ),
						"id" => $shortname."_post_image_support",
						"std" => "true",
						"class" => "collapsed",
						"type" => "checkbox" );
	
	$options[] = array( "name" => __( 'WP Post Thumbnail - Dynamic Image Resizing', 'woothemes' ),
						"desc" => __( 'The post thumbnail will be dynamically resized using native WP resize functionality. <em>(Requires PHP 5.2+)</em>', 'woothemes' ),
						"id" => $shortname."_pis_resize",
						"std" => "true",
						"class" => "hidden",
						"type" => "checkbox" );
	
	$options[] = array( "name" => __( 'WP Post Thumbnail - Hard Crop', 'woothemes' ),
						"desc" => __( 'The post thumbnail will be cropped to match the target aspect ratio (only used if <em>Dynamic Image Resizing</em> is enabled).', 'woothemes' ),
						"id" => $shortname."_pis_hard_crop",
						"std" => "true",
						"class" => "hidden last",
						"type" => "checkbox" );
	
	$options[] = array( "name" => __( 'TimThumb - Custom Settings Panel', 'woothemes' ),
						"desc" => __( 'This will enable the <a href="http://code.google.com/p/timthumb/">TimThumb</a> (thumb.php) script which dynamically resizes images added through the <strong>custom settings panel below the post</strong>. Make sure your themes <em>cache</em> folder is writable. <a href="http://www.woothemes.com/2008/10/troubleshooting-image-resizer-thumbphp/">Need help?</a>', 'woothemes' ),
						"id" => $shortname."_resize",
						"std" => "true",
						"type" => "checkbox" );

$options[] = array( "name" => __( 'Automatic Image Thumbnail', 'woothemes' ),
					"desc" => __( 'If no thumbnail is specified then the first uploaded image in the post is used.', 'woothemes' ),
					"id" => $shortname."_auto_img",
					"std" => "false",
					"type" => "checkbox" );

$options[] = array( "name" => __( 'Enable Lightbox', 'woothemes' ),
					"desc" => __( 'Enable the PrettyPhoto lighbox script on images within your website\'s content.', 'woothemes' ),
					"id" => $shortname."_enable_lightbox",
					"std" => "false",
					"type" => "checkbox" );



/****************************/	
/*                          */
/*       SOCIAL MEDIA       */	
/*                          */
/****************************/	
// 

$options[] = array( "name" => __( 'Social Media Buttons', 'woothemes' ),
					"icon" => "connect",
					"type" => "heading");
					
// Social Media Settings
$options[] = array( "name" => __( 'Social Media Buttons', 'woothemes' ),
					"icon" => "connect",
					"type" => "heading");

// SOCIAL MEDIA ICONS

$options[] = array( 'name' => __( 'Social Media Profiles', 'woothemes' ),
					'type' => 'subheading' );
					
$options[] = array( "name" => __( 'Social Media Connections Icons', 'woothemes' ),
					"desc" => "",
					"id" => $shortname."_topmenu_items",
					"std" => __( 'One great way to build connections with people is by having your profile set up at various Social Media sites and connecting your website to those profiles. Create a profile and/or page on Facebook, get a Twitter account and tweet often, create a Linked In profile, and others. Then add your profile and account urls in the spaces below. Click the button to display your social media icons and a link for each of your profiles will appear in the top right of the banner.', 'woothemes' ),
					"type" => "info");	
					
$options[] = array( "name" => __( 'RSS URL', 'woothemes' ),
					"desc" => __( 'Enter your preferred RSS URL (Feedburner or other). If left blank, the normal Wordpress RSS feed link ', 'woothemes' ),
					"id" => $shortname."_feed_url",
					"std" => "",
					"type" => "text");
	
$options[] = array( "name" => __( 'Twitter URL', 'woothemes' ),
						"desc" => __( 'Enter your  <a href="http://www.twitter.com/" target="_blank">Twitter</a> URL e.g. http://www.twitter.com/myauthorplatform', 'woothemes' ),
					"id" => $shortname."_connect_twitter",
					"std" => '',
					"type" => "text"); 
					
$options[] = array( "name" => __( 'Twitter Handle', 'woothemes' ),
						"desc" => __( 'Enter your Twitter Handle e.g. @myauthorplatform', 'woothemes' ),
					"id" => $shortname."_connect_twitter_handle",
					"std" => '',
					"type" => "text"); 
					
	
$options[] = array( "name" => __( 'Facebook URL', 'woothemes' ),
					"desc" => __( 'Enter your  <a href="http://www.facebook.com/" target="_blank">Facebook</a> URL e.g. http://www.facebook.com/myauthorplatform', 'woothemes' ),
					"id" => $shortname."_connect_facebook",
					"std" => '',
					"type" => "text"); 
						
$options[] = array( "name" => __( 'YouTube URL', 'woothemes' ),
					"desc" => __( 'Enter your  <a href="http://www.youtube.com/" target="_blank">YouTube</a> URL e.g. http://www.youtube.com/myauthorplatform', 'woothemes' ),
					"id" => $shortname."_connect_youtube",
					"std" => '',
					"type" => "text"); 
	
$options[] = array( "name" => __( 'LinkedIn URL', 'woothemes' ),
					"desc" => __( 'Enter your  <a href="http://www.linkedin.com.com/" target="_blank">LinkedIn</a> URL e.g. http://www.linkedin.com/in/myauthorplatform', 'woothemes' ),
					"id" => $shortname."_connect_linkedin",
					"std" => '',
					"type" => "text"); 
	
$options[] = array( "name" => __( 'Pinterest URL', 'woothemes' ),
					"desc" => __( 'Enter your <a href="http://www.pinterest.com/" target="_blank">Pinterest</a> URL e.g. http://www.pinterest.com/myauthorplatform', 'woothemes' ),
					"id" => $shortname."_connect_pinterest",
					"std" => '',
					"type" => "text"); 
					
$options[] = array( "name" => __( 'Goodreads URL', 'woothemes' ),
					"desc" => __( 'Enter your <a href="http://www.goodreads.com/" target="_blank">Goodreads</a> URL e.g. http://www.goodreads.com/myauthorplatform', 'woothemes' ),
					"id" => $shortname."_connect_goodreads",
					"std" => '',
					"type" => "text"); 
	
$options[] = array( "name" => __( 'Google+ URL', 'woothemes' ),
					"desc" => __( 'Enter your <a href="http://plus.google.com/" target="_blank">Google+</a> URL e.g. https://plus.google.com/104560124403688998123/. <br /> THIS WILL ALSO BE USED FOR THE REL=AUTHOR TAG IN FOR GOOGLE - VERY HELPFUL FOR SEO!', 'woothemes' ),
					"id" => $shortname."_connect_googleplus",
					"std" => '',
					"type" => "text" );
					
/* Likes and Tweets */		
									
	$options[] = array( 'name' => __( 'Like and Tweet buttons', 'woothemes' ),
						'type' => 'subheading' );
						
	$options[] = array( "name" => __( 'Social Media Connections Icons', 'woothemes' ),
						"desc" => "",
						"id" => $shortname."_liketweet_description",
						"std" => __( 'Like and Tweet buttons show up on each blog post and allow your readers to announce to their friends on Facebook (Like button) and Twitter (Tweet button) that your article is worth a look. The buttons also display how many people liked or tweeted that article', 'woothemes' ),
						"type" => "info");	
						
	$options[] = array( "name" => __( 'Use LikeTweet Buttons', 'woothemes' ),
						"desc" => __( 'Check the box use LikeTweet Buttons. If NOT checked, then LikeTweets will not be used at all.', 'woothemes' ),
						"id" => $shortname."_use_liketweets",
						"std" => "false",
						"type" => "checkbox"); 

	
	$options[] = array( "name" => __( 'Show buttons in post excerpts on category pages', 'woothemes' ),
						"desc" => __( 'Check the box to show like and tweet buttons for each post in that post\'s excertp. Post excerpts are the short teasers for each post that show up in a list on category and archive pages.', 'woothemes' ),
						"id" => $shortname."_liketweet_in_excerpts",
						"std" => "false",
						"type" => "checkbox"); 
	
	
	$options[] = array( "name" => __( 'Show buttons in pages', 'woothemes' ),
						"desc" => __( 'Check the box to show like and tweet buttons for pages.', 'woothemes' ),
						"id" => $shortname."_liketweet_in_pages",
						"std" => "false",
						"type" => "checkbox"); 
	
	$options[] = array( "name" => __( 'Show buttons in book pages', 'woothemes' ),
						"desc" => __( 'Check the box to show like and tweet buttons for book pages.', 'woothemes' ),
						"id" => $shortname."_liketweet_in_books",
						"std" => "false",
						"type" => "checkbox"); 
	
	$child_images_dir =  get_stylesheet_directory_uri() . '/images/';					
	
	$options[] = array( "name" => __( 'Button Location', 'woothemes' ),
						"desc" => __( 'Select where you would like your Like and Tweet buttons to appear.', 'woothemes' ),
						"id" => $shortname . "_liketweet_placement",
						"std" => "before",
						"type" => "images",
						"options" => array(
							'before' => $child_images_dir . 'liketweet_before.jpg',
							'after' => $child_images_dir . 'liketweet_after.jpg'
							)
						);	
// LikeTweet Button Choices	
									
	$options[] = array( "name" => __( 'Show Facebook Share Count Button', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_liketweet_sharethis_facebook",
						"std" => "true",
						"type" => "checkbox"); 
						
	$options[] = array( "name" => __( 'Show Facebook Like Count Button', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_liketweet_sharethis_fblike",
						"std" => "false",
						"type" => "checkbox"); 

	$options[] = array( "name" => __( 'Show Twitter Count Button', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_liketweet_sharethis_twitter",
						"std" => "true",
						"type" => "checkbox");
						
						
						
	$options[] = array( "name" => __( 'Show Google+ Count Button', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_liketweet_sharethis_googleplus",
						"std" => "true",
						"type" => "checkbox"); 
						
	$options[] = array( "name" => __( 'Show Pinterest Count Button', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_liketweet_sharethis_pinterest",
						"std" => "true",
						"type" => "checkbox"); 
						
	$options[] = array( "name" => __( 'Show LinkedIn Count Button', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_liketweet_sharethis_linkedin",
						"std" => "true",
						"type" => "checkbox"); 
					

	$options[] = array( "name" => __( 'Show ShareThis Count Button', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_liketweet_sharethis_sharethis",
						"std" => "true",
						"type" => "checkbox"); 

	$options[] = array( "name" => __( 'Show Comment Count Button', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_liketweet_sharethis_comments",
						"std" => "true",
						"type" => "checkbox"); 
						
/* ShareThis Buttons*/						
							
	$options[] = array( 'name' => __( 'Social Media Share Buttons', 'woothemes' ),
						'type' => 'subheading' );
						
	$options[] = array( "name" => __( 'Share Button Setup', 'woothemes' ),
						"desc" => "",
						"id" => $shortname."_sharebutton_description",
						"std" => __( 'Share buttons allow readers to share information about a post or page they are reading on your site with their friends in one of many social media networks. Click the box below to use Sharethis with the basic options. Additional Button styles and settings can be applied by going here http://www.sharethis.com/publishers/get-sharing-tools to output custom buttons and replacing the code in /includes/sharethis-functions.php ( some coding skills required ) ', 'woothemes' ),
						"type" => "info");	
						

						
	$options[] = array( "name" => __( 'Use Sharethis - with basic options', 'woothemes' ),
						"desc" => __( 'Check the box to show sharethis buttons at the bottom of each post.', 'woothemes' ),
						"id" => $shortname."_use_sharethis",
						"std" => "false",
						"class" => 'collapsed',
						"type" => "checkbox"); 

	$options[] = array( "name" => __( 'Show Sharethis Bar on Archives', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_sharethis_onexcerpts",
						"std" => "false",
						"class" => 'hidden',
						"type" => "checkbox"); 
												
						
						
	$options[] = array( "name" => __( 'Sharethis Label', 'woothemes' ),
						"desc" => __( 'Label for Sharethis bar- e.g. "Share This:"', 'woothemes' ),
						"id" => $shortname."_sharethis_label",
						"std" => "Share This: ",
						"class" => "hidden",
						"type" => "text");	
							
// Sharethis Button Choices	
									
	$options[] = array( "name" => __( 'Show Facebook Button', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_sharethis_facebook",
						"std" => "true",
						"class" => 'hidden',
						"type" => "checkbox"); 
						
	$options[] = array( "name" => __( 'Show Facebook Like Button', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_sharethis_fblike",
						"std" => "false",
						"class" => 'hidden',
						"type" => "checkbox"); 
						
	$options[] = array( "name" => __( 'Show Twitter Button', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_sharethis_twitter",
						"std" => "true",
						"class" => 'hidden',
						"type" => "checkbox");
						
	$options[] = array( "name" => __( 'Show Google+ Button', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_sharethis_googleplus",
						"std" => "false",
						"class" => 'hidden',
						"type" => "checkbox"); 
						
	$options[] = array( "name" => __( 'Show Pinterest Button', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_sharethis_pinterest",
						"std" => "false",
						"class" => 'hidden',
						"type" => "checkbox"); 
						
	$options[] = array( "name" => __( 'Show LinkedIn Button', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_sharethis_linkedin",
						"std" => "true",
						"class" => 'hidden',
						"type" => "checkbox"); 
						
	$options[] = array( "name" => __( 'Show Email Button', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_sharethis_linkedin",
						"std" => "true",
						"class" => 'hidden',
						"type" => "checkbox"); 

	$options[] = array( "name" => __( 'Show ShareThis Button', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_sharethis_sharethis",
						"std" => "true",
						"class" => 'hidden',
						"type" => "checkbox"); 

						
	$options[] = array( "name" => __( 'Sharethis Button Style', 'woothemes' ),
					"desc" => __( 'Select the kind of buttons that you want.', 'woothemes' ),
					"id" => $shortname."_sharethis_style",
					"std" => "alignleft",
					"type" => "radio",
					"options" => array("large" => "Large Buttons","small" => "Small Buttons","vcounts" => "Buttons with Counts over them","hcounts" => "Small Buttons with Counts - right")); 				

														

/************************************/	
/*                                  */
/*   EMAIL AND FEED SUBSCRIPTIONS   */	
/*                                  */
/************************************/	
// 
/* Email and Feed Subscription */	
$options[] = array( "name" => __( 'Subscriptions', 'woothemes' ),
					"type" => "heading",
					"icon" => "featured"); 



/* RSS FEEDS */	

/*
$options[] = array( 'name' => __( 'Subscribe By Email - Feedburner', 'woothemes' ),
					'type' => 'subheading' );
					
$options[] = array( "name" => __( 'Subscribe By E-mail ID (Feedburner)', 'woothemes' ),
					"desc" => sprintf( __( 'Enter your <a href="%s">Feedburner ID</a> for the e-mail subscription form.', 'woothemes' ), 'http://www.google.com/support/feedburner/bin/answer.py?hl=en&answer=78982' ),
					"id" => $shortname."_connect_newsletter_id",
					"std" => '',
					"type" => "text"); 		
*/
/****************************/
/* Mailchimp theme options  */
/****************************/

$options[] = array( 'name' => __( 'Mail List Subscription (Mailchimp)', 'woothemes' ),
					'type' => 'subheading' );
					
$options[] = array( "name" => __( 'MailChimp Setup', 'woothemes' ),
					"desc" => '',
					"id" => $shortname."_connect_mailchimp_setup",
					"std" => 'Add your Mailchimp API settings below',
					"type" => "info"); 					

$options[] = array( 'name' => "Mailchimp Api Key",
					'desc' => "Enter the Mailchimp API key for your account",
					'id' => $shortname . '_mc_api_key',
					'std' => '',
					'type' => 'text' );	
						
$options[] = array( 'name' => "Mailchimp List ID",
					'desc' => "Enter the Mailchimp list Id",
					'id' => $shortname . '_mc_list_id',
					'std' => '',
					'type' => 'text' );	
					
$options[] = array( 'name' => "Email Address Label",
					'desc' => "label that shows above the email box",
					'id' => $shortname . '_email_label',
					'std' => '',
					'type' => 'text' );	
					
$options[] = array( 'name' => "Email Default Value",
					'desc' => "label that shows inside the email box, that clears when you click into the box",
					'id' => $shortname . '_email_default_value',
					'std' => '',
					'type' => 'text' );					

/* sidebar widget */				
$options[] = array( "name" => __( 'Subscribe Widget -- Sidebar ', 'woothemes' ),
					"desc" => '',
					"id" => $shortname."_connect_mailchimp_widget_sidebar_info",
					"std" => "Use the following controls to set up your sidebar subscribe widget",
					"type" => "info"); 					

$options[] = array( "name" => __( 'Sidebar Subscribe Widget Image', 'woothemes' ),
					"desc" => __( 'Upload an image to appear above the email line of the sidebar subscribe widget.', 'woothemes' ),
					"id" => $shortname."_subscribe_widget_image",
					"std" => "",
					"type" => "upload");    
		
$options[] = array( "name" => __( 'Sidebar Subscribe Image Alignment', 'woothemes' ),
					"desc" => __( 'Select the alignment for the image - default is centered', 'woothemes' ),
					"id" => $shortname."_subscribe_widget_ialign",
					"std" => "aligncenter",
					"type" => "select2",
					"options" => array( 'aligncenter' => __( 'No Align - Float Top, Center', 'woothemes' ), 'alignleft' => __( 'Align Left', 'woothemes' ), 'alignright' => __( 'Align Right', 'woothemes' ) ) );	
						
						
$options[] = array( 'name' => "Sidebar Subscribe Form Title",
					'desc' => "Optional: Enter a title that will go above the image and subscribe form",
					'id' => $shortname . '_subscribe_widget_title',
					'std' => '',
					'type' => 'text' );												

$options[] = array( 'name' => "Sidebar Subscribe Text before the form",
					'desc' => "Optional: Enter text that will go under the image but before the form",
					'id' => $shortname . '_subscribe_widget_text_before',
					'std' => '',
					'type' => 'text' );												

$options[] = array( 'name' => "Sidebar Subscribe Text after the form",
					'desc' => "Optional: Enter text that will go under the image but before the form",
					'id' => $shortname . '_subscribe_widget_text_after',
					'std' => '',
					'type' => 'text' );	

$options[] = array( 'name' => "Subscribe with Name Field in Sidebar Form?",
					'desc' => "Check if you want to have subscribers add their name in the form",
					'id' => $shortname . '_mc_subscribe_with_name',
					'std' => '',
					'class' => 'collapsed',
					"type" => "checkbox" );

$options[] = array( 'name' => "Name Label",
					'desc' => "label that shows above the name box",
					'id' => $shortname . '_name_label',
					'std' => '',
					'class' => 'hidden',
					'type' => 'text' );	
					
$options[] = array( 'name' => "Name Default Value",
					'desc' => "label that shows inside the name box, that clears when you click into the box",
					'id' => $shortname . '_name_default_value',
					'std' => '',
					'class' => 'hidden last',
					'type' => 'text' );	

						
$options[] = array( 'name' => "Mailchimp Use Groupings in Sidebar Form?",
					'desc' => "If you have groups set up in Mailchimp for your list and want to allow users to choose groups to join, check this box. Your Groupings name and groups will appear on your signup form",
					'id' => $shortname . '_mc_use_groupings',
					'std' => 0,
					'class' => 'collapsed',
					"type" => "checkbox" );
					
$options[] = array( 'name' => "Mailchimp Group to have checked by default (Optional)",
					'desc' => "Choose the checkbox of the group to select by default. If none is selected then the first box will be checked. If you are not sure what to do here, leave it blank.",
					'id' => $shortname . '_mc_sel_group',
					'std' => '',
					'class' => 'hidden last',
					'type' => 'text' );						
					
$options[] = array( 'name' => "Sidebar Form Submit Button Text",
					'desc' => "Text that displays in submit button",
					'id' => $shortname . '_submit_button',
					'std' => "",
					'type' => 'text' );	
				
$options[] = array( 'name' => "Sidebar Form Text after submit",
					'desc' => "Text that displays in place of the form after someone signs up",
					'id' => $shortname . '_text_after_signup',
					'std' => "",
					'type' => 'textarea' );	

/* homepage widget */

$options[] = array( 'name' => __( 'Additional Subscribe Widget', 'woothemes' ),
					'type' => 'subheading' );
					
$options[] = array( 'name' => "Use Additional Subscribe Widget",
					'desc' => "Add an additional subscribe widget separate from the one above. This is great for adding different settings to the homepage version of the subscribe widget but also useful in other places",
					'id' => $shortname . '_connect_mailchimp_widget_home_enable',
					'std' => 0,
					"class" => '',
					"type" => "checkbox" );
					
$options[] = array( "name" => __( 'Subscribe Widget -- Homepage ', 'woothemes' ),
					"desc" => '',
					"id" => $shortname."_connect_mailchimp_widget_home_info",
					"std" => "Use the following controls to set up your homepage subscribe widget. Though is called Homepage, This widget can also be used where you want a differnt widget format. It is generally used on the homepage and generally is more horizontal in orientation",
					"class" => "",
					"type" => "info"); 					

$options[] = array( "name" => __( 'Homepage Subscribe Widget Image', 'woothemes' ),
					"desc" => __( 'Upload an image to appear above the email line of the sidebar subscribe widget.', 'woothemes' ),
					"id" => $shortname."_subscribe_widget_image_home",
					"std" => "",
					"class" => "",
					"type" => "upload");     
				
$options[] = array( "name" => __( 'Homepage Subscribe Image Alignment', 'woothemes' ),
					"desc" => __( 'Select the alignment for the image - default is centered', 'woothemes' ),
					"id" => $shortname."_subscribe_widget_ialign_home",
					"std" => "aligncenter",
					"class" => "",
					"type" => "select2",
					"options" => array( 'aligncenter' => __( 'No Align - Float Top, Center', 'woothemes' ), 'alignleft' => __( 'Align Left', 'woothemes' ), 'alignright' => __( 'Align Right', 'woothemes' ) ) );	
						
						
$options[] = array( 'name' => "Homepage Subscribe Form Title",
					'desc' => "Optional: Enter a title that will go above the image and subscribe form",
					'id' => $shortname . '_subscribe_widget_title_home',
					"class" => "",
					'std' => '',
					'type' => 'text' );												

$options[] = array( 'name' => "Homepage Subscribe Text before the form",
					'desc' => "Optional: Enter text that will go under the image but before the form",
					'id' => $shortname . '_subscribe_widget_text_before_home',
					"class" => "",
					'std' => '',
					'type' => 'text' );												

$options[] = array( 'name' => "Homepage Subscribe Text after the form",
					'desc' => "Optional: Enter text that will go under the image but before the form",
					'id' => $shortname . '_subscribe_widget_text_after_home',
					"class" => "",
					'std' => '',
					'type' => 'text' );	
						
$options[] = array( 'name' => "Mailchimp Use Groupings in Homepage Form?",
					'desc' => "If you have groups set up in Mailchimp for your list and want to allow users to choose groups to join, check this box. Your Groupings name and groups will appear on your signup form",
					'id' => $shortname . '_mc_use_groupings_home',
					'std' => 0,
					"class" => '',
					"type" => "checkbox" );
					
										
$options[] = array( 'name' => "Subscribe with Name Field in Homepage Form?",
					'desc' => "Check if you want to have subscribers add their name in the form",
					'id' => $shortname . '_mc_subscribe_with_name_home',
					'std' => '',
					"class" => "collapsed",
					"type" => "checkbox" );
					
$options[] = array( 'name' => "Name Label in Homepage Form",
					'desc' => "label that shows above the name box",
					'id' => $shortname . '_name_label_home',
					'std' => '',
					'class' => 'hidden',
					'type' => 'text' );	
					
$options[] = array( 'name' => "Name Default Value in Homepage Form",
					'desc' => "label that shows inside the name box, that clears when you click into the box",
					'id' => $shortname . '_name_default_value_home',
					'std' => '',
					'class' => 'hidden last',
					'type' => 'text' );	


$options[] = array( 'name' => "Homepage Form Submit Button Text",
					'desc' => "Text that displays in submit button",
					'id' => $shortname . '_submit_button_home',
					'std' => "",
					"class" => "",
					'type' => 'text' );	
				
$options[] = array( 'name' => "Homepage Form Text after submit",
					'desc' => "Text that displays in place of the form after someone signs up",
					'id' => $shortname . '_text_after_signup_home',
					'std' => "",
					"class" => "",
					'type' => 'textarea' );	



// Add extra options through function
if ( function_exists("woo_options_add") )
	$options = woo_options_add($options);

if ( get_option('woo_template') != $options) update_option('woo_template',$options);      
if ( get_option('woo_themename') != $themename) update_option('woo_themename',$themename);   
if ( get_option('woo_shortname') != $shortname) update_option('woo_shortname',$shortname);
if ( get_option('woo_manual') != $manualurl) update_option('woo_manual',$manualurl);
                                                                  
// Woo Metabox Options
$woo_metaboxes = array();

if( get_post_type() == 'post' || !get_post_type() ){

	$woo_metaboxes[] = array (	"name" => "image",
								"label" => __( 'Image', 'woothemes' ),
								"type" => "upload",
								"desc" => __( 'Upload file here...', 'woothemes' ) );
	
	if ( get_option('woo_resize') == "true" ) {						
		$woo_metaboxes[] = array (	"name" => "_image_alignment",
									"std" => "Center",
									"label" => __( 'Image Crop Alignment', 'woothemes' ),
									"type" => "select2",
									"desc" => __( 'Select crop alignment for resized image', 'woothemes' ),
									"options" => array(	"c" => "Center",
														"t" => "Top",
														"b" => "Bottom",
														"l" => "Left",
														"r" => "Right"));
	}

	$url =  get_template_directory_uri() . '/functions/images/';
	$woo_metaboxes[] = array (	"name" => "layout",
								"label" => __( 'Layout', 'woothemes' ),
								"type" => "images",
								"desc" => __( 'Select a specific layout for this post/page. Overrides default site layout.', 'woothemes' ),
								"options" => array(	'' => $url . 'layout-off.png',
													'one-col' => $url . '1c.png',
													'two-col-left' => $url . '2cl.png',
													'two-col-right' => $url . '2cr.png',
													'three-col-left' => $url . '3cl.png',
													'three-col-middle' => $url . '3cm.png',
													'three-col-right' => $url . '3cr.png'));
	
	$woo_metaboxes[] = array (	"name" => "embed",
								"label" => __( 'Embed', 'woothemes' ),
								"type" => "textarea",
								"desc" => __( 'Enter embed code for use on single posts and with the Video widget.', 'woothemes' ) );
	
	if (get_option('woo_woo_tumblog_switch') == 'true') {
	
		$woo_metaboxes[] = array (	"name" => "video-embed",
									"label" => __( 'Tumblog : Embed Code (Videos)', 'woothemes' ),
									"type" => "textarea",
									"desc" => __( 'Add embed code for video services like Youtube or Vimeo - Tumblog only.', 'woothemes' ) );
		
    	$woo_metaboxes[] = array (	"name" => "quote-author",
									"std" => "Unknown",
									"label" => __( 'Tumblog : Quote Author', 'woothemes' ),
									"type" => "text",
									"desc" => __( 'Enter the name of the Quote Author.', 'woothemes' ) );
								    
    	$woo_metaboxes[] = array (	"name" => "quote-url",
									"std" => "http://",
									"label" => __( 'Tumblog : Link to Quote', 'woothemes' ),
									"type" => "text",
									"desc" => __( 'Enter the url/web address of the Quote if available.', 'woothemes' ) );
								    
    	$woo_metaboxes[] = array (	"name" => "quote-copy",
    								"std"  => "Unknown",
									"label" => __( 'Tumblog : Quote', 'woothemes' ),
									"type" => "textarea",
									"desc" => __( 'Enter the Quote.', 'woothemes' ) );
		
		$woo_metaboxes[] = array (	"name" => "audio",
									"std" => "http://",
									"label" => __( 'Tumblog : Audio URL', 'woothemes' ),
									"type" => "text",
									"desc" => __( 'Enter the url/web address of the Audio file.', 'woothemes' ) );							    
    	 
    	$woo_metaboxes[] = array (	"name" => "link-url",
									"std" => "http://",
									"label" => __( 'Tumblog : Link URL', 'woothemes' ),
									"type" => "text",
									"desc" => __( 'Enter the url/web address of the Link.', 'woothemes' ) );  
	
	}
							
} // End post

// if slide
if( get_post_type() == 'slide' || ! get_post_type() ) {

	$woo_metaboxes[] = array (	"name" => "image",
								"label" => __( 'Image', 'woothemes' ),
								"type" => "upload",
								"desc" => __( 'Upload an image to be used as background of this slide. (optional)', 'woothemes' ) );
	
	$woo_metaboxes[] = array (	"name" => "line_1",
								"label" => __( 'Line 1 (Optional)', 'woothemes' ),
								"type" => "text",
								"desc" => __( 'Enter a line of text that will be the top line of the slide. This should be twice as long as line 2', 'woothemes' ) );

	$woo_metaboxes[] = array (	"name" => "line_2",
								"label" => __( 'Line 2 (Optional)', 'woothemes' ),
								"type" => "text",
								"desc" => __( 'Enter a line of text that will be the second line of the slide. This should be half as long as line 1', 'woothemes' ) );
								
	$woo_metaboxes[] = array (	"name" => "button_text",
								"label" => __( 'Button Text (Optional)', 'woothemes' ),
								"type" => "text",
								"desc" => __( 'What do you want the button to say?', 'woothemes' ) );

	
	$woo_metaboxes[] = array (	"name" => "url",
								"label" => __( 'URL', 'woothemes' ),
								"type" => "text",
								"desc" => __( 'Enter URL if you want to add a link to the uploaded image. (optional). Or if using Line 1, Line 2 and button text, this link will be for the button', 'woothemes' ) );
								
								
} // End slide 






// Page fields.
if( get_post_type() == 'page' || ! get_post_type() ) {

	// Create an array of the available "Slide Pages".
	$slide_pages = array(
						'all' => __( 'All', 'woothemes' )
						);
						
	$terms = get_terms( 'slide-page' );
	
	if ( is_array( $terms ) && ( count( $terms ) > 0 ) ) {
		foreach ( $terms as $k => $v ) {
			$slide_pages[$v->slug] = $v->name;
		}
	}
						
	$woo_metaboxes[] = array (	"name" => "_slide-page",
									"std" => "",
									"label" => __( 'Slide Page', 'woothemes' ),
									"type" => "select2",
									"desc" => __( 'Optionally select a "Slide Page" to show slides from only that "Slide Page".', 'woothemes' ),
									"options" => $slide_pages );
								
} // End slide

// Show layout option on all pages
if ( get_post_type() != 'post' && get_post_type() != 'slide' && get_post_type() != 'feedback' ) {

	$url =  get_template_directory_uri() . '/functions/images/';
	$woo_metaboxes[] = array (	"name" => "layout",
								"label" => __( 'Layout', 'woothemes' ),
								"type" => "images",
								"desc" => __( 'Select a specific layout for this post/page. Overrides default site layout.', 'woothemes' ),
								"options" => array(	'' => $url . 'layout-off.png',
													'one-col' => $url . '1c.png',
													'two-col-left' => $url . '2cl.png',
													'two-col-right' => $url . '2cr.png',
													'three-col-left' => $url . '3cl.png',
													'three-col-middle' => $url . '3cm.png',
													'three-col-right' => $url . '3cr.png'));

} 
							

// Add extra metaboxes through function
if ( function_exists("woo_metaboxes_add") )
	$woo_metaboxes = woo_metaboxes_add($woo_metaboxes);
    
if ( get_option('woo_custom_template') != $woo_metaboxes) update_option('woo_custom_template',$woo_metaboxes);      

} // END woo_options()
} // END function_exists()

// Add options to admin_head
add_action( 'admin_head','woo_options' );  

//Enable WooSEO on these Post types
$seo_post_types = array( 'post','page' );
define( "SEOPOSTTYPES", serialize($seo_post_types)); 

//Global options setup
add_action( 'init','woo_global_options' );
function woo_global_options(){
	// Populate WooThemes option in array for use in theme
	global $woo_options;
	$woo_options = apply_filters('change_some_woo_opts',get_option( 'woo_options' ));	
}

/*
add_action( 'init','change_woo_option', 30 );
function change_woo_option(){
	global $woo_options;
	$woo_options['woo_layout'] = 'one-col';
	return $woo_options;
}
*/

?>