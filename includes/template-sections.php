<?php
// To DO
/*
2. either remove call to siteintro  before or keep the do atomic line active
3. Make the css enqueues load after custom.css
*/
// create a few more hookable regions
function woo_homepage_blocks() { woo_do_atomic( 'woo_homepage_blocks' ); }

//
/*************************/	
/*                       */
/*      SITE HEADER       */	
/*                       */
/*************************/	
//


/*-----------------------------------------------------------------------------------*/
/* Show codes in the header */
/*-----------------------------------------------------------------------------------*/
function add_header_codes(){
	global $woo_options;
	$output = $woo_options['woo_header_codes'];
	if ( $output != '' )
		echo stripslashes( $output ) . "\n";
} // End add header codes
add_action( 'wp_head','add_header_codes' );

// Replaces WOO's pathetic site logo function with one that does not force revealing the site name and tagline;
// SITE TITLE

// 
if ( ! function_exists( 'add_site_title' ) ) {
	function add_site_title() {
		global $woo_options;
		
		 $site_title = get_bloginfo( 'name' );
		 $site_url = home_url( '/' );
		 $site_description = get_bloginfo( 'description' );
	
		 ?>   
	<div id="logo">
		<?php
				// Website heading/logo and description text.
				if ( isset($woo_options['woo_logo_show']) && $woo_options['woo_logo_show'] == "true" && isset($woo_options['woo_logo']) && $woo_options['woo_logo'] ) {
					echo '<a href="' . $site_url . '" title="' . $site_description . '"><img src="' . $woo_options['woo_logo'] . '" alt="' . $site_title . '" /></a>' . "\n";
				} // End IF logo
				// then the site name
				if ( isset($woo_options['woo_sitename_show']) && $woo_options['woo_sitename_show']=="true") { // if use sitename is clicked, 
					if ( isset($site_title) && !empty($site_title) )	{ // if site title
						echo '<h1 class="sitetitle"><a href="'.$site_url.'"title="'.$site_title.'">' . $site_title . '</a></h2>' . "\n";
					} // end if site title
				}
				
				// then the site Description
				if ( isset($woo_options['woo_sitedescription_show']) && $woo_options['woo_sitedescription_show']=="true") { // if use sitename is clicked, 
					if ( isset($site_description) && !empty($site_description) )	{ // if site title
						echo '<h2 class="sitedescription">' . $site_description . '</h2>' . "\n";
					} // end if site title
				}		
		
		?>
			</div><!-- /#logo -->	
		  <?php 
	}
}


/*------------*/
/* NAV MENU   */
/*------------*/


// Added by Katie Suess. Altered version of function in canvas theme-actions
/*-----------------------------------------------------------------------------------*/
/* Optional Top Navigation (WP Menus)  */
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'woo_top_navigation' ) ) {
	function woo_top_navigation() {
		if ( function_exists( 'has_nav_menu' ) && has_nav_menu( 'top-menu' ) ) {
	?>		
		<div id="top">
			<div class="col-full">
				<?php wp_nav_menu( array( 'depth' => 6, 'sort_column' => 'menu_order', 'container' => 'ul', 'menu_id' => 'top-nav', 'menu_class' => 'nav fl', 'theme_location' => 'top-menu' ) ); ?>
			</div>
		</div><!-- /#top -->
	<?php 
		}
	} // End woo_top_navigation()
}
// SEARCH BAR

// Search

// Donde Esta Search thing
function add_nav_search() {
	global $woo_options;
	echo '<div id="searchbox">';
	//get_new_search_form();
	echo get_search_form();
	echo '</div>';
}

/**
 * Display 2013 search form. Code taken from WP 3.6 core and renamed from get_search_form
 *
 */
if ( ! function_exists( 'get_new_search_form' ) ) {
	function get_new_search_form( $echo = true ) {
	
		$format = apply_filters( 'search_form_format', 'xhtml' );
	
			if ( 'html5' == $format ) {
				$form = '<form role="search" method="get" class="newsearchform" action="' . esc_url( home_url( '/' ) ) . '">
					<label><span class="screen-reader-text">' . _x( 'Search for:', 'label' ) . '</span>
						<input type="search" placeholder="' . esc_attr_x( 'Search &hellip;', 'placeholder' ) . '" value="' . get_search_query() . '" name="s" title="' . _x( 'Search for:', 'label' ) . '" />
					</label>
					<input type="submit" class="searchsubmit" value="'. esc_attr_x( 'Search', 'submit button' ) .'" />
				</form>';
			} else {
				$form = '<form role="search" method="get" id="newsearchform" class="newsearchform" action="' . esc_url( home_url( '/' ) ) . '">
					<div>
						<label class="screen-reader-text" for="s">' . _x( 'Search for:', 'label' ) . '</label>
						<input type="text" value="' . get_search_query() . '" name="s" id="s" />
						<input type="submit" class="searchsubmit" value="'. esc_attr_x( 'Search', 'submit button' ) .'" />
					</div>
				</form>';
			}
	
		$result = apply_filters( 'get_new_search_form', $form );
		if ( null === $result )
			$result = $form;
	
		if ( $echo )
			echo $result;
		else
			return $result;
	}
}

/*------------------------------------------*/
/*  Breadcrumbs
/*------------------------------------------*/ 

// trying out non-woo-breadcrumbs
// This only works if 
// 1. woo theme options breadcrumbs are off - see display tab
// 2. Install yoast seo then go to yoast seo >> internal links >> make sure breadcrumbs is turned on and basics are configured
if ( ! function_exists( 'use_yoast_breadcrumbs' ) ) {
	function use_yoast_breadcrumbs($woobreadcrumbs){
		global $woo_options;
		//print_r('hi there');
		if ( isset( $woo_options['woo_breadcrumbs_type'] ) && $woo_options['woo_breadcrumbs_type'] != 'yoast' ) {
			return $woobreadcrumbs;
		}
		// do nothing if Yoast Breadcrumbs are not enabled
		if ( !function_exists('yoast_breadcrumb') ) {
			return $woobreadcrumbs;
		}
		// do nothing if this is the front page
		if ( is_front_page()) {
			return false;
		}	 
		 $yoastbreadcrumbs = yoast_breadcrumb('<div class="breadcrumbs">','</div>',false);

		 return $yoastbreadcrumbs;
	}
}
add_filter('woo_breadcrumbs','use_yoast_breadcrumbs');
//add_action('woo_main_before','use_yoast_breadcrumbs',30);


/*------------------------------------------*/
/*  Social Media Profile Buttons
/*------------------------------------------*/

// Profile Buttons
if ( ! function_exists( 'social_media_profile_buttons' ) ) {
	function social_media_profile_buttons() {
		global $woo_options;

		if ( $woo_options['woo_feed_url'] ) { 
			$rsslink = esc_url( $woo_options['woo_feed_url'] ); 
		} else { 
			$rsslink = get_bloginfo_rss('rss2_url'); 
		} 	
		
		$output = '';

		$output .= '<div class="top-social fr">';
		
		if ( $woo_options['woo_connect_facebook' ] != "" ) { 
			$facebook = '<a target="_blank" href="'.esc_url( $woo_options['woo_connect_facebook'] ).'" class="facebook" title="Facebook"></a>';				
		} 
		
		if ( $woo_options['woo_connect_twitter' ] != "" ) { 
			$twitter = '<a target="_blank" href="'.esc_url( $woo_options['woo_connect_twitter'] ).'" class="twitter" title="Twitter"></a>';
		} 
		
		if ( $woo_options['woo_connect_youtube' ] != "" ) { 
			$youtube = '<a target="_blank" href="'.esc_url( $woo_options['woo_connect_youtube'] ).'" class="youtube" title="YouTube"></a>';
		} 
		
		if ( $woo_options['woo_connect_pinterest' ] != "" ) { 
			$pinterest = '<a target="_blank" href="'.esc_url( $woo_options['woo_connect_pinterest'] ).'" class="pinterest" title="Pinterest"></a>';
		} 
		
		if ( $woo_options['woo_connect_linkedin' ] != "" ) { 
			$linkedin = '<a target="_blank" href="'.esc_url( $woo_options['woo_connect_linkedin'] ).'" class="linkedin" title="LinkedIn"></a>';
		} 
		
		if ( $woo_options['woo_connect_googleplus' ] != "" ) { 
			$googleplus = '<a target="_blank" href="'.esc_url( $woo_options['woo_connect_googleplus'] ).'" class="googleplus" title="Google+"></a>';
		} 
		
		if ( $woo_options['woo_connect_goodreads' ] != "" ) {
			$goodreads = '<a target="_blank" href="'.esc_url( $woo_options['woo_connect_goodreads'] ).'" class="goodreads" title="Goodreads"></a>';
		} 
		
		if ( $woo_options['woo_feed_url' ] != "" ) { 
			$rss = '<a target="_blank" href="'.$rsslink.'" class="subscribe" title="RSS"></a>';	
		 } 
		 
		// print_r('<pre style="padding: 10px; border: 1px solid #000; margin: 10px">'); print_r( $twitter ); print_r('</pre>');
		 // allow changing the order through a filter
		$default_order = '[x[facebook]x] [x[twitter]x] [x[youtube]x] [x[pinterest]x] [x[linkedin]x] [x[googleplus]x] [x[goodreads]x] [x[rss]x]';
		$order = apply_filters('change_socmed_node_order',$default_order);
		$socmed = str_replace(array('[x[facebook]x]','[x[twitter]x]','[x[youtube]x]','[x[pinterest]x]','[x[linkedin]x]','[x[googleplus]x]','[x[goodreads]x]','[x[rss]x]'),array($facebook,$twitter,$youtube,$pinterest,$linkedin,$googleplus,$goodreads,$rss),$order);
			
		$output .= $socmed;
			 
		$output .= '</div>'; // end social  
		return $output;
	}
}



if ( ! function_exists( 'woo_add_socmed_to_banner' ) ) {
	function woo_add_socmed_to_banner() {
		echo '<div id="sm-profiles">';		
			echo social_media_profile_buttons();
		echo '</div><!-- #sm-profiles -->';
	} // End woo_add_banner_notice()
}

//
/*************************/	
/*                       */
/*      HOME PAGE        */	
/*                       */
/*************************/	
//

// HOMEPAGE INTRO
if ( ! function_exists( 'add_homeintro' ) ) {
	function add_homeintro(){
		global $woo_options;
		// Add the Homepage Intro
		if ( isset($woo_options['woo_homeintro_enable']) && $woo_options['woo_homeintro_enable'] == "true" && ( is_front_page() )) {
		?>
		<div id="homeintro">
			
		<?php
			if (isset( $woo_options['woo_homeintro_image'] ) && $woo_options['woo_homeintro_image'] != '' ) { 
				if ( isset($woo_options['woo_homeintro_image_size']) && !empty($woo_options['woo_homeintro_image_size'])) { 
					woo_image('src='. $woo_options['woo_homeintro_image'] .'&meta='.$woo_options['woo_homeintro_title'].'&width='.$thewidth.'&noheight=true&class=homeimage'); 
				} else { 
					$image = '<img class="homeimage thumbnail aligncenter" alt="'.$woo_options['woo_homeintro_title'].'" src="'.$woo_options['woo_homeintro_image'].'">';
					echo $image;
				}
				
			}	
		?>			
			<div class="introtext">
			<h2 class="homeintrotitle"><?php echo $woo_options['woo_homeintro_title'];?></h2>
			<?php if(isset($woo_options['woo_authorpubbox_content']) ){ echo wpautop(stripslashes($woo_options['woo_authorpubbox_content']));} ?>
			<?php if( isset($woo_options['woo_authorpubbox_content']) && isset($woo_options['woo_homeintro_link']) && $woo_options['woo_homeintro_link'] != '') { ?>
			<a href="<?php echo $woo_options['woo_homeintro_link'];?>" class="read-more">Read more &raquo;</a>
			<?php } ?>
				
			</div>
			<div class="fix"></div>
	
		</div>
		<?php 
		
		} // if homeintro is true
	
	} // end function
}
// END HOME INTRO 


// HOME VIDEO
if ( ! function_exists( 'add_homevideo' ) ) {
	function add_homevideo(){
		global $woo_options;
		// Add the Homepage Intro
		if (isset($woo_options['woo_homevideo_enable']) && $woo_options['woo_homevideo_enable'] == "true" && is_front_page() ) {
		?>	
			<div id="home-video-cont">
				<div id="home-video">
					<div class="embed-container">
						<?php echo $woo_options['woo_home_video']; ?>
					</div>	
					<?php if (  is_user_logged_in() && current_user_can('manage_options') ) { ?>
					<a href="/wp-admin/admin.php?page=woothemes#woo-option-homepagesettings">Edit Intro (click here, then Homepage settings tab)</a>
					<?php } ?>			
					<div class="fix"></div>	
				</div>
			</div>
		<?php 
		
		} // if homevideo is true
	} // end home vid function
}

// END HOME Video

// HOME TAG LINE
if ( ! function_exists( 'add_hometagline' ) ) {
	function add_hometagline(){
		global $woo_options;
		// Add the Homepage Intro
		if (isset($woo_options['woo_hometagline_enable']) && $woo_options['woo_hometagline_enable'] == "true" && is_front_page() ) {
		?>
			
			<div id="hometagline">
				<div id="inner">
					<?php if(isset($woo_options['woo_hometagline_title']) && !empty($woo_options['woo_hometagline_title'])){?>
						<h2><?php echo $woo_options['woo_hometagline_title'] ;?></h2>
					<?php } ?>
					
					<?php if(isset($woo_options['woo_hometagline_text']) && !empty($woo_options['woo_hometagline_text'])){?>
						<div class="tagline"><?php echo stripslashes($woo_options['woo_hometagline_text']);?></div>
					<?php } ?>
				
				<?php if (  is_user_logged_in() && current_user_can('manage_options') ) { ?>
				<a href="/wp-admin/admin.php?page=woothemes#woo-option-homepagesettings"><!--Edit tagline (click here, then Homepage settings tab)--></a>
				<?php } ?>	
				</div>
			</div>
			<div class="fix"></div>
				
		<?php 
		} // if hometagline is true
	} // end function
}
// END HOME TAGLINE

// HOME CTA
if ( ! function_exists( 'add_homecta' ) ) {
	function add_homecta(){
		global $woo_options;
		// Add the Homepage Intro
		if (isset($woo_options['woo_homecta_enable']) && $woo_options['woo_homecta_enable'] == "true" && is_front_page()  ) {
		?>
			<a href="<?php echo $woo_options['woo_homecta_link'];?>" class="mainButton">
			<div id="homecta" style="background-image: url('<?php echo $woo_options['woo_homecta_image'] ;?>')">
				<?php if(isset($woo_options['woo_homecta_title']) && !empty($woo_options['woo_homecta_title'])){?>
					<h2 class="title"><?php echo stripslashes(__($woo_options['woo_homecta_title'],'woothemes'));?></h2>
				<?php } ?>
				<?php if(isset($woo_options['woo_homecta_message']) && !empty($woo_options['woo_homecta_message'])){?>
					<div class="message"><?php echo $woo_options['woo_homecta_message'];?>
					</div>
					
					<?php if(isset($woo_options['woo_homecta_buttontext']) && !empty($woo_options['woo_homecta_buttontext'])){?>
					<div class="cta-button">
						<a href="<?php echo $woo_options['woo_homecta_link'];?>" class="mainButton">
						<?php echo $woo_options['woo_homecta_buttontext'];?>
						</a> 
					</div>
					<?php } ?>
				
				<?php } ?>
				
				<div class="fix"></div>
				
			</div>
			</a>
			<div class="fix"></div>
				
		<?php 
		} // if hometagline is true
	} // end function
}

// HOMEPAGE FEATUREBOXES
if ( ! function_exists( 'add_feature_boxes' ) ) {
	function add_feature_boxes(){
		global $woo_options;
		// Add the Homepage Intro
		if ( isset($woo_options['woo_featureboxes_enable']) && $woo_options['woo_featureboxes_enable'] == "true"  && ( /*is_home() ||*/ is_front_page() )) {
		?>
	
		<div id="feature-boxes">
			<?php 
			$ahcss = '';
			if(empty($woo_options['woo_feature3_title'])){$ahcss='-wide';}
			?>
			<?php if(!empty($woo_options['woo_featureboxes_title'])){ ?>
			<div class="featurebox-text">
				<h3 class="featuretitle"><?php echo stripslashes($woo_options['woo_featureboxes_title']); ?></h3>
				<p><?php echo stripslashes($woo_options['woo_featureboxes_content']); ?>
			</div>
			<?php } ?>
			<?php if(!empty($woo_options['woo_feature1_title'])){ ?>
			<div class="featurebox<?php echo $ahcss; ?> fbox1">
				<div class="boxtitle"><?php echo stripslashes($woo_options['woo_feature1_title']); ?></div>
				<?php if (isset($woo_options['woo_feature1_image']) && !empty($woo_options['woo_feature1_image']) ){ ?>
				<div class="boximage">
				<a href="<?php echo $woo_options['woo_feature1_link']; ?>">
				<?php woo_image('src='. $woo_options['woo_feature1_image'] .'&meta=&width=300&noheight'); ?>
				</a>
				</div>
				<?php } ?>
				<p><?php echo stripslashes($woo_options['woo_feature1_content']); ?>
				<?php if (isset($woo_options['woo_feature1_link']) && !empty($woo_options['woo_feature1_link']) && isset($woo_options['woo_feature1_linktext']) && !empty($woo_options['woo_feature1_linktext'])){ ?>
				<a href="<?php echo $woo_options['woo_feature1_link']; ?>"><?php echo stripslashes($woo_options['woo_feature1_linktext']); ?></a>
				<?php }?></p>
				<div class="fix"></div>
			</div>       
			<?php }?> 
			
			<?php if(!empty($woo_options['woo_feature2_title'])){ ?>
			<div class="featurebox<?php echo $ahcss; ?> fbox2">
				<div class="boxtitle"><?php echo stripslashes($woo_options['woo_feature2_title']); ?></div>
				<?php if (isset($woo_options['woo_feature2_image']) && !empty($woo_options['woo_feature2_image']) ){ ?>
				<div class="boximage">
				<a href="<?php echo $woo_options['woo_feature2_link']; ?>">
				<?php woo_image('src='. $woo_options['woo_feature2_image'] .'&meta=&width=300&noheight'); ?>
				</a>
				</div>
				<?php } ?>
				<p><?php echo stripslashes($woo_options['woo_feature2_content']); ?>
				<?php if (isset($woo_options['woo_feature2_link']) && !empty($woo_options['woo_feature2_link']) && isset($woo_options['woo_feature2_linktext']) && !empty($woo_options['woo_feature2_linktext'])){ ?>
				<a href="<?php echo $woo_options['woo_feature2_link']; ?>"><?php echo stripslashes($woo_options['woo_feature2_linktext']); ?></a>
				<?php }?> </p>
				<div class="fix"></div>
			</div>       
			<?php }?>   
			
			<?php if(!empty($woo_options['woo_feature3_title'])){ ?>
				<div class="featurebox<?php echo $ahcss; ?> fbox3">
					<div class="boxtitle"><?php echo stripslashes($woo_options['woo_feature3_title']); ?></div>
					<?php if (isset($woo_options['woo_feature3_image']) && !empty($woo_options['woo_feature3_image']) ){ ?>
					<div class="boximage">
					<a href="<?php echo $woo_options['woo_feature3_link']; ?>">
					<?php woo_image('src='. $woo_options['woo_feature3_image'] .'&meta=&width=300&noheight'); ?>
					</a>
					</div>

					<?php } ?>
					<p><?php echo stripslashes($woo_options['woo_feature3_content']); ?>
					<?php if (isset($woo_options['woo_feature3_link']) && !empty($woo_options['woo_feature3_link']) && isset($woo_options['woo_feature3_linktext']) && !empty($woo_options['woo_feature3_linktext'])){ ?>
					<a href="<?php echo $woo_options['woo_feature3_link']; ?>"><?php echo stripslashes($woo_options['woo_feature3_linktext']); ?></a>
					<?php }?> </p>
					<div class="fix"></div>
				</div>       
			<?php }?>
			<div class="fix"></div>
		</div>  
	<?php 
		// Add a stylesheet for this section
		wp_enqueue_style( 'home_features_css', get_stylesheet_directory_uri() . '/css/home-features.css' );
		} // if woo feature boxes are enabled
	}
}

/*------------------------------------------*/
/*  HOMEPAGE WIDGET AREAS                   */
/*------------------------------------------*/

// Additional Homepage Widgets
// Register widgetized areas
if ( ! function_exists( 'homepage_widgets_init' ) ) {
	function homepage_widgets_init() {
		if ( !function_exists('register_sidebars') ) {
			return;
		}
		
		// Widgetized homepage area
		register_sidebar(array('name' => 'Home Widget 1','id' => 'home_1','description' => "Left Homepage Widget", 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>')); 
		  
		register_sidebar(array('name' => 'Home Widget 2','id' => 'home_2','description' => "Middle Homepage Widget", 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>')); 
		
		register_sidebar(array('name' => 'Home Widget 3','id' => 'home_3','description' => "Right Homepage Widget", 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>')); 
		
		register_sidebar(array('name' => 'Home Widget 4','id' => 'home_4','description' => "Extra Homepage Widget", 'before_widget' => '<div id="%1$s" class="widget %2$s">','after_widget' => '</div>','before_title' => '<h3>','after_title' => '</h3>')); 	 
					
	}
}

if ( ! function_exists( 'load_home_widgets' ) ) {
	function load_home_widgets(){
		$wooopts = get_option( 'woo_options' );
		if ( isset($wooopts['woo_homewidgets_enable']) && $wooopts['woo_homewidgets_enable'] == "true" ) {
			// add in homepage widget areas
			add_action( 'init', 'homepage_widgets_init', 30);  
		} 
	}
}
add_action( 'init', 'load_home_widgets', 10 );  

// Widgetized Homepage section
if ( ! function_exists( 'add_widgets_to_homepage' ) ) {
	function add_widgets_to_homepage(){
		global $woo_options;
		// Add the Homepage Intro
		if ( isset($woo_options['woo_homewidgets_enable']) && $woo_options['woo_homewidgets_enable'] == "true"  && ( /*is_home() || */is_front_page() )) {
		?>
	
		<div id="home-widgets">
		<?php 
		$homewidgets = 0;
		if( is_active_sidebar('home_1') ) { $homewidget1 = true; $homewidgets++; }
		if( is_active_sidebar('home_2') ) { $homewidget2 = true; $homewidgets++; }
		if( is_active_sidebar('home_3') ) { $homewidget3 = true; $homewidgets++; }
		if( is_active_sidebar('home_4') ) { $homewidget4 = true; $homewidgets++; }

		if ($homewidgets < 3 ){ $hw_width = 'wide_'.$homewidgets;}
		?>
		
		<?php if( is_active_sidebar('home_1') ) { ?>
			<div class="homewidget box1 <?php echo $hw_width; ?>">
				<?php
					woo_sidebar_inside_before();
					woo_sidebar('home_1');
					woo_sidebar_inside_after();
				?>
				<div class="fix"></div>
			</div>
		<?php } ?>    
		
		<?php if( is_active_sidebar('home_2') ) { ?>
			<div class="homewidget box2 <?php echo $hw_width; ?>">
				<?php
					woo_sidebar_inside_before();
					woo_sidebar('home_2');
					woo_sidebar_inside_after();
				?>
				<div class="fix"></div>
			</div>       
		<?php } ?>  
		
		<?php if( is_active_sidebar('home_3') ) { ?>
			<div class="homewidget box3 <?php echo $hw_width; ?>">
				<?php
					woo_sidebar_inside_before();
					woo_sidebar('home_3');
					woo_sidebar_inside_after();
				?>
				<div class="fix"></div>
			</div>       
		<?php } ?> 
		
		<?php if( is_active_sidebar('home_4') ) { ?>
			<div class="homewidget box4 <?php echo $hw_width; ?>">
				<?php
					woo_sidebar_inside_before();
					woo_sidebar('home_4');
					woo_sidebar_inside_after();
				?>
				<div class="fix"></div>
			</div>       
		<?php } ?>  
	
			<div class="fix"></div>
		</div>  
	<?php  
		
		} // if home widgets are enabled
	}
}

add_action( 'wp_enqueue_scripts', 'load_homeblocks_css', 10 ); 
function load_homeblocks_css() {
	global $woo_options;
	if (!is_front_page()){
		return;
	}
	// Home Intro
	if ( isset($woo_options['woo_homeintro_enable']) && $woo_options['woo_homeintro_enable'] == "true" ) {
		wp_enqueue_style( 'home_intro_css', get_stylesheet_directory_uri() . '/css/home-intro.css' );
	}

	// Home Video
	if (isset($woo_options['woo_homevideo_enable']) && $woo_options['woo_homevideo_enable'] == "true" ) {
		wp_enqueue_style( 'home_video_css', get_stylesheet_directory_uri() . '/css/home-video.css' );
	}

	// Home Tagline
	if (isset($woo_options['woo_hometagline_enable']) && $woo_options['woo_hometagline_enable'] == "true" ) {
		wp_enqueue_style( 'home_tagline_css', get_stylesheet_directory_uri() . '/css/home-tagline.css' );
	}

	// Home CTA
	if (isset($woo_options['woo_homecta_enable']) && $woo_options['woo_homecta_enable'] == "true" ) {
		wp_enqueue_style( 'home_cta_css', get_stylesheet_directory_uri() . '/css/home-cta.css' );
	}

	// Home Features
	if ( isset($woo_options['woo_featureboxes_enable']) && $woo_options['woo_featureboxes_enable'] == "true" ) {
		wp_enqueue_style( 'home_features_css', get_stylesheet_directory_uri() . '/css/home-features.css' );
	}

	// Home Widgets
	if ( isset($woo_options['woo_homewidgets_enable']) && $woo_options['woo_homewidgets_enable'] == "true" ) {
		wp_enqueue_style( 'home_widgets_css', get_stylesheet_directory_uri() . '/css/home-widgets.css' );
	}
}
//
/*************************/	
/*                       */
/*        POSTS          */	
/*                       */
/*************************/	
//

// POSTS COUNTS ON FRONT PAGE
if ( ! function_exists( 'adjust_home_posts_count' ) ) {
	function adjust_home_posts_count($query_args) {
		global $woo_options;
		if ( isset( $woo_options['woo_magazine_limit'] ) && ( $woo_options['woo_magazine_limit'] != '' ) && is_front_page() ) {
			$query_args['posts_per_page'] = intval( $woo_options['woo_magazine_limit'] );
		} else {
			$query_args['posts_per_page'] = intval( $woo_options['woo_blog_limit'] );
		}
		return $query_args;
	}
}
add_filter('woo_blog_template_query_args', 'adjust_home_posts_count', 10);


// POST META
// customized copy of woo_post_meta
//created filter for changing order of the meta pieces. Replace whole function in framework
// Framework Innovation - debugged var errors
if ( ! function_exists( 'woo_post_meta' ) ) { 
	function woo_post_meta() {
	global $woo_options, $post;
	
 	if ( is_page() ) { return; }
	
	// get the symbols or labels for the metas
	if ($woo_options['woo_postmeta_labels']== "true"){		
		$authortag = $woo_options['woo_postmeta_authortag'];
		$datetag = $woo_options['woo_postmeta_datetag'];
		$cattag = $woo_options['woo_postmeta_cattag'];
		$tagtag = $woo_options['woo_postmeta_tagtag']; 
		}
	if (isset($woo_options['woo_postmeta_icons']) && $woo_options['woo_postmeta_icons']== "true"){		
		$authorclass = 'author';
		$dateclass = 'date';
		$catclass = 'cats';
		$tagclass = 'tags';
		$commentclass = 'comments';
	} else {
		$authorclass = '';
		$dateclass = '';
		$catclass = '';
		$tagclass = '';
		$commentclass = '';
	}	
	
		// output the author link
		$theauthor = do_shortcode('[post_author_posts_link]');
		// output the date
		$thedate = do_shortcode('[post_date]');
		// output the cats
		$thecats = do_shortcode('[post_categories before=""]');
		// output the tags
		$thetags = get_the_tag_list( '', ', ', '' );
		// comments
		$thecomments = do_shortcode('[post_comments before=""]');
		
	$post_info = '<div class="meta-1">';
	
	// Author Node
	if (isset($woo_options['woo_show_postmeta_by']) && $woo_options['woo_show_postmeta_by']== "true"){
		$author_info = '<p class="'.$authorclass.' icon"> <span class="small postmeta-author"> '.$authortag.' </span> '.$theauthor.'</p>';
	} else {$author_info = '';}
	
	// Cats Node
	if ($woo_options['woo_show_postmeta_incats']== "true"){	
		$cat_info = '<p class="'.$catclass.' icon"><span class="small postmeta-cats"> '.$cattag.' </span> '.$thecats.'</p>';
	} else {$cat_info = '';}
	
	// Date Node
	if ($woo_options['woo_show_postmeta_on']== "true"){
		$date_info = '<p class="'.$dateclass.' icon"><span class="small postmeta-date icon-date"> '.$datetag.' </span> '.$thedate.' </p>';
	} else {$date_info = '';}
	
	// Tags Node
	if ($woo_options['woo_show_postmeta_intags']== "true" && $thetags != ''){	
		$tags_info = '<div class="meta-2"><p class="'.$tagclass.' icon"><span class="small postmeta-tags"> '.$tagtag.' </span> '.$thetags.'</p></div>';
	} else {$tags_info = '';}
	
	$default_order = '[x[author]x] [x[cat]x] [x[date]x] [x[tags]x]';
	$order = apply_filters('change_postmeta_node_order',$default_order);
	
	$metas = str_replace(array('[x[author]x]','[x[cat]x]','[x[date]x]','[x[tags]x]'),array($author_info,$cat_info,$date_info,$tags_info),$order);
	
	$post_info .= $metas;

	$post_info .= '</div>';
	if ($woo_options['woo_show_postmeta_incomments']== "true" && is_single() ){	
		$post_info .= '<p class="'.$commentclass.' icon"><span class="small postmeta-comments"> '.$thecomments.' </span></p>';
	}
	//$post_info .= '[post_edit]';
	printf( '<div class="post-meta">%s</div>' . "\n", apply_filters( 'woos_filter_post_meta', $post_info ) );
 } // End woo_post_meta()
}

if ( ! function_exists( 'liketweet_position' ) ) { 
	function liketweet_position() {
		global $woo_options;
	
		$allowed = false;
		// show this only in full articles unless the woo option is checked for show in excerpts
		if( is_single() || ( is_archive() && isset($woo_options['woo_liketweet_in_excerpts']) && $woo_options['woo_liketweet_in_excerpts']== "true") ){
			$allowed = true;
		}
	
		if( is_front_page() && isset($woo_options['woo_liketweet_in_excerpts']) && $woo_options['woo_liketweet_in_excerpts']== "true") {
			$allowed = true;
		}
			
		if( is_home() && isset($woo_options['woo_liketweet_in_excerpts']) && $woo_options['woo_liketweet_in_excerpts']== "true") {
			$allowed = true;
		}
	
		if(is_page_template('template-blog.php') && isset($woo_options['woo_liketweet_in_excerpts']) && $woo_options['woo_liketweet_in_excerpts']== "true") {
			$allowed = true;
		}
		if(is_page() && isset($woo_options['woo_liketweet_in_pages']) && isset($woo_options['woo_liketweet_in_pages']) &&  $woo_options['woo_liketweet_in_pages']== "true") {
			$allowed = true;
		}	
		if('mbt_books' == get_post_type() && $woo_options['woo_liketweet_in_books']== "true") {
			$allowed = true;
		}
			
		if($allowed == true){
	
			if( 'mbt_books' == get_post_type()){
				add_action('woo_post_inside_after', 'add_like_and_tweets', 20);
			} elseif( isset($woo_options['woo_liketweet_placement']) && $woo_options['woo_liketweet_placement']=='before'){
				add_action('woo_post_inside_before', 'add_like_and_tweets', 5);
			} else {
				add_action('woo_post_inside_after', 'add_like_and_tweets', 20);
			}
		}
	}
}
include(STYLESHEETPATH . '/includes/post-arrange.php'); 

///
/*************************/	
/*                       */
/*   POST ARCHIVES       */	
/*                       */
/*************************/	
//

// READ MORE
// changing how post more function works
// framework innovation - added filters to read more
if ( ! function_exists( 'mess_with_woo_post_more' ) ) { 
	function mess_with_woo_post_more($content){
		global $woo_options;
		$html = '<span class="read-more"><a class="button" href="'.get_permalink().'">'.apply_filters('read_more_of_this','read more').'</a></span>';
		$html .= '<div class="fix"></div>';
		return $html;
	}
}
add_filter('woo_post_more','mess_with_woo_post_more', 20);


// making articles end with clear both
if ( ! function_exists( 'woo_clear_both' ) ) { 
	function woo_clear_both(){
		echo '<div class="fix"></div>';
	}
}
add_action( 'woo_post_inside_after', 'woo_clear_both', 3000);

// Woo Author
function woo_author(){
        // Author box single post page
        if ( is_single() && get_option( 'woo_disable_post_author' ) != 'true' ) { add_action( 'woo_post_inside_after', 'woo_author_box', 10 ); }
        // Author box author page
        if ( is_author()  && get_option( 'woo_disable_post_author' ) != 'true'  ) { add_action( 'woo_loop_before', 'woo_author_box', 10 ); }
} // End woo_author()


//
/*************************/	
/*                       */
/*    REGISTER SIDEBARS  */	
/*                       */
/*************************/	
//

// Register widgetized areas
if ( ! function_exists( 'the_widgets_init' ) ) {
	function the_widgets_init() {
	    if ( ! function_exists( 'register_sidebars' ) )
	        return;
	
		// Widgetized sidebars
	    register_sidebar( array( 'name' => __( 'Primary', 'woothemes' ), 'id' => 'primary', 'description' => __( 'The default primary sidebar for your website, used in two or three-column layouts.', 'woothemes' ), 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>' ) );	
		register_sidebar( array( 'name' => __( 'Homepage', 'woothemes' ), 'id' => 'homepage', 'description' => __( 'Optional widgetized homepage (displays only if widgets are added here).', 'woothemes' ), 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>' ) );	
		
		// Footer widgetized areas
		$total = get_option( 'woo_footer_sidebars', 4 );
		if ( ! $total ) $total = 4;
		for ( $i = 1; $i <= intval( $total ); $i++ ) {
			register_sidebar( array( 'name' => sprintf( __( 'Footer %d', 'woothemes' ), $i ), 'id' => sprintf( 'footer-%d', $i ), 'description' => sprintf( __( 'Widgetized Footer Region %d.', 'woothemes' ), $i ), 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<h3>', 'after_title' => '</h3>' ) );
		}
		
	} // End the_widgets_init()
}

add_action( 'init', 'the_widgets_init' );  


//
/*************************/	
/*                       */
/*        FOOTER         */	
/*                       */
/*************************/	
//


// if footer right (credit) contains nothing then make footer left full width
if ( ! function_exists( 'full_width_footer_left' ) ) {
	function full_width_footer_left(){
		global $woo_options;
		// see if anything is registered to to the footer right hook
		$hooks = what_hooks_here('woo_footer_right');
		
		// see if there is text registered in this action hook
		if ( ( isset($woo_options['woo_footer_right_text']) && $woo_options['woo_footer_right_text'] != "" ) || $hooks>1 ) {	
			return;
		}
		// add a style to make footer left Full Width
		$thestyles = ' #footer #copyright { max-width: 100% !important ; } ';
		$newstyle = "\n<!-- Footer Custom Styling -->\n<style type=\"text/css\">\n" . $thestyles . "</style>\n<!-- /Footer Custom Styling -->\n\n";
	
		echo $newstyle;
	}
}
add_action( 'wp_head', 'full_width_footer_left', 375); // Add custom styling	


//SIGNUP BOX
// add the signup box widget as a template tag
if ( ! function_exists( 'add_signup_box' ) ) {
	function add_signup_box() {
		the_widget('CastleNewsletterSignUpWidget');
	}
}

// load all the required homepage sections according to priority
if ( ! function_exists( 'load_homepage_sections' ) ) {
	function load_homepage_sections() {
		global $woo_options;
		$woo_hometagline_priority = isset($woo_options['woo_hometagline_priority']) ? $woo_options['woo_hometagline_priority'] : 10;
		$woo_homecta_priority = isset($woo_options['woo_homecta_priority']) ? $woo_options['woo_homecta_priority'] : 10;
		$woo_homeintro_priority = isset($woo_options['woo_homeintro_priority']) ? $woo_options['woo_homeintro_priority'] : 10;
		$woo_homevideo_priority = isset($woo_options['woo_homevideo_priority']) ? $woo_options['woo_homevideo_priority'] : 10;
		$woo_featureboxes_priority = isset($woo_options['woo_featureboxes_priority']) ? $woo_options['woo_featureboxes_priority'] : 10;
		$woo_homewidgets_priority = isset($woo_options['woo_homewidgets_priority']) ? $woo_options['woo_homewidgets_priority'] : 10;
		// MyBookTable extra features
		if( function_exists('mbt_init')){ // mybooktable must be active
		$woo_featurebook_priority = isset($woo_options['woo_featurebook_priority']) ? $woo_options['woo_featurebook_priority'] : 10;
		$woo_booksec_priority = isset($woo_options['woo_booksec_priority']) ? $woo_options['woo_booksec_priority'] : 10;
		} // end MyBookTable extra features
		add_action('woo_homepage_blocks', 'add_hometagline', $woo_hometagline_priority);
		add_action('woo_homepage_blocks', 'add_homecta', $woo_homecta_priority);
		add_action('woo_homepage_blocks', 'add_homeintro', $woo_homeintro_priority);
		add_action('woo_homepage_blocks', 'add_homevideo', $woo_homevideo_priority);
		add_action('woo_homepage_blocks', 'add_feature_boxes', $woo_featureboxes_priority);
		add_action('woo_homepage_blocks', 'add_widgets_to_homepage', $woo_homewidgets_priority);	
		// MyBookTable extra features
	//	if( function_exists('mbt_init')){ // mybooktable must be active
	//	add_action('woo_homepage_blocks', 'add_home_featuredbook', $woo_featurebook_priority);
	//	add_action('woo_homepage_blocks', 'add_booktable_genres', $woo_booksec_priority);
	//	} // end MyBookTable extra features
	
	}
}
add_action('woo_head', 'load_homepage_sections', 20);


/*********************************/	
/*                               */
/*          Misc Functions       */	
/*                               */
/*                               */
/*********************************/	

//add post dates at end of post
//add_filter('the_content','post_date_after', 5);
if ( ! function_exists( 'post_date_after' ) ) {
	function post_date_after($content) {
	
		global $woo_options, $post;
			if (is_singular('post')) {
				$datetag = $woo_options['woo_postmeta_datetag'];
	
				if ($woo_options['woo_postmeta_icons']== "true"){		
					$dateclass = 'date';
				}	
	
				// output the date
				$thedate = do_shortcode('[post_date]');
	
				$content .= '<p class="'.$dateclass.' icon"> '.$thedate.' </p>';
			}
			return $content;
	}
}

// Add a True Mullet
// also add the woo opt below to the framework 
function is_mulletized() {
	global $woo_options, $wp_query;
	$woo_image_priority = isset($woo_options['woo_arcimage_priority']) ? $woo_options['woo_arcimage_priority'] : 20;
	// is mullet activated for home and this is home?
	// is mullet activated for blog page and this is blogpage?
	// is mullet active for all archives?
	if (isset($woo_options['woo_mullet_enable']) && $woo_options['woo_mullet_enable'] == "true" && 'post' == get_post_type() && !is_single()) {
	// is this the first in the loop?
		$first_value = reset($wp_query->posts);
		//print_r('<pre>1');  print_r($first_value->ID); print_r('2</pre>');
		if($first_value->ID == get_the_ID() ){
			remove_action('woo_post_inside_before','load_post_image',$woo_image_priority);
			add_filter('woo_post_more','no_more_post_more',60);
			return true;
		}
	} // end if woo option set
	add_action('woo_post_inside_before','load_post_image',$woo_image_priority);
	remove_filter('woo_post_more','no_more_post_more',60);
	return false;
}

// Kill the post more
// changing how post more function works
if ( ! function_exists( 'no_more_post_more' ) ) { 
	function no_more_post_more($content){ 
		return '';
	}
}

// function to return true if this is a certain template. This works where is_page_template fails
// add this to functions
function is_my_template($mytemplate='front-page.php'){
	global $template;
	$sty_dir = get_stylesheet_directory();
	$path = $sty_dir.'/'.$mytemplate;
	if ($template == $path){
		return true;	
	}
	return false;
}

// add social media functions
// output the sharethis javascript generated from http://sharethis.com/publishers/get-sharing-tools#
//  - removed this from liketweet-functions and sharethis-functions and added it in template-sections
if ( ! function_exists( 'output_sharethis_js' ) ) { 
	function output_sharethis_js(){
		global $woo_options;
		if( (!isset($woo_options['woo_use_sharethis']) || $woo_options['woo_use_sharethis']=='false')&&(!isset($woo_options['woo_use_liketweets']) || $woo_options['woo_use_liketweets']=='false')){
			return;
		}

	//	if( /*!is_single() && !is_archive() is_reallyhome()*/){
	//		return false;
	//	}
		$output = "";
		
$output .= '		
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "823aa72e-9bc0-4431-a2f8-1d9ef5c829a8", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
';
		echo $output;
	}
}

add_action('wp_head','output_sharethis_js',100);


// add social media functions
include(STYLESHEETPATH . '/includes/sharethis-functions.php'); // support for sharethis buttons
include(STYLESHEETPATH . '/includes/liketweet-functions.php'); // support for sharethis buttons

?>