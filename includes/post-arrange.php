<?php
function load_post_title(){
	global $woo_options;
	$title_before = '<h1 class="title">';
	$title_after = '</h1>';
	
	if ( !is_single() ) {
		$title_before = '<h2 class="title">';
		$title_after = '</h2>';
		$title_before = $title_before . '<a href="' . esc_url( get_permalink( get_the_ID() ) ) . '" rel="bookmark" title="' . the_title_attribute( array( 'echo' => 0 ) ) . '">';
		$title_after = '</a>' . $title_after;
	}
	echo '<header>';
		the_title( $title_before, $title_after ); 
	echo '</header>';		
} 
 

// Post Image

function remove_woo_single_image(){
	if( 'post' == get_post_type() || ( is_home() || is_front_page() || is_post_type_archive('post') || is_page_template('template-blog.php') || is_page_template('page-blog.php')|| is_page_template('template-magazine.php') )){ 
 	remove_action( 'woo_post_inside_before', 'woo_display_post_image', 10 );
	}
}
add_action('woo_post_inside_before','remove_woo_single_image',2);

function load_post_image(){
	global $woo_options;
	$settings = array(
		'thumb_w' => 100,
		'thumb_h' => 100,
		'thumb_align' => 'alignleft',
		'post_content' => 'excerpt',
		'comments' => 'both'
	);

	$settings = woo_get_dynamic_values( $settings );
	if ( 'content' != $settings['post_content'] && !is_singular() ) {
		if( isset($settings['thumb_h'] ) && ($settings['thumb_h'] != '')) {
			woo_image('width='.esc_attr($settings['thumb_w']).'&noheight=false&height='.esc_attr($settings['thumb_h']).'&class=thumbnail '.esc_attr( $settings['thumb_align']));
		} else {	
			woo_image('width='.esc_attr($settings['thumb_w'] ).'&noheight=true&class=thumbnail '.esc_attr( $settings['thumb_align']));
		}
	} else {
		woo_display_post_image();
	}
}

// load post meta
function load_the_post_meta(){
	global $woo_options;
	if(($woo_options['woo_show_postmeta']) == "true" ) {
		woo_post_meta();
	}
}

// framework innovation - added is page template page-blog
function arrange_my_post() {
	global $woo_options, $wp_query;
	// arrange for archive posts
	
	if( 'post' == get_post_type() || ( is_home() || is_front_page() || is_post_type_archive('post') || is_page_template('template-blog.php') || is_page_template('page-blog.php')|| is_page_template('template-magazine.php') )){ 
		if ( !is_single()){
			$woo_arctitle_priority = isset($woo_options['woo_arctitle_priority']) ? $woo_options['woo_arctitle_priority'] : 10;
			$woo_arcimage_priority = isset($woo_options['woo_arcimage_priority']) ? $woo_options['woo_arcimage_priority'] : 20;
			$woo_arcmeta_priority = isset($woo_options['woo_arcmeta_priority']) ? $woo_options['woo_arcmeta_priority'] : 30;
		
			add_action('woo_post_inside_before','load_post_title',$woo_arctitle_priority);
			add_action('woo_post_inside_before','load_post_image',$woo_arcimage_priority);
			add_action('woo_post_inside_before','load_the_post_meta',$woo_arcmeta_priority);
		} else {
			$woo_posttitle_priority = isset($woo_options['woo_posttitle_priority']) ? $woo_options['woo_posttitle_priority'] : 10;
			$woo_postimage_priority = isset($woo_options['woo_postimage_priority']) ? $woo_options['woo_postimage_priority'] : 20;
			$woo_postmeta_priority = isset($woo_options['woo_postmeta_priority']) ? $woo_options['woo_postmeta_priority'] : 30;
		
			add_action('woo_post_inside_before','load_post_title',$woo_posttitle_priority);
			add_action('woo_post_inside_before','load_post_image',$woo_postimage_priority);
			add_action('woo_post_inside_before','load_the_post_meta',$woo_postmeta_priority);
		}
	}
}
add_action('woo_head', 'arrange_my_post', 30);
?>