<?php	
function mc_con(){
	global $woo_options;

	//API Key - see http://admin.mailchimp.com/account/api
	$apikey = ( isset($woo_options['woo_mc_api_key']) && !empty($woo_options['woo_mc_api_key']) ) ? $woo_options['woo_mc_api_key'] : '';
	
	// A List Id to run examples against. use lists() to view all
	// Also, login to MC account, go to List, then List Tools, and look for the List ID entry
	$listId = (isset($woo_options['woo_mc_list_id']) && !empty($woo_options['woo_mc_list_id']) ) ? $woo_options['woo_mc_list_id'] : '';
	// A Campaign Id to run examples against. use campaigns() to view all
	$campaignId = '';

	//some email addresses used in the examples:
	$my_email = (isset($woo_options['woo_contactform_email']) && !empty($woo_options['woo_contactform_email']) ) ? $woo_options['woo_contactform_email'] : '';
   // $boss_man_email = 'INVALID@example.com';

	//just used in xml-rpc examples
	$apiUrl = 'http://api.mailchimp.com/1.3/';

	$mc_connect = array(
		'apikey' => $apikey,
		'listId' => $listId,	
		'campaignId' => $campaignId,
		'my_email' => $my_email,
		'apiUrl' => $apiUrl,
	);
	return $mc_connect;
}

?>
