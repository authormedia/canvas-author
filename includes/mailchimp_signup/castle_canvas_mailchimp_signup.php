<?php  
/*
Theme Integration of Plugin Named Castle Canvas Mailchimp Signup
Description: A simple integration of the mailchimp signup API (PHP MCAPI 1.3 - http://api.mailchimp.com/1.3/.) This is designed to work with Woo Theme Options and also allows adding groups

*/

function mc_api_call($type='comment'){
	global $woo_options;
	$wooextra = '';
	if ($type == 'home'){$wooextra = '_home';}

	require_once("config.inc.php");		
	require_once("MCAPI.class.php");
	
	$con = mc_con();
	
	$apikey = $con['apikey'];
	$listId = $con['listId'];
	$campaignId = $con['campaignId'];
	$my_email = $con['my_email'];
	$apiUrl = $con['apiUrl'];
	
		
	// grab an API Key from http://admin.mailchimp.com/account/api/
	$api = new MCAPI($apikey); 
	// List the groups
	//print_r($woo_options['woo_mc_use_groupings_home'] );
	if( (( $type == 'reg' && isset($woo_options['woo_mc_use_groupings']) && $woo_options['woo_mc_use_groupings'] == 'true'  ) || ( $type == 'home' && isset($woo_options['woo_mc_use_groupings_home']) && $woo_options['woo_mc_use_groupings_home'] == 'true'  ) ) && ( !isset($_POST['submit']) && !isset($_POST['ajax'])) ) { 
		
	//cache request
	$transient_key = "_mailchimp_groups";
	// If cached (transient) 
	$cached = get_transient( $transient_key );
	if ( isset($cached) && !empty($cached) ){
		$groupys = $cached;
		//print_r($cached );
	} else {
		$groupys = $api->listInterestGroupings($listId);
		set_transient( $transient_key, $groupys, 60*60*24 );
	}
		if (isset($groupys) && !empty($groupys) ){
			$gcnt = 0;
			echo '<div class="mc-groups">';
			foreach ($groupys as $groupy){
				$gcnt++;
				echo '<div class="groups-title">'.$groupy['name'].'</div>';
				echo '<input name="thegroups-'.$gcnt.'" type="hidden" value="'.$groupy['name'].'" />';
				$groupitems = $groupy['groups'];
				if (isset($groupitems) && !empty($groupitems) ){
					echo '<ul id=grouplist>';
					$cnt = 0;
					$groupcnt = count($groupitems);
					// get the selected group  to check by default
					$sel_group = (isset($woo_options['woo_mc_sel_group']) && !empty($woo_options['woo_mc_sel_group']) && is_numeric($woo_options['woo_mc_sel_group']) && $woo_options['woo_mc_sel_group'] <= $groupcnt ? $woo_options['woo_mc_sel_group'] : 1);
					foreach ( $groupitems as $group) {
						//print_r('<pre>'); print_r($sel_group); print_r('</pre>');
						$cnt++;
						if ($cnt == $sel_group) { $checkit = 'checked="checked"';} else {$checkit = '';} 
						echo'<li><input id="mc-group-'.$cnt.'" class="checkbox" type="checkbox" value="'.$group['name'].'" name="group'.$cnt.'" '.$checkit.'">
						<label for="mc-group-'.$cnt.'">'.$group['name'].'</label></li>';
					} // end foreach
					echo '</ul>';
				} // end  if
				
			} // end foreach
		echo '</div>'; 
		} // end if
	} // END IF

	// On Submit
	if( isset($_POST['submit']) || isset($_POST['ajax']) ){
	
		// Validation
		if(!$_POST['ccmc-email']){ 
			return "No email address provided"; 
		} 
	
		if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*$/i", $_POST['ccmc-email'])) {
			return "Email address is invalid"; 
		}
		
		// add merge vars
		$mergeVars = ''; //array('ZIP'=>$_POST['zip']);
		
		if( isset($woo_options['woo_mc_subscribe_with_name']) && $woo_options['woo_mc_subscribe_with_name'] == 'true' ) {
			$mergeVars = array('FNAME'=>$_POST['ccmc-name']);
		}

		// To Do - Add Name field if it is selected

		// Groups Merge Vars
		if( (( $type == 'reg' && $woo_options['woo_mc_use_groupings'] ) || ( $type == 'home' && $woo_options['woo_mc_use_groupings_home'] ) )  ) {
			$getgroups = $_POST;
			$mygroups = array();
			foreach ($getgroups as $key => $value){
				$gotgroup = substr($key,0,5);
				if ($gotgroup=='group'){
					$mygroups[] = $value;
				}
			}
			if ($mygroups){
				$mc_groupings_groups = implode(',',$mygroups);
				$mc_groupings_groups = stripslashes($mc_groupings_groups);
				$mergeVars['GROUPINGS'] = array( 
					array( 'name' => $_POST['thegroups-1'], 'groups' => $mc_groupings_groups )
				);
					 // print_r($mc_groupings_groups);
					//exit;
			} // end if
		}	
		
		if($api->listSubscribe($listId, $_POST['ccmc-email'], $mergeVars) === true) {
			// It worked!	
			
			if ( isset($woo_options['woo_text_after_signup'.$wooextra]) && !empty($woo_options['woo_text_after_signup'.$wooextra]) ){ 
				return  '<div class="message">'.$woo_options['woo_text_after_signup'.$wooextra].'</div>'; 
			} else {
				return '<div class="message">Success! Check your email to confirm.</div>';
			}
			
		}else{
			// An error ocurred, return error message	
			return '<div class="message"> Error: ' . $api->errorMessage .'</div>';
		}
	}
	
}

	// Merge variables are the names of all of the fields your mailing list accepts
	// Ex: first name is by default FNAME
	// You can define the names of each merge variable in Lists > click the desired list > list settings > Merge tags for personalization
	// Pass merge values to the API in an array as follows
	
	//$mergeVars = array('FNAME'=>$_POST['fname'], 'LNAME'=>$_POST['lname'],'ADDRESS'=>$_POST['address'],'CITY'=>$_POST['city'],'STATE'=>$_POST['state'],						'ZIP'=>$_POST['zip']);


function output_mc_form($type='comment'){
		global $post, $woo_options;
		$wooextra = '';
		if ($type == 'home'){$wooextra = '_home';}
		echo  '<div class="mc_signup_form '.$type.'">';
	//	echo  '<div class="mc_title">'.$woo_options['woo_subscribe_widget_title'].'</div>';
		echo  '<form class="mailchimp_signup" id="mailchimp_signup" action="" method="post">';
		echo  ' <fieldset>';
			  
		echo  '<div class="response">';
		echo  mc_api_call($type); 	 // loads groups or response		
		echo  '</div>';
		
/*****************/	
/*   Form Name   */
/*****************/	
		
		// use name field
		if( ( isset($woo_options['woo_mc_subscribe_with_name'.$wooextra]) && $woo_options['woo_mc_subscribe_with_name'.$wooextra] == 'true') ) {
		
		// Name Field Label
		if( ( isset($woo_options['woo_name_label'.$wooextra]) && !empty($woo_options['woo_name_label'.$wooextra])) ) {
		echo  '<label for="ccmc-name" name="ccmc-name-label" id="ccmc-name-label" class="fieldlabel">'.$woo_options['woo_name_label'.$wooextra].'</label>';	
		} // end if
		
			$defaultname = ( isset($woo_options['woo_name_default_value'.$wooextra]) ? $woo_options['woo_name_default_value'.$wooextra] : '' );
			echo  '<input type="text" name="ccmc-name" id="ccmc-name" class="fieldinput" value="'.$defaultname.'" onfocus="if (this.value == \''.$defaultname.'\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \''.$defaultname.'\';}" />';	
		} // end if

/*****************/	
/*   Form Email  */
/*****************/	

		// Email Field Label
		if( ( isset($woo_options['woo_email_label']) && !empty($woo_options['woo_email_label'])) ) {
		echo  '<label for="ccmc-email" name="ccmc-email-label" id="ccmc-email-label" class="fieldlabel">'.$woo_options['woo_email_label'].'</label>';	
		} // end if


		$em_val = (isset($woo_options['woo_email_default_value']) && !empty($woo_options['woo_email_default_value']) ? $woo_options['woo_email_default_value'] : 'Email Address');
		echo  '<input type="text" name="ccmc-email" id="ccmc-email" class="fieldinput" value="'.$em_val.'" onfocus="if (this.value == \''.$em_val.'\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \''.$em_val.'\';}"/>';	
	//	
			
/*****************/	
/*   Form Submit */
/*****************/	
		echo  '<input type="submit" name="submit" value="'.$woo_options['woo_submit_button'.$wooextra].'" class="btn button"  />';
		
		//echo  '	<div class="after-mc-form">'.$woo_options['woo_text_after_signup'.$wooextra].'</div>'; 
		echo  ' </fieldset>';
		echo  '</form>';
		echo  '</div>';
		
		//return $output
}


//Add the Widget
include('widget-mc-subscribe.php');

//Add the Widget for home
include('widget-mc-home-subscribe.php');

// Widget CSS
if ( ! function_exists( 'mc_form_css_load' ) ) {
	function mc_form_css_load() {
		//$siteurl = get_option('siteurl');
		$url = get_stylesheet_directory_uri() . '/includes/mailchimp_signup/css/mcform-styles.css';
		wp_register_style( 'mcform-styles', $url);
		wp_enqueue_style('mcform-styles');
	}
}
add_action( 'wp_head', 'mc_form_css_load', 20 );

// add to comment form
/*
function add_subscribe_to_comments(){
	global $woo_options;
	if (!is_single()){
		return;
	}
	//print_r('ahhhhhhhhhhhh'.$woo_options['woo_add_to_comment_form']);exit;
	
	if( isset($woo_options['woo_add_to_comment_form']) && $woo_options['woo_add_to_comment_form'] == 'true') {
			//add_action('thesis_hook_after_comment_box',output_mc_form,20);
			add_action('comment_form','output_mc_form',5);
			add_action('comment_approved_','output_mc_form',10,1);
			add_action('comment_post', 'output_mc_form', 50, 2);
			//add_action('woo_post_after','output_mc_form',20);
		}
}

function load_add_subscribe_to_comments(){
	add_subscribe_to_comments();
}
add_action('wp_head','load_add_subscribe_to_comments',20);

*/

// Sad to have to write this but wordpress does not have a good way to eval if you are on the true home page. It often treats the home and blog page the same
if ( ! function_exists( 'is_reallyhome' ) ) {
	function is_reallyhome(){
		if ($_SERVER["REQUEST_URI"] == '/' ) { $reallyhome = true; } else {$reallyhome = false;}
		return $reallyhome;
	}
}

?>