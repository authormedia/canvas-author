<?php
/*---------------------------------------------------------*/
/* Featured Books Widget                                   */
/*---------------------------------------------------------*/

class MBT_Featured_Book_Plus extends WP_Widget {
	function MBT_Featured_Book_Plus() {
		$widget_ops = array('classname' => 'MBT_Featured_Book_Plus', 'description' => "Displays featured or random books with some extra features.");
		parent::WP_Widget('MBT_Featured_Book_Plus', 'MBT Featured Books PLUS', $widget_ops);
		add_action('admin_enqueue_scripts', array('MBT_Featured_Book_Plus', 'enqueue_widget_js'));
		$this->defaultargs = array('selectmode' => 'by_date', 'featured_books' => array(), 'image_size' => 'medium', 'num_books' => 1, 'show_buttons' => true, 'show_blurb' => true);
	}

	function enqueue_widget_js() {
		global $pagenow;
		if($pagenow == 'widgets.php') {
			wp_enqueue_script("mbt-widgets", plugins_url('js/widgets.js', dirname(dirname(__FILE__))), 'jquery', '', true);
		}
	}

	function widget($args, $instance) {
			/* Provide some defaults */
			$defaults = array( 'title' => '', 'image_size' => 'none');

			$instance = wp_parse_args( (array) $instance, $defaults );	
			extract( $args );
			extract($instance);
			
			$title = apply_filters('widget_title', $title);


		$num_books = intval($num_books);
		if($num_books > 10 or $num_books < 1) { $num_books = 1; }
		if(!empty($featured_book)) { $featured_books = array((int)$featured_book); }


		if($selectmode == 'manual_select' and !empty($featured_books)) {
			$books = array();
			foreach($featured_books as $featured_book) {
				$new_book = get_post($featured_book);
				if($new_book) { $books[] = $new_book; }
			}
		} else if($selectmode == 'random') {
			$wp_query = new WP_Query(array('post_type' => 'mbt_book', 'posts_per_page' => -1));
			$books = array();
			$keys = array_rand($wp_query->posts, $num_books);
			if(!is_array($keys)) { $keys = array($keys); }
			foreach($keys as $key) {
				$books[] = $wp_query->posts[$key];
			}
		} else {
			$wp_query = new WP_Query(array('post_type' => 'mbt_book', 'orderby' => 'date', 'posts_per_page' => $num_books));
			$books = $wp_query->posts;
		}
		if(!empty($books)) {
			echo $before_widget;
				
			// insert the title
			echo '<h3>'.$title.'</h3>'; 
			?> <ul class="mbt-featured-book-widget"> <?php
			foreach($books as $book) {
				$permalink = get_permalink($book->ID);
				?>
					<li class="mbt-featured-book-widget-book">
						<a href="<?php echo($permalink); ?>"><?php echo(get_the_title($book->ID)); ?></a>
						<?php if (isset($image_size) && $image_size != 'none' ){?>
						<div class="mbt-book-images"><a href="<?php echo($permalink); ?>"><?php echo(mbt_get_book_image($book->ID, array('class' => $image_size))); ?></a></div>
						<?php } 
						 if($show_blurb) { ?><div class="mbt-book-blurb"><?php echo(mbt_get_book_blurb($book->ID, true)); ?></div><?php } 
						 if($show_buttons) {
						 ?>
						<div class="mbt-book-buybuttons">
							<?php
								$buybuttons = mbt_get_buybuttons($book->ID, array('display' => 'featured'));
								if (isset($show_buttons) && $show_buttons ==  true ){
									echo(mbt_format_buybuttons($buybuttons));
								}
							?>
							<div style="clear:both;"></div>
						</div>
						<?php }?> 
					</li>
				<?php
			}
			?> </ul> <?php
		}

		echo('<div style="clear:both;"></div>');
		echo($args['after_widget']);
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['selectmode'] = strip_tags($new_instance['selectmode']);
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['image_size'] = $new_instance['image_size'];
		$instance['num_books'] = intval($new_instance['num_books']);
		$instance['show_buttons'] = (bool)$new_instance['show_buttons'];
		$instance['show_blurb'] = (bool)$new_instance['show_blurb'];
		$instance['featured_books'] = (array)json_decode($new_instance['featured_books']);
		unset($instance['featured_book']);
		return $instance;
	}

	function form($instance) {
		extract(wp_parse_args($instance, $this->defaultargs));
		if(!empty($featured_book)) { $featured_books = array((int)$featured_book); }
		?>

		<div class="mbt-featured-book-widget-editor" onmouseover="mbt_initialize_featured_book_widget_editor(this);">
		
		  	<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title (optional): </label>
			<input type="text" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr( $title ); ?>"   id="<?php echo $this->get_field_id('title'); ?>" />
			</p>


			<p>
				<label>Book image size:<br>
				<?php foreach(array('none', 'small', 'medium', 'large') as $size) { ?>
					<input type="radio" name="<?php echo($this->get_field_name('image_size')); ?>" value="<?php echo($size); ?>" <?php echo($image_size == $size ? ' checked' : ''); ?> ><?php echo(ucfirst($size)); ?><br>
				<?php } ?>
				</label>
			</p>
			<p>
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('show_blurb'); ?>" name="<?php echo $this->get_field_name('show_blurb'); ?>"<?php checked($show_blurb); ?> />
				<label for="<?php echo $this->get_field_id('show_blurb'); ?>">Show book blurb</label>
			</p>
			<p>
				<input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('show_buttons'); ?>" name="<?php echo $this->get_field_name('show_buttons'); ?>"<?php checked($show_buttons); ?> />
				<label for="<?php echo $this->get_field_id('show_buttons'); ?>">Show book buttons</label>
			</p>

			<p>
				<label for="<?php echo($this->get_field_id('selectmode')); ?>">Choose how to select the featured books:</label>
				<select class="MBT_Featured_Book_Plus_selectmode" name="<?php echo($this->get_field_name('selectmode')); ?>" id="<?php echo($this->get_field_id('selectmode')); ?>">
					<option value="by_date"<?php selected($selectmode, 'by_date'); ?>>Most Recent Books</option>
					<option value="manual_select"<?php selected($selectmode, 'manual_select'); ?>>Choose Manually</option>
					<option value="random"<?php selected($selectmode, 'random'); ?>>Random Books</option>
				</select>
			</p>
			<div class="mbt-featured-book-manual-selector" <?php echo($selectmode === 'manual_select' ? '' : 'style="display:none"'); ?>>
				<label for="mbt-book-selector">Select Books:</label></br>
				<select class="mbt-featured-book-selector">
					<option value=""> -- Choose One -- </option>
					<?php
						$wp_query = new WP_Query(array('post_type' => 'mbt_book', 'orderby' => 'title', 'order' => 'ASC', 'posts_per_page' => -1));
						if(!empty($wp_query->posts)) {
							foreach($wp_query->posts as $book) {
								echo '<option value="'.$book->ID.'">'.substr($book->post_title, 0, 25).(strlen($book->post_title) > 25 ? '...' : '').'</option>';
							}
						}
					?>
				</select>
				<input type="button" class="mbt-featured-book-adder button" value="Add" /><br>

				<?php
					echo('<ul class="mbt-featured-book-list">');
					foreach($featured_books as $featured_book) {
						$book = get_post($featured_book);
						if($book) {
							echo('<li data-id="'.$book->ID.'" class="mbt-book">'.substr($book->post_title, 0, 25).(strlen($book->post_title) > 25 ? '...' : '').'<a class="mbt-book-remover">X</a></li>');
						}
					}
					echo('</ul>');
				?>
				<input class="mbt-featured-books" id="<?php echo($this->get_field_id('featured_books')); ?>" name="<?php echo($this->get_field_name('featured_books')); ?>" type="hidden" value="<?php echo(json_encode($featured_books)); ?>">
			</div>
			<div class="mbt-featured-book-options" <?php echo($selectmode !== 'manual_select' ? '' : 'style="display:none"'); ?>>
				<p>
					<label>Number of Books:
						<input type="number" name="<?php echo($this->get_field_name('num_books')); ?>" value="<?php echo(intval($num_books ? $num_books : 1)); ?>"  min="1" max="10" <?php echo($image_size == $size ? ' checked' : ''); ?> >
					</label>
				</p>
			</div>
		</div>

		<?php
	}
}

function register_new_widget_after_old(){
	register_widget( 'MBT_Featured_Book_Plus' );
}
add_action('widgets_init','register_new_widget_after_old',10);

?>