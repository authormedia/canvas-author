<?php

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

//if(is_plugin_active('mybooktable-master/mybooktable.php')){

  /**
   * Add list categories
   *
   */	
function list_store_categories(){

	if( !function_exists('mbt_init')){ // mybooktable must be active
		return false;
	}

	//list terms in a given taxonomy using wp_list_categories
	$type         = 'mbt_book';
	$taxonomy     = 'mbt_genre';
	$orderby      = 'ID'; 
	$order        = 'DESC'; 
	$hierarchical = 0;      // 1 for yes, 0 for no
	$hide_empty   = 0;
	$pad_counts   = 0;
	
	$args = array(
	  'type'         => $type,
	  'taxonomy'     => $taxonomy,
	  'orderby'      => $orderby,
	  'order'        => $order,
	  'child_of'     => 0,
	  'parent'       => 0,
	  'hierarchical' => $hierarchical,
	  'hide_empty'   => $hide_empty,
      'pad_counts'   => $pad_counts
	);
	
	$thecats = get_categories($args);

	if ( !isset($thecats) || empty($thecats)){
		return false;
	}
		$output = '<div id="product-cats">';
		
		foreach($thecats as $mycat) { 
			$url = '/genre/'.$mycat->slug.'/';
			$output .= '<div class="prodcat">';
			$output .= '<a href="'.$url.'"><div class="image">';
			$img = mbt_genre_image($mycat->term_id);
			$output .= $img;
			$output .= '</div><div class="name">';
			$output .= $mycat->name;
			$output .= '</a></div></div>';
		}
		
		$output .= '</div><div style="clear:both;"></div>';
	
	return $output;
}

// Get the Book Genre Image
// based on taxonomy image plugin
/*
function woo_the_taxonomy_image($term_id) {
	if (!function_exists('taxonomy_image_plugin_get_associations')){
		return false;
	}
 	$associations = taxonomy_image_plugin_get_associations();
	$tt_id = absint( $term_id );
	$img_id = false;
	if ( array_key_exists( $tt_id, $associations ) ) {
		$img_id = absint( $associations[$tt_id] );
	}
	
	// get the img from taxonomy images and output with woo
	$img = wp_get_attachment_image_src( $img_id, $size, $icon );
	$img_src .= $img[0];
	$the_img = woo_image( 'width=120&noheight=true&class=thudmbnail&src='.$img_src.'&return=true');
	return $the_img;
 }
 */
 
 
 // Get the Book Genre Image
// based on Tim Zook's added custom field for genre image - yay Tim

function mbt_genre_image($term_id) {

	if( !function_exists('mbt_init')){ // mybooktable must be active
		return false;
	}

	$img_src = mbt_get_taxonomy_image('mbt_genre',$term_id); 
	// get the img from taxonomy images and output with woo
	//$img = wp_get_attachment_image_src( $img_id, $size, $icon );
	//$img_src .= $img[0];
	$the_img = woo_image( 'width=120&noheight=true&class=thudmbnail&src='.$img_src.'&return=true');
	return $the_img;
 }


// booktable genres on homepage
function add_booktable_genres(){
	global $woo_options;
	
	if( !function_exists('mbt_init')){ // mybooktable must be active
		return false;
	}

	// Add the Homepage Intro
	if ( isset($woo_options['woo_booksec_enable']) && $woo_options['woo_booksec_enable'] == "true" && ( is_home() || is_front_page() )) {
	?>
		
			<div id="home-books">
				<?php if(!empty($woo_options['woo_bookssec_title']) || !empty($woo_options['woo_bookssec_content'])){ ?>
				<div class="books-desc">
					<h3 class="books-title"><?php echo stripslashes($woo_options['woo_bookssec_title']); ?></h3>
					<p><?php echo stripslashes($woo_options['woo_bookssec_content']); ?></p>
				</div>
				
				<?php 
				} // end if title or content not empty
				
				if (function_exists('list_store_categories')){echo list_store_categories();} ?>
		
			</div>
		
	<?php } // if homeintro is true

} // end function

// booktable genres on homepage
function add_home_featuredbook(){
	global $woo_options;
	
	if( !function_exists('mbt_init')){ // mybooktable must be active
		return false;
	}
	
	// Add the Featured Book to Homepage
	if ( isset($woo_options['woo_featurebook_enable']) && $woo_options['woo_featurebook_enable'] == "true" && $woo_options['woo_featurebook_book'] > 1 && ( is_home() || is_front_page() )) {
	?>
		
			<div id="home-featured-book">
				<?php if(!empty($woo_options['woo_featurebook_title']) ){ ?>
				<div class="book">
					<h3 class="books-title"><?php echo stripslashes($woo_options['woo_featurebook_title']); ?></h3>
				</div>
				<?php  } // end if title not empty
				// get the featured book object
				 
				$thebook = get_post($woo_options['woo_featurebook_book']); 
					$thetitle = $thebook->post_title;
					$theexcerpt = $thebook->post_excerpt;
					$thelink = get_permalink($woo_options['woo_featurebook_book']);
					$thb = get_permalink( get_post_thumbnail_id( $woo_options['woo_featurebook_book'] ) );
					$image = wp_get_attachment_image_src(get_post_thumbnail_id($woo_options['woo_featurebook_book']), 'full');
					$width = ( !empty($woo_options['woo_featuredbook_image_size']) ? $woo_options['woo_featuredbook_image_size'] : 100 );
					$thumb = woo_image('width='.$width.'&noheight=true&src='.$image[0].'&force=true&return=true&class="" alignleft');
				?>
				<div class="image"><?php echo $thumb; ?></div>
				<div class="text">
					<h3><?php echo $thetitle; ?></h3>
					<?php echo $theexcerpt; ?>
					<div class="links">
					<a class="buybutton" href="<?php echo $thelink; ?>">Buy This Book &raquo; </a>
					<a class="morebutton" href="/booktable/">More Books &raquo; </a>
					</div>
				</div>
				<div style="clear:both;"></div>
			</div>
		
	<?php } // if homeintro is true 

} // end function

 
// alternate function for outputting book series 
function mbt_get_book_series_altered($post_id) {
	if( !function_exists('mbt_init')){ // mybooktable must be active
		return false;
	}

	$output = '';
	$series_all = wp_get_post_terms($post_id, 'mbt_series');
	if(!empty($series_all)) {
		foreach($series_all as $series) {
			$relatedbooks = new WP_Query(array('mbt_series' => $series->slug, 'post__not_in' => array($post_id)));
			if(!empty($relatedbooks->posts)) {

				$output .= '<div class="mbt-book-series">';
				$output .= '<div class="mbt-book-series-title">Other books in "'.$series->name.'":</div>';
				$output .= '<div class="series-books">';
				foreach($relatedbooks->posts as $relatedbook) {
				$image = wp_get_attachment_image_src(get_post_thumbnail_id($relatedbook->ID), 'book-image');
				//print_r($image); exit;

					$output .= '<div class="mbt-book">';
					$output .= '<a href="file:///C|/xampp/htdocs/garden/wp-content/themes/canvas-author/includes/'.get_permalink($relatedbook-&gt;ID).'">
					'.woo_image('width=90&noheight=true&src='.$image[0].'&return=true&class=thumbnail alignleft').'
					</a>';					
					
					$output .= '<div class="mbt-book-title"><a href="file:///C|/xampp/htdocs/garden/wp-content/themes/canvas-author/includes/'.get_permalink($relatedbook-&gt;ID).'">'.$relatedbook->post_title.'</a></div>';

					$output .= '<div class="clear:both"></div>';
					$output .= '</div>';
				}
				$output .= '</div></div>';
			}
		}
	}
	return $output;
}


// RSS Links on Genre archives
function book_genre_rss() {
global $wp_query;
	if( !function_exists('mbt_init')){ // mybooktable must be active
		return false;
	}

	$taxonomy_obj = $wp_query->get_queried_object();
	$term_id = $taxonomy_obj->term_id;
	$taxonomy_short_name = $taxonomy_obj->taxonomy;
	$image = '<img src="file:///C|/xampp/htdocs/garden/wp-content/themes/canvas-author/includes/'.get_stylesheet_directory_uri().'/images/ico-rss.png">';
	$output = '';
	$output .= '<span class="fr catrss">';
	//print_r($cat_id); exit;
	$output .= '<a href="file:///C|/xampp/htdocs/garden/wp-content/themes/canvas-author/includes/'.get_term_feed_link( $term_id, $taxonomy_short_name, '' ).'">'.$image.'</a></span>';
	return $output;
}

function remove_liketweets_from_bookspage() {
	global $wp_query;
	if( !function_exists('mbt_init')){ // mybooktable must be active
		return false;
	}

	if( isset($wp_query->queried_object->post_content) && $wp_query->queried_object->post_content == '[mbt_booktable]'){
		remove_action('woo_post_inside_before', 'add_like_and_tweets', 20);
		remove_action('woo_post_inside_after', 'add_like_and_tweets', 20);
	}
}
add_action('woo_head','remove_liketweets_from_bookspage',60);

function mbt_meta_lists($postID) {
	global $woo_options;
	if( !function_exists('mbt_init')){ // mybooktable must be active
		return false;
	}

	if(isset($woo_options['woo_liketweet_in_books']) && $woo_options['woo_liketweet_in_books'] == 'true' ){
		$content = add_like_and_tweets();
	}
	$content .= '<div class="mbt-book-meta">';
	
		echo(get_the_term_list($post->ID, 'mbt_author', "<strong>Authors:</strong> ", ", ", "<br /> ")); 
		echo(get_the_term_list($post->ID, 'mbt_series', "<strong>Series:</strong> ", ", ", "<br /> "));
		echo(get_the_term_list($post->ID, 'mbt_genre', "<strong>Genres:</strong> ", ", ", "<br /> "));
			 
	$content .= '</div>';
	return $content;
}

add_filter('mbt_single_book_socialmedia','mbt_sociable');
function mbt_sociable() { 
	if( function_exists( do_sociable() ) ){ do_sociable(); } 
}

// } // is plugin active