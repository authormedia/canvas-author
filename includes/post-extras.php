<?php
// 
/*
Several functions for adding features at the end of post singles
To DO
2. \
3. 
*/

//
/*************************/	
/*                       */
/*     Related Posts     */	
/*                       */
/*************************/	
//

function add_related_posts($direct = 0){
	global $woo_options;
	// make sure this feature is activated and this is a post single
	if ( isset($woo_options['woo_relatedposts_enable']) && $woo_options['woo_relatedposts_enable'] == "true"  && ( is_single() || $direct == 1 )) {
		$size =( isset($woo_options['woo_relatedposts_imagesize']) && !empty($woo_options['woo_relatedposts_imagesize']) ? $woo_options['woo_relatedposts_imagesize'] : 75);
		$numposts =( isset($woo_options['woo_relatedposts_numposts']) && !empty($woo_options['woo_relatedposts_numposts']) ? $woo_options['woo_relatedposts_numposts'] : 5);
		if ( isset($woo_options['woo_relatedposts_images']) && !empty($woo_options['woo_relatedposts_images']) ) {
			$img = 'image="'.$size.'"';
		}
		$related_posts = do_shortcode( '[my_new_related_posts limit="'.$numposts.'" '.$img.']' );
		echo '<div id="related-posts">';
		if ( isset($woo_options['woo_relatedposts_heading']) && !empty($woo_options['woo_relatedposts_heading']) ) {
			echo '<h3 class=rposts>'.$woo_options['woo_relatedposts_heading'].'</h3>';
		}
		echo $related_posts;
		echo '</div>';
	}
} 
add_action( 'woo_post_after','add_related_posts', 20 );

//
/*******************************/	
/*                             */
/*    Signup Form in Posts     */	
/*                             */
/*******************************/	
//

function add_mc_form_in_posts($direct = 0){
	global $woo_options;
	// make sure this feature is activated and this is a post single
	if ( isset($woo_options['woo_post_mc_enable']) && $woo_options['woo_post_mc_enable'] == "true"  && ( is_single() || $direct == 1 )) {
		echo '<div id="post-mc-form">';
		if ( isset($woo_options['woo_post_mc_heading']) && !empty($woo_options['woo_post_mc_heading']) ) {
			echo '<h3 class=mcpost-head>'.$woo_options['woo_post_mc_heading'].'</h3>';
		}
		// Text Before
		if ( isset($woo_options['woo_post_mc_text_before']) && !empty($woo_options['woo_post_mc_text_before']) ) {
			echo '<div class=mcpost-before>'.$woo_options['woo_post_mc_text_before'].'</div>';
		}
		// output the form - (home, reg, comment)
		echo output_mc_form('reg');
		// Text after
		if ( isset($woo_options['woo_post_mc_text_after']) && !empty($woo_options['woo_post_mc_text_after']) ) {
			echo '<div class=mcpost-after>'.$woo_options['woo_post_mc_text_after'].'</div>';
		}
		echo '</div>';
	}
} 
add_action( 'woo_post_after','add_mc_form_in_posts', 30);

// Add the appropriate css for the various added blocks
function load_postextras_css() {
	global $woo_options;
	if (!is_single()){
		return;
	}
	// Related Posts
	if ( isset($woo_options['woo_relatedposts_enable']) && $woo_options['woo_relatedposts_enable'] == "true" ) {
		wp_enqueue_style( 'relatedposts_css', get_stylesheet_directory_uri() . '/css/relatedposts.css' );
	}
	// Subscribe Form
	if ( isset($woo_options['woo_post_mc_enable']) && $woo_options['woo_post_mc_enable'] == "true" ) {
		wp_enqueue_style( 'post_mc_subscribe_css', get_stylesheet_directory_uri() . '/css/post_mc_subscribe.css' );
	}

}
add_action( 'wp_enqueue_scripts', 'load_postextras_css', 10 ); 



/*-----------------------------------------------------------------------------------*/
/*  Related Posts - Totally ripped off from woocanvas function in admin_shortcodes.  */
/*  Too bad there were not enough hooks
/*-----------------------------------------------------------------------------------*/
/*
*/

function new_related_posts ( $atts ) {
	global $post, $woo_options;
	
		wp_reset_query(); // Make sure we have a fresh query before we start.
	
		$defaults = array(
					'limit' => 4, 
					'image' => 0, 
					'float' => 'none'
				 );
	
		$atts = shortcode_atts( $defaults, $atts );
	
		// Sanitize float attribute.
		if ( isset( $atts['float'] ) && ! in_array( $atts['float'], array( 'none', 'left', 'right' ) ) ) { $atts['float'] = 'none'; }
	
		// Float translation array.
		$floats = array( 'none' => '', 'left' => 'fl', 'right' => 'fr' );
		$css_class = 'new-related-posts';
		extract( $atts );
		if ( $float != 'none' ) { $css_class .= ' ' . $floats[$float]; }
		
		$output = '';
		$post_type = get_post_type( $post->ID );
		$post_type_obj = get_post_type_object( $post_type );
		
		$taxonomies_string = 'post_tag, category';
		$taxonomies = array( 'post_tag', 'category' );
		
		if ( isset( $post_type_obj->taxonomies ) && count( $post_type_obj->taxonomies ) > 0 ) {
			$taxonomies_string = join( ', ', $post_type_obj->taxonomies );
			$taxonomies = $post_type_obj->taxonomies;
		}
	 	
	 	// Clean up our taxonomies for use in the query.
	 	if ( count( $taxonomies ) ) {
	 		foreach ( $taxonomies as $k => $v ) {
	 			$taxonomies[$k] = trim( $v );
	 		}
	 	}
	 	
	 	// Determine which terms we're going to relate to this entry.
	 	$related_terms = array();
	 	
	 	foreach ( $taxonomies as $t ) {
	 		$terms = get_the_terms( $post->ID, $t );
	 		
	 		if ( ! empty( $terms ) ) {
	 			foreach ( $terms as $k => $v ) {
	 				$related_terms[$t][$v->term_id] = $v->slug;
	 			}
	 		}
	 	}
		
		$specific_terms = array();
		foreach ( $related_terms as $k => $v ) {
			foreach ( $v as $i => $j ) {
				$specific_terms[] = $j;
			}
		}
		// get the size of the images
		$sizestyle = '';
		if(isset($woo_options['woo_relatedposts_imagesize']) && !empty($woo_options['woo_relatedposts_imagesize'])) {
			$imgsize = $woo_options['woo_relatedposts_imagesize'];
			// estimate width = 575
			$percent = round($imgsize / 575 * 100,2);
			$sizestyle = 'style="width: '.$percent.'%;"';
		}
		$hideimageless = ( isset($woo_options['woo_relatedposts_hideimageless']) && $woo_options['woo_relatedposts_hideimageless'] == "true" ? true : false); 
		$lim = ($hideimageless ? $lim = $atts['limit'] * 3 : $atts['limit']);
		
				$query_args = array(
 						'limit' => $lim, 
 						'post_type' => $post_type, 
 						'taxonomies' => $taxonomies_string, 
 						'specific_terms' => $specific_terms, 
 						'order' => 'DESC', 
 						'orderby' => 'date', 
 						'exclude' => array( $post->ID )
 					);
		
		$posts = woo_get_posts_by_taxonomy( $query_args );
		
		if ( count( (array)$posts ) ) {
			
			$output .= '<div class="' . $css_class . '">' . "\n";
		
			$output .= '<ul>' . "\n";
			$cnt = 1;
			
			foreach ( $posts as $post ) {
				if($cnt <= $atts['limit']){
					setup_postdata( $post );
					
					if ( $image <= 0 ) {
						$image_html = '';
					} else {
						$wooimg = woo_image( 'link=img&width=' . $imgsize . '&height=' . esc_attr( $imgsize ) . '&return=true&id=' . esc_attr( $post->ID ) );
						$image_html = '<a href="' . esc_url( get_permalink( $post->ID ) ) . '" class="thumbnail">' . $wooimg  . '</a>' . "\n";
					}
					// do not display if there aint no image
					if( !$hideimageless || (isset($wooimg) && !empty($wooimg)) ){
						// truncate the longish titles
						if ( isset($woo_options['woo_relatedposts_trunctitle']) && !empty($woo_options['woo_relatedposts_trunctitle'])) {
							$thetitle = ttruncat(get_the_title( $post->ID ),$woo_options['woo_relatedposts_trunctitle']);
						} else {
							$thetitle = get_the_title( $post->ID );
						}
						$output .= '<li '.$sizestyle.' class="post-id-' . esc_attr( $post->ID ) . '">' . "\n" . $image_html . "\n" . '<a href="' . esc_url( get_permalink( $post->ID ) ) . '" title="' . the_title_attribute( array( 'echo' => 0 ) ) . '" class="related-title"><span>' . $thetitle  . '</span></a>' . "\n" . '</li>' . "\n";
					} // end if
					$cnt++;
				} // end if
			}
			
			$output .= '</ul>' . "\n";
			$output .= '<div class="fix"></div><!--/.fix-->' . "\n";
			$output .= '</div><!--/.new-related-posts-->';
		}
		
		wp_reset_postdata();
		
		return apply_filters( 'new_shortcode_related_posts', $output, $atts );
} // End woo_shortcode_related_posts()

add_shortcode( 'my_new_related_posts', 'new_related_posts' );


// Theme Options 

// additional home title addon
function woo_options_add_postextras($options){
	$shortname = "woo";
			   
	// More stuff
	$options[] = array( "name" => __( 'Post Single Extras', 'woothemes' ),
						"icon" => "layout",
						"type" => "heading"); 
						
	$options[] = array( "name" => __( 'Related Posts', 'woothemes' ),
						"type" => "subheading");
						
	$options[] = array( "name" => __( 'Enable Related Posts on Single Blog Post', 'woothemes' ),
						"desc" => __( 'Check the box to enable related posts on a single blog post. If disabled they will not appear.', 'woothemes' ),
						"id" => $shortname."_relatedposts_enable",
						"std" => "false",
						"type" => "checkbox");
						
	$options[] = array( "name" => __( 'Related Posts Heading', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_relatedposts_heading",
						"std" => "Related Posts",
						"type" => "text");											
 

	$options[] = array( "name" => __( 'Enable Images on Related Posts', 'woothemes' ),
						"desc" => __( 'Check the box to enable related posts on a single blog post. If disabled they will not appear.', 'woothemes' ),
						"id" => $shortname."_relatedposts_images",
						"std" => "false",
						"class" => "collapsed",
						"type" => "checkbox");
						
	$options[] = array( "name" => __( 'Do not show Related Posts without an image', 'woothemes' ),
						"desc" => __( 'Check the box to hide related posts that do not have an image. ', 'woothemes' ),
						"id" => $shortname."_relatedposts_hideimageless",
						"std" => "false",
						"class" => "collapsed",
						"type" => "checkbox");

	$size = array();
	for ( $i = 50; $i <= 200; $i+=25 ) {
		$size[] = $i;
	}
	
	$num = array();
	for ( $ii = 1; $ii <= 10; $ii++ ) {
		$num[] = $ii;
	}
		
	$options[] = array( "name" => __( 'Size of Image To Display', 'woothemes' ),
						"desc" => __( 'Select the width of the image to display.', 'woothemes' ),
						"id" => $shortname."_relatedposts_imagesize",
						"std" => "50",
						"type" => "select",
						"class" => "hidden last",
						"options" => $size );		

	$options[] = array( "name" => __( 'Number of Related Posts To Display', 'woothemes' ),
						"desc" => __( 'Select the number of related posts to display.', 'woothemes' ),
						"id" => $shortname."_relatedposts_numposts",
						"std" => "5",
						"type" => "select",
						"options" => $num );
						
	$options[] = array( "name" => __( 'Related Post Title Length', 'woothemes' ),
						"desc" => __( 'Choose the number of characters to display in the related post title. You can enter 0 to hide the title. Leave blank to show the entire title.', 'woothemes' ),
						"id" => $shortname."_relatedposts_trunctitle",
						"std" => "",
						"type" => "text");											
				
						
	$options[] = array( "name" => __( 'Mailchimp Subscribe', 'woothemes' ),
						"type" => "subheading"); 

	$options[] = array( "name" => __( 'Enable Mailchimp Subscribe on Single Blog Post', 'woothemes' ),
						"desc" => __( 'Check the box to enable related posts on a single blog post. If disabled they will not appear.', 'woothemes' ),
						"id" => $shortname."_post_mc_enable",
						"std" => "false",
						"type" => "checkbox");
						
	$options[] = array( "name" => __( 'Mailchimp Subscribe on Single Post Heading', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_post_mc_heading",
						"std" => "Subscribe",
						"type" => "text");	
						
	$options[] = array( 'name' => "Text before the form",
						'desc' => "Optional: Enter text that will go before the form",
						'id' => $shortname . '_post_mc_text_before',
						'std' => '',
						'type' => 'text' );												
	
	$options[] = array( 'name' => "Text after the form",
						'desc' => "Optional: Enter text that will go after the form",
						'id' => $shortname . '_post_mc_text_after',
						'std' => '',
						'type' => 'text' );		
						
	return $options;
}

add_filter('yet_more_woo_options','woo_options_add_postextras',15);
?>