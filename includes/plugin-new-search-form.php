<?php 

/**
 * Display 2013 search form. Code taken from WP 3.6 core and renamed from get_search_form
 *
 */
function get_new_search_form( $echo = true ) {

	$format = apply_filters( 'search_form_format', 'xhtml' );

		if ( 'html5' == $format ) {
			$form = '<form role="search" method="get" class="newsearchform" action="' . esc_url( home_url( '/' ) ) . '">
				<label><span class="screen-reader-text">' . _x( 'Search for:', 'label' ) . '</span>
					<input type="search" placeholder="' . esc_attr_x( 'Search &hellip;', 'placeholder' ) . '" value="' . get_search_query() . '" name="s" title="' . _x( 'Search for:', 'label' ) . '" />
				</label>
				<input type="submit" class="searchsubmit" value="'. esc_attr_x( 'Search', 'submit button' ) .'" />
			</form>';
		} else {
			$form = '<form role="search" method="get" id="newsearchform" class="newsearchform" action="' . esc_url( home_url( '/' ) ) . '">
				<div>
					<label class="screen-reader-text" for="s">' . _x( 'Search for:', 'label' ) . '</label>
					<input type="text" value="' . get_search_query() . '" name="s" id="s" />
					<input type="submit" id="searchsubmit" value="'. esc_attr_x( 'Search', 'submit button' ) .'" />
				</div>
			</form>';
		}
//	$result = apply_filters( 'get_new_search_form', $form );
//	if ( null === $result )
		$result = $form;

//	if ( $echo )
		echo $result;
//	else
//		return $result;
}

add_action('woo_nav_inside','get_new_search_form'); // cool search feature 


/// css

/* Messiing with a minimizing search box as seen in twentythirteen theme */

/* kill the unnecessary box shadow in forms*/
#newsearchform input:focus,
#newsearchform textarea:focus {
  box-shadow: none !important;
  -moz-box-shadow: none !important;
  -webkit-box-shadow: none !important;
}

#newsearchform {
	position: absolute;
	right: 20px;
	top: 2px;
	z-index: 300;
}

#newsearchform {
	top: 2px;
}

#newsearchform [type="search"],
#newsearchform [type="text"] {
	background-color: transparent;
	background-image: url(images/search-icon.png);
	background-position: 5px center;
	background-repeat: no-repeat;
	background-size: 24px 24px;
	border: none;
	cursor: pointer;
	height: 37px;
	-webkit-transition: width 400ms ease, background 400ms ease;
	transition:         width 400ms ease, background 400ms ease;
	margin: 1px 0;
	padding: 0 0 0 34px;
	position: relative;
	width: 0;
}

#newsearchform [type="search"]:focus,
#newsearchform [type="text"]:focus {
	background-color: #fff;
	border: 1px solid #c3c0ab;
	cursor: text;
	outline: 0;
	width: 230px;
}

#newsearchform [type="submit"] {
    display: none;
}

#newsearchform .screen-reader-text {
	position: absolute !important;
	clip: rect(1px, 1px, 1px, 1px);
}

.screen-reader-text:focus {
	background-color: #f1f1f1;
	border-radius: 3px;
	box-shadow: 0 0 2px 2px rgba(0, 0, 0, 0.6);
	clip: auto !important;
	color: #454545;
	display: block;
	font-size: 14px;
	font-weight: bold;
	line-height: normal;
	padding: 15px 23px 14px;
	position: absolute;
	top: 5px;
	left: 5px;
	text-decoration: none;
	height: auto;
	width: auto;
	z-index: 10000; /* Above WP toolbar */
}
/* done messing with twentythirteen search */


?>
