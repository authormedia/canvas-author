<?php
// HOMEPAGE SLIDER
/*
To Do

1. make toggle in themeopts to use the background image with other stuff, or not
x2. made css load before custom css
*/

// Add a stylesheet for slider
function cmg_wooslider_css() {
	wp_enqueue_style( 'cmg_wooslider_css', get_stylesheet_directory_uri() . '/css/home-wooslider.css' );
}
add_action('wp_enqueue_scripts','cmg_wooslider_css',11);


// make the featured image into the background image and also load custom fields as the content
function slide_contents_custom($slide){
global $post, $woo_options;
	if ( !isset($woo_options['woo_homeslides_type']) || $woo_options['woo_homeslides_type'] != "overlay" ) {
		return $slide;
	}

// Use this to make the woo - custom meta image the background
$thumb_url = get_post_meta($post->ID, 'image', true); 
$line_1 = get_post_meta($post->ID, 'line_1', true);
$line_2 = get_post_meta($post->ID, 'line_2', true);
$url = get_post_meta($post->ID, 'url', true);
$button_text = get_post_meta($post->ID, 'button_text', true);

if ( ! is_bool( $thumb_url ) && isset( $thumb_url ) ) { // if using featured image use $thumb_url[0] instead of $thumb_url
			$bgimage = esc_url( $thumb_url ); // if using featured image use $thumb_url[0] instead of $thumb_url
		} 
	$contents = '<div class="slide-container" style="background-image: url('.$bgimage.'); background-repeat:no-repeat;" >';
	if( !empty($line_1) || !empty($line_2)){
		$contents .= '<div class="caption">
						<h2 class="line1">'.$line_1.'</h2>
						<div class="line2">'.$line_2.'</div>
						<div class="btn">
						<a href="'.$url.'">'.$button_text.'</a>
						</div>
					</div>';
	} else {
		$contents .= '<a href="'.$url.'" class="slidelink"></a>';
	}
	$contents .= '</div>';
	
// Redid the thing to be much less useful because the client didnt value useful
	return $contents;
}

add_filter('wooslider_slide_content_slides','slide_contents_custom',20);


// remove the main editor pane - since we are adding slide details through custom meta boxes set up in theme_options.php
function remove_editor() {
	global $woo_options;
	if ( !isset($woo_options['woo_homeslides_type']) || $woo_options['woo_homeslides_type'] != "overlay" ) {
		return;
	}
  remove_post_type_support('slide', 'editor');
  remove_post_type_support('slide', 'excerpt');
}
//add_action('admin_init', 'remove_editor');

// framework innovation - made args load in filter
// Call Slider for homepage slider - will call all slides in the "home" slide_page (group)
function make_slider_appear(){
	echo '<div class="fix"></div>';
	echo '<div id="cmg-homeslider">';
	$slargs = array(
		'slider_type' => 'slides',
		'display_content' => false, 
		'imageslide' => true, 
		'link_slide' => true,
		'order' => "ASC",
		'order_by' => "menu_order",
		'slide_page' => 'homemain',
		'limit' => 10,
		'thumbnails' => false
	);
	$slargs = apply_filters('change_me_slargs',$slargs);
	// some of the best args only work in shortcodes - booo - so we need to transform them to shortcode args
	$shortslargs = '';
	// rewrite each in shortcode arg format - ie - slidertype = "slides"
	foreach ($slargs as $slarg => $value ) {
		if (!isset($value) || empty($value) ) { // if false
			$shortslargs .= ''.$slarg.'="false" ';
		} elseif ($value == 1 ){ // if true
			$shortslargs .= ''.$slarg.'="true" ';
	//	} elseif ( is_numeric($value) ) { // if number
	//		$shortslarg .= ' '.$slarg.' = '.$value.' ';
		} else { // string
			$shortslargs .= ''.$slarg.'="'.$value.'" ';
		}
	}
	//print_r('<pre style="padding: 10px; border: 1px solid #000; margin: 10px">'); print_r( $shortslargs ); print_r('</pre>');
	echo do_shortcode('[wooslider '.$shortslargs.' ]');
		//wooslider( $slargs ); 
	echo  '</div>';			
}

// Add slider to homepage
function place_slider_onhome(){
	global $woo_options;
	if( !isset($woo_options['woo_homeslides_enable']) || $woo_options['woo_homeslides_enable'] != 'true' ) {
		return false;
	}
	if( !is_front_page() ) {
		return false;
	}

	make_slider_appear();
}


function change_homeslider_position() {
	global $woo_options;
	$woo_homeslides_priority = isset($woo_options['woo_homeslides_priority']) ? $woo_options['woo_homeslides_priority'] : 10;
	remove_action('woo_main_before','place_slider_onhome');
	add_action('woo_homepage_blocks','place_slider_onhome', $woo_homeslides_priority);
}
add_action('woo_head', 'change_homeslider_position', 40);
?>