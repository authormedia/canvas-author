<?php 
// LIKE AND TWEET BUTTONS
//  Add the Tweet and Like to the top of posts
// framework innovation - added filter to change the default order of socmed icons
function add_like_and_tweets($style='vert',$config=1) {
	
	global $woo_options;
	$output = "";
	
	if( !isset($woo_options['woo_use_liketweets']) || $woo_options['woo_use_liketweets']=='false'){
		return;
	}
	
	if( isset($woo_options['woo_liketweet_placement']) && $woo_options['woo_liketweet_placement']=='after'){
		$float = 'left';
	}else{
		$float = 'right';
	}
	
	// different classes per configuration 
	if($config==1){
		$buttons = 'config1';
	}else{
		$buttons = 'config2';
	}

	$output .= '<div class="liketweets '.$float.' '.$buttons.'">';
	
	$blog_title = get_bloginfo('name');
	$link = urlencode(get_permalink(get_the_ID()));
	$title = get_the_title(get_the_ID());
	$site = $blog_title;
	
/**************
/// Sharethis Count Buttons   //
**************/

	if ( isset($style) && $style='vert' )	{ // if use sharethis vertical counts
		$alt = '_vcount';
	} elseif (  isset($style) && $style='vert' )	{ // if use sharethis large buttons
		$alt = '_hcount';
	} else {
		$alt = '';
	}
	$output .= "";


	if ( isset($woo_options['woo_liketweet_sharethis_googleplus']) && $woo_options['woo_liketweet_sharethis_googleplus'] == 'true')	{
		$gplus = "<div class='st_googleplus".$alt." cont-g' displayText='Google +'></div>";
	}

// framework innovation - fixed bug in comments output	
/*********************
/// Comment Count  - not currently part of sharethis //
*********************/
	if($style != 'horz' && isset($woo_options['woo_liketweet_sharethis_comments']) && $woo_options['woo_liketweet_sharethis_comments'] == 'true'){
		if( 'post' == get_post_type() || 'product' == get_post_type() ) {
			$comment = '
			<div class="cont">
			<a href="'.get_permalink(get_the_ID()).'#comments">
			<div class="commentcount">'.get_comments_number(get_the_ID()).'</div>
			</a>
			</div>';
		} 
	}
//fixed output line errors
	if ( isset($woo_options['woo_liketweet_sharethis_sharethis']) && $woo_options['woo_liketweet_sharethis_sharethis'] == 'true')	{
		$sharethis = "<div class='st_sharethis".$alt." cont-st' st_url='".get_permalink()."' st_title='".get_the_title()."' displayText='ShareThis'></div>";
	}
	if ( isset($woo_options['woo_liketweet_sharethis_linkedin']) && $woo_options['woo_liketweet_sharethis_linkedin'] == 'true')	{
		$linkedin = "<div class='st_linkedin".$alt." cont' st_url='".get_permalink()."' displayText='LinkedIn'></div>";
	}	
	
	if ( isset($woo_options['woo_liketweet_sharethis_pinterest']) && $woo_options['woo_liketweet_sharethis_pinterest'] == 'true')	{
		$pinterest = "<div class='st_pinterest".$alt." cont' st_url='".get_permalink()."' displayText='Pinterest'></div>";
	}
	
	if( isset($woo_options['woo_connect_twitter_handle']) && !empty($woo_options['woo_connect_twitter_handle']) ){
		$handle = str_replace('@','',$woo_options['woo_connect_twitter_handle']);
		$twithandle = "st_via='".$handle."'";
	}
	
	if ( isset($woo_options['woo_liketweet_sharethis_twitter']) && $woo_options['woo_liketweet_sharethis_twitter'] == 'true')	{
		$twitter = "<div class='st_twitter".$alt." cont' st_url='".get_permalink()."' st_title='".get_the_title()."' ".$twithandle." displayText='Twitter'></div>";
	}

	if ( isset($woo_options['woo_liketweet_sharethis_facebook']) && $woo_options['woo_liketweet_sharethis_facebook'] == 'true')	{
		$facebook = "<div class='st_facebook".$alt." cont' st_url='".get_permalink()."' st_title='".get_the_title()."' displayText='Facebook'></div>";
	}

	if ( isset($woo_options['woo_liketweet_sharethis_fblike']) && $woo_options['woo_liketweet_sharethis_fblike'] == 'true')	{
		$fblike = "<div class='st_fblike".$alt." cont' st_url='".get_permalink()."' st_title='".get_the_title()."' displayText='Facebook Like'></div>";
	}
	
	// allow changing the order through a filter
	$default_order = '[x[facebook]x] [x[fblike]x] [x[twitter]x] [x[pinterest]x] [x[linkedin]x] [x[sharethis]x] [x[comment]x] [x[gplus]x]';
	$order = apply_filters('change_liketweet_node_order',$default_order);
	
	$liketweets = str_replace(array('[x[facebook]x]','[x[fblike]x]','[x[twitter]x]','[x[pinterest]x]','[x[linkedin]x]','[x[sharethis]x]','[x[comment]x]','[x[gplus]x]'),array($facebook,$fblike,$twitter,$pinterest,$linkedin,$sharethis,$comment,$gplus),$order);
	
	$output .= $liketweets;
		
	$output .= "</div>";
	echo $output;
}



?>