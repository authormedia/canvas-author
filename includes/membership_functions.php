<?php
// Mess with Woothemes menu labels  
// THEME VARIABLES
$themename = "Authormedia";
$themeslug = "authormedia";
$myicon = get_stylesheet_directory_uri() . '/images/authormedia-icon.png';

if ( get_option('woo_themename') != $themename) update_option('woo_themename',$themename);   
 //if ( get_option('woo_shortname') != $shortname) update_option('woo_shortname',$shortname);
if ( get_option('framework_woo_backend_icon') != $myicon) update_option('framework_woo_backend_icon',$myicon);

 
// deregister dashboard widgets
// NOT sure if this needs to run on each admin init or just when a change is made
function wpc_dashboard_widgets() {
	global $wp_meta_boxes;
	// Today widget
	//unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	// Last comments
	//unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	// Incoming links
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	// Plugins
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	// Newsletter Signup widget stuff
	//unset($wp_meta_boxes['dashboard']['normal']['core']['dvk_db_widget']);
	// quickpress
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}
 add_action('wp_dashboard_setup', 'wpc_dashboard_widgets');
/*   END ADMIN INIT FUNCTIONS        */	


// MAKE THE BODY CLASS ONE COLUMN 
// this needs to be called directly in functions where the page needs to be changed to one column - on-load
function site_is_one_col($classes){
	$newclasses = array();
	foreach ($classes as $class){
	$newclasses[] = str_replace(array('two-col-left','two-col-right'),'one-col',$class);
	//$newclasses[] = str_replace('','one-col',$class);
//	print_r('<pre>'); print_r($class); print_r('</pre>'); 
	}
	return $newclasses;
}

/*  END THEME LAYOUT INIT FUNCTIONS  */	

/************************************/	
/*                                  */
/*   ADMIN BAR, ADMIN BRANDING      */	
/*                                  */
/************************************/	
// Finding ways to reorganize the admin menu according to user level. This allows us to move away from extra plugins

// Messin with the admin bar
// remove links/menus from the admin bar
function mytheme_admin_bar_render() {
	global $wp_admin_bar;
	// remove link on member site admin bar to the master site
	$wp_admin_bar->remove_node('wp-logo');
	$wp_admin_bar->remove_node('woothemes');
//print_r('<pre>');print_r($wp_admin_bar);print_r('</pre>'); 
}
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );


function toolbar_link_to_mypage( $wp_admin_bar ) {
  global $wp_admin_bar;

  $args = array(
    'id' => 'site_level',
    'title' => 'Authormedia Website',
    'href' => 'http://www.authormedia.com',
    'meta' => array('class' => 'level-label')
  );
  $wp_admin_bar->add_node($args);
}
add_action( 'admin_bar_menu', 'toolbar_link_to_mypage', 1 );


// some really useful ideas from this article:
// http://wp.smashingmagazine.com/2012/05/17/customize-wordpress-admin-easily/

// change the logo link
// Use your own external URL logo link in login page
function wpc_url_login(){
	return "http://www.authormedia.com/"; // your URL here
}
add_filter('login_headerurl', 'wpc_url_login');

// Custom WordPress Footer
function remove_footer_admin () {
	echo '&copy; '.date('Y').' - Authormedia';
}
add_filter('admin_footer_text', 'remove_footer_admin');
 

// Add our own RSS Feed
// Adding a news feed widget to the dashboard
 function wp_admin_dashboard_add_news_feed_widget() {
	 global $wp_meta_boxes;
	 // Our new dashboard widget
	 wp_add_dashboard_widget( 'dashboard_authormedia_feed', 'The Latest from AuthorMedia', 'dashboard_authormedia_feed_output' );
	 
	 // move it to the left side
	$my_widget = $wp_meta_boxes['dashboard']['normal']['core']['dashboard_authormedia_feed'];

	// We then unset that part of the array
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_authormedia_feed']);

	// Now we just add your widget back in
	$wp_meta_boxes['dashboard']['side']['core']['dashboard_authormedia_feed'] = $my_widget;
 }

 add_action('wp_dashboard_setup', 'wp_admin_dashboard_add_news_feed_widget');

// The Feed
 function dashboard_authormedia_feed_output() {
	 echo '<div>';
	 wp_widget_rss_output(array(
	 'url' => 'http://www.authormedia.com/feed/',
	 'title' => 'Latest Help from Authormedia',
	 'items' => 5,
	 'show_summary' => 1,
	 'show_author' => 1,
	 'show_date' => 1
	 ));
	 echo "</div>";
 }

/*   END ADMIN BAR, ADMIN BRANDING */
?>