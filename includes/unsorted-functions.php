<?php 
/********************************************/
/*    Thickbox pop-up					    */
/*     for use with all sorts for stuff     */
/********************************************/

function load_thickbox_div($content,$divID='my-content-id'){
	add_thickbox();
	echo '<div id="'.$divID.'" style="display:none;">
		 <p>';
	echo $content;	  
	echo '</p>
	</div>';
}

function pop_up_widget_video(){
	global $woo_options;
	$video  = (isset($woo_options['woo_popup_video']) && !empty($woo_options['woo_popup_video']) ? $woo_options['woo_popup_video'] : '' );
	$divID = "video-popup";
	return load_thickbox_div($video,$divID);
}

//add_action('woo_main_before','pop_up_widget_video');
?>