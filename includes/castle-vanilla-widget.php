<?php
/*
Vanilla Widget
This is a template for displaying just about any function. All you need to do is:

1. add the name of your function below to the list of functions
2. Add the widget as normal into the sidebar
3. The widget then loads your function

*/

if ( ! defined( 'ABSPATH' ) ) exit;

$blocks = vanilla_widget_functions();
/*$blocks = array(
	'my_pretty_function',
	'add_home_podcast_box',
	'mailchimp_home_subscribe_redo'
);*/
/*---------------------------------------------------------------------------------*/
/* widget */
/*---------------------------------------------------------------------------------*/
class Vanilla_Widget extends WP_Widget {
	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {
		parent::__construct(
	 		'Vanilla_Widget', // Base ID
			'Vanilla Widget', // Name
			array( 'description' => __( 'Write a function and output it with this widget', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		extract( $args );
		
		
		//$before_widget= str_replace('widget_outer','widget_bubble',$before_widget);
		
		echo $before_widget;
		
		call_user_func($instance['function']);
		$title = apply_filters( 'widget_title', $instance['function'] );
		echo $after_widget;
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['function'] = strip_tags( $new_instance['function'] );
		$instance['title'] = strip_tags( $instance['function'] ); // added simply to show the function name in admin title
		return $instance;
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		global $blocks;
		?>
				  	
			<input type="hidden" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['function']; ?>"   id="<?php echo $this->get_field_id('title'); ?>" />

        <p>
            <label for="<?php echo $this->get_field_id( 'function' ); ?>"><?php  _e( 'List of Functions:' ); ?></label>
            <select name="<?php echo $this->get_field_name( 'function' ); ?>" class="widefat" id="<?php echo $this->get_field_id( 'function' ); ?>">
                <option value=""><?php _e( 'Select One' ); ?></option>
				<?php foreach ($blocks as $block ) { ?>
                	<option value="<?php echo $block ?>" <?php selected( $instance['function'], $block ); ?>><?php echo $block; ?></option>
				<?php } ?>	
            </select>
        </p>

		<?php 
	}

} // class Vanilla_Widget

// register Vanilla_Widget widget
add_action( 'widgets_init', create_function( '', 'register_widget( "Vanilla_Widget" );' ) );


?>