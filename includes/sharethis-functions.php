<?php 
//
/*************************/	
/*                       */
/*      SHARE THIS       */	
/*                       */
/*************************/	
//

// 
// framework innovation - added output order filter

// output the sharethis buttons generated from http://sharethis.com/publishers/get-sharing-tools#
function output_sharethis_buttons(){
global $woo_options;
	//$output = $content;
	if ( isset($woo_options['woo_sharethis_style']) && $woo_options['woo_sharethis_style'] == 'large')	{ // if use sharethis large buttons
		$alt = '_large';
	} elseif ( isset($woo_options['woo_sharethis_style']) && $woo_options['woo_sharethis_style'] == 'vcounts')	{ // if use sharethis large buttons
		$alt = '_vcount';
	} elseif ( isset($woo_options['woo_sharethis_style']) && $woo_options['woo_sharethis_style'] == 'hcounts')	{ // if use sharethis large buttons
		$alt = '_hcount';
	} else {
		$alt = '';
	}
	if( isset($woo_options['woo_connect_twitter_handle']) && !empty($woo_options['woo_connect_twitter_handle']) ){
		$handle = str_replace('@','',$woo_options['woo_connect_twitter_handle']);
		$twithandle = "st_via='".$handle."'";
	}

	//fixed output line errors
	if ( isset($woo_options['woo_sharethis_facebook']) && $woo_options['woo_sharethis_facebook'] == 'true')	{
		$facebook = "<span class='st_facebook".$alt."' st_url='".get_permalink()."' st_title='".get_the_title()."' displayText='Facebook'></span>";
	}
	if ( isset($woo_options['woo_sharethis_twitter']) && $woo_options['woo_sharethis_twitter'] == 'true')	{
		$twitter = "<span class='st_twitter".$alt."' st_url='".get_permalink()."' ".$twithandle." displayText='Twitter'></span>";
	}
	if ( isset($woo_options['woo_sharethis_googleplus']) && $woo_options['woo_sharethis_googleplus'] == 'true')	{
		$gplus = "<span class='st_googleplus".$alt."' st_url='".get_permalink()."' displayText='Google +'></span>";
	}
	if ( isset($woo_options['woo_sharethis_pinterest']) && $woo_options['woo_sharethis_pinterest'] == 'true')	{
		$pinterest = "<span class='st_pinterest".$alt."' st_url='".get_permalink()."' displayText='Pinterest'></span>";
	}
	if ( isset($woo_options['woo_sharethis_linkedin']) && $woo_options['woo_sharethis_linkedin'] == 'true')	{
		$linkedin = "<span class='st_linkedin".$alt."' st_url='".get_permalink()."' displayText='LinkedIn'></span>";
	}
	if ( isset($woo_options['woo_sharethis_email']) && $woo_options['woo_sharethis_email'] == 'true')	{
		$email = "<span class='st_email".$alt."' st_url='".get_permalink()."' displayText='Email'></span>";
	}
	if ( isset($woo_options['woo_sharethis_sharethis']) && $woo_options['woo_sharethis_sharethis'] == 'true')	{
		$sharethis = "<span class='st_sharethis".$alt."' st_url='".get_permalink()."' st_title='".get_the_title()."' displayText='ShareThis'></span>";
	}
	if ( isset($woo_options['woo_sharethis_fblike']) && $woo_options['woo_sharethis_fblike'] == 'true')	{
	$facebook_like = "<span class='st_fblike".$alt."' st_url='".get_permalink()."' st_title='".get_the_title()."' displayText='Like'></span>";
	}

	$output = "<div id='sharethis'>";
	
	if( isset($woo_options['woo_sharethis_label']) && !empty($woo_options['woo_sharethis_label']) ){
		$output .= "<div class='header'>".$woo_options['woo_sharethis_label']." </div>";
	}

	// allow changing the order through a filter
	$default_order = '[x[facebook]x] [x[facebook_like]x] [x[twitter]x] [x[pinterest]x] [x[linkedin]x] [x[sharethis]x] [x[gplus]x]';
	$order = apply_filters('change_sharethis_node_order',$default_order);
	
	$sharethis = str_replace(array('[x[facebook]x]','[x[facebook_like]x]','[x[twitter]x]','[x[pinterest]x]','[x[linkedin]x]','[x[sharethis]x]','[x[gplus]x]'),array($facebook,$facebook_like,$twitter,$pinterest,$linkedin,$sharethis,$gplus),$order);
	
	$output .= $sharethis;

	$output .= "</div>";
	return $output;
}


// simple template tag version to echo the sharethis buttons anywhere
function echo_sharethis_buttons(){
	global $woo_options;
	$output =  '';
	$show = 0;
	if( !isset($woo_options['woo_use_sharethis']) || $woo_options['woo_use_sharethis']=='false'){
		return;
	}
	if ( is_single() || is_mulletized() ) {
		$show = 1;
	}
	if( (is_archive() || is_page_template('template-blog.php') ) && ( isset($woo_options['woo_sharethis_onexcerpts']) && $woo_options['woo_sharethis_onexcerpts']== 'true') ){
		$show = 1;
	} 
	
	if ($show == 1){
		$output =  output_sharethis_buttons();
		echo $output;
	}
	
}

// improved if -- return clause 
function place_sharethis_buttons(){
	global $woo_options;
	//print_r($woo_options['woo_use_sharethis']); exit;
	if( !isset($woo_options['woo_use_sharethis']) || $woo_options['woo_use_sharethis']=='false'){
		return;
	}
	if ( is_singular() || is_single() || is_mulletized() ) {
		add_filter('the_content','output_sharethis_buttons', 10);
	}
	if( isset($woo_options['woo_sharethis_onexcerpts']) && $woo_options['woo_sharethis_onexcerpts']== 'true'){
		add_filter('the_excerpt','output_sharethis_buttons', 10);
	} 
}

//add_action('woo_head','place_sharethis_buttons',20);
?>