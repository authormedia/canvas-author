<?php
function save_lost_settings_on_update($data) {
	global $woo_options, $userlevel;
	if ($userlevel < 3){	// this only needs to happen if non-super is updating options, because that is when settings are hidden and then lost on update
		foreach($data as $key=>$value) {
			$woo_options[$key] = $value;
	    }
		return $woo_options;
	}
	return $data; // if is superadmin, then return what was passed, unchanged
}
// pre_update_optiion_(option name) is filter that is applied right before an option save runs
//add_filter( 'pre_update_option_woo_options', 'save_lost_settings_on_update', 40 );

// make a filter from the function in woo_options for allowing more options to be appended
function woo_options_add($options){
	$options = apply_filters('yet_more_woo_options', $options);
	return $options;
}
