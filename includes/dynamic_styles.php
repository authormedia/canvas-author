<?php  
/*-----------------------------------------------------------------------------------*/
/* // Add custom styling  ADDED by Jim Camomile -- Castledevs           		     */
/* // this adds styles that we want to use from theme-options even though            */
/* // the normal theme option styles are blocked from loading 		                 */
/* // this is particularly useful for loading the custom style box in styles         */
/*-----------------------------------------------------------------------------------*/

				
/*-----------------------------------------------------------------------------------*/
/* Additional Google Webfonts Stylesheet Generator */
/*-----------------------------------------------------------------------------------*/
/*
This is based on the woo_google_webfonts function in canvas admin functions lines 1788 ff. It is used for adding google font when selected in style pre-sets
*/

// load google fonts
if ( ! function_exists( 'enqueue_google_fonts' ) ) {
	function enqueue_google_fonts(){
		global $woo_options;
		$output = "";
		$myfonts = get_my_gfonts(); // this function should be in site-specific.php 
		$myfonts = str_replace( " ","+",$myfonts);
		if (empty($myfonts) || $myfonts == ""){return;}
		$output .= "\n<!-- Google Webfonts -->\n";
		$output .= '<link id="gfonts" href="http'. ( is_ssl() ? 's' : '' ) .'://fonts.googleapis.com/css?family=' . $myfonts .'" rel="stylesheet" type="text/css" />'."\n";
		$output = str_replace( '|"','"',$output);
		echo $output;
	}
}
add_action( 'wp_head', 'enqueue_google_fonts' );

// Strange Magic that solves the font loading issue in Chrome, especially when G fonts and sharethis js are both loading. This little script in the header works but I do not know why
function gfont_chrome_mojo(){ 
	echo "
	<script type=\"text/javascript\">
	//JavaScript goes here
	WebFontConfig = {
	  google: { families: ['Open Sans', 'Oswald'] },
		fontinactive: function (fontFamily, fontDescription) {
	   //Something went wrong! Let's load our local fonts.
		WebFontConfig = {
		  custom: { families: ['Open Sans', 'Oswald'],
		  urls: ['font-one.css', 'font-two.css']
		}
	  };
	  loadFonts();
	  }
	};
	
	function loadFonts() {
	  var wf = document.createElement('script');
	  wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
		'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
	  wf.type = 'text/javascript';
	  wf.async = 'true';
	  var s = document.getElementsByTagName('script')[0];
	  s.parentNode.insertBefore(wf, s);
	}
	
	(function () {
	  //Once document is ready, load the fonts.
	  loadFonts();
	  })();
	
	</script>
	";
}
//add_action( 'wp_head', 'gfont_chrome_mojo',600 ); 

// Using Adobe Fonts
// load google fonts
if ( ! function_exists( 'enqueue_adobe_fonts' ) ) {
	function enqueue_adobe_fonts(){
		$output = "";
		$usetypekit = use_adobefonts(); // this function should be set to true or false in site-specific.php
		if ($usetypekit == false){return;}
		$output .= "\n<!-- Adobe Typekit Webfonts -->\n";
		$output .= '<script type="text/javascript" src="//use.typekit.net/wbo2glm.js"></script>
					<script type="text/javascript">try{Typekit.load();}catch(e){}</script>'."\n";
		echo $output;
	}
}
add_action( 'wp_head', 'enqueue_adobe_fonts' );

function no_home_sidebar(){
	global $woo_options;
	//print_r($woo_options['woo_homesidebar_enable']); exit;
	if (!is_front_page() || ( !isset($woo_options['woo_homesidebar_enable'] ) || $woo_options['woo_homesidebar_enable'] != 'false' ) ){ // for homepage only
		return false; 
	}
	$css = '.home.two-col-left #main-sidebar-container #main {width: 100%; }';
	$output = "\n<!-- No Sidebar in Home Styling -->\n<style type=\"text/css\">\n" . $css . "</style>\n<!-- /No Sidebar in Home Styling -->\n\n";
	echo $output;
}
add_action( 'wp_head','no_home_sidebar', 400 );		