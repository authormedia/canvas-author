<?php
//////////////////////////////////////////////////////////////////
///                    COMMON  Functions                        //
///   The following are needed for most or all sites            //
///   They allow you to config common features in most site     //
//////////////////////////////////////////////////////////////////

// THE FOLLOWING MUST BE ON THIS PAGE FOR GOOGLE FONTS TO WORK
// Add the  Google fonts that you want to load. Make sure to add the extension and font weight numbers, just as they are in http://www.google.com/fonts/
// ie - if you get this from google-fonts to paste into the header - <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
// then add this as the entry: " Oswald:400,300,700&subset=latin,latin-ext "
// end every line with a pipe |
// see examples  



// We Must get the calling of fonts into a function ON SITE-SPECIFIC or all the fonts will be lost on each framework update
function get_my_gfonts(){
	$myfonts = ""; 
	// add the google font calls using the examples as a guide
	$myfonts = ""; 
	// add the google font calls using the examples as a guide
	//$myfonts .= "Lato:400,100,300,700|"; 
	//$myfonts .= "Open+Sans:r,i,b,bi|";
	return $myfonts;
}

// Adobe Typekit fonts. set the the following true or false to indicate use of Adobe Typekit Fonts
function use_adobefonts(){
	return true;
	//return false;
}

// Add Wysiwig editors in theme opts as needed
// Added by Jim Camomile
// add a new instance of nicedit for each textarea that you want to transform.
// framework innovation - moved nicEdit loader from themeopts to site specific
function add_wysiwyg_to_themeopts(){
	global $current_screen;
	if($current_screen->base == 'toplevel_page_woothemes'){
	?>
		<script type="text/javascript" src="http://js.nicedit.com/nicEdit-latest.js"></script>
		<script type="text/javascript">
		//<![CDATA[
		jQuery('document').ready(function() {
			new nicEditor({buttonList : ['fontFormat','bold','italic','left','center','right','ol','ul','indent','outdent','hr','link','unlink','xhtml']}).panelInstance('woo_reservationswidget_blurb');
			new nicEditor({buttonList : ['fontFormat','bold','italic','left','center','right','ol','ul','indent','outdent','hr','link','unlink','xhtml']}).panelInstance('woo_locationwidget_blurb');
			new nicEditor({buttonList : ['fontFormat','bold','italic','left','center','right','ol','ul','indent','outdent','hr','link','unlink','xhtml']}).panelInstance('woo_ourstory_content');
			jQuery(".submit-button").mousedown(function(){jQuery.each(nicEditors.editors, function(i,e){e.saveContent();});});
		});
		
		//]]>
		</script>
	<?php 
	}
}
add_action('admin_head','add_wysiwyg_to_themeopts');

// Rearrange the order of post-meta stuff
// filter the order of the post meta pieces
// default is [x[author]x] [x[cat]x] [x[date]x] [x[tags]x]
function meta_node_order($nodes){
	$rssthing = get_rss_thing();
	return '<div id="postmetabox"> '.$rssthing.' [x[cat]x] [x[date]x] [x[author]x] <br /> [x[tags]x]</div>';
}
add_filter('change_postmeta_node_order','meta_node_order');


// Move the liketweets to just below the post title
function move_liketweets_custom() {
	remove_action('woo_post_inside_before', 'add_like_and_tweets', 5);
	remove_action('woo_post_inside_after', 'add_like_and_tweets', 20);
	add_action('woo_post_inside_before', 'add_like_and_tweets', 15);
}
add_action('woo_head','move_liketweets_custom',60);

// Adjust the Social Media icons order
function change_socmed_order($order){
	$new_order = '[x[twitter]x] [x[facebook]x] [x[youtube]x] [x[pinterest]x] [x[linkedin]x] [x[googleplus]x] [x[goodreads]x] [x[rss]x]';
	return $new_order;
}
add_filter('change_socmed_node_order','change_socmed_order');


// Adjust the Liketweet order
function change_liketweet_order($order){
	$new_order = '[x[comment]x] [x[twitter]x] [x[fblike]x] [x[facebook]x] [x[pinterest]x] [x[linkedin]x] [x[sharethis]x]  [x[gplus]x]';
	return $new_order;
}
add_filter('change_liketweet_node_order','change_liketweet_order');

// Adjust the Sharethis order
function change_sharethis_order($order){
	$new_order = '[x[facebook]x] [x[facebook_like]x] [x[twitter]x] [x[pinterest]x] [x[linkedin]x] [x[sharethis]x] [x[gplus]x]';
	return $new_order;
}
add_filter('change_sharethis_node_order','change_sharethis_order');

// Move the sharethis buttons to the top of content
function move_sharethis_custom() {
	remove_action('woo_head','place_sharethis_buttons',20);
	add_action('woo_post_inside_before', 'echo_sharethis_buttons', 25);
}
add_action('woo_head','move_sharethis_custom',150);

// Vanilla Widget functions
/// If you have any functions that you want to add with the vanilla widget, then add them below
function vanilla_widget_functions(){
	// add the function names below and then select the function you want in a vanilla widget
	$blocks = array(
		'villa_our_story_widget',
		'villa_features_slider',
		'villa_home_blog_intro',
		'villa_reservations_widget',
		'villa_location_widget',
	);
	return $blocks;
}


// Mess with the style of excerpt
// use stripped version of the content and set a character limit
function excerpt_style($excerpt){
	// limit to posts only
	if ('post' != get_post_type()){
		return $excerpt;
	}
	// set the limit here in words
	$limit = 90;
	$start_content =  get_the_content();
	// strip out images
	//$cleancontent = preg_replace("/<img[^>]+\>/i", "", $start_content);
	$cleancontent = strip_tags($start_content, '<p><a><strong><i><div><ul><li><i><em>');
	//$cleancontent = str_replace(array('<br>','<br />'),array('',''),$cleancontent);
	$content = explode(' ', $cleancontent, $limit);
	if (count($content)>=$limit) {
		array_pop($content);
		$content = implode(" ",$content).'...';
	} else {
		$content = implode(" ",$content);
	}	
	$content = preg_replace('#<p[^>]*>(\s|&nbsp;?)*</p>#', '', $content);
	$content = preg_replace('/\[.+\]/','', $content);
	$content = apply_filters('the_content', $content);
	$content = str_replace(']]>', ']]&gt;', $content);
	$content = preg_replace('#<p[^>]*>(\s|&nbsp;?)*</p>#', '', $content);
	return $content;
}
//add_filter('the_excerpt','excerpt_style', 40);

// Change the Read More Text
function change_readmore_label($label){
	return 'Read Post';
}
add_filter('read_more_of_this','change_readmore_label');

// Change Excerpt length
function change_excerpt_length($excerpt){
	return 90; // set length here
}
add_filter('excerpt_length','change_excerpt_length');

// Change the ending of the excerpt
function change_excerpt_end($excerpt){
	return ' &hellip;'; // add whatever you want on the end of the excerpt. Default is '[&hellip;]'
}
add_filter('excerpt_more','change_excerpt_end');


// changing how the archive titles work
// Option One - a simple filter
//add_filter('woo_archive_title','new_archive_title', 10);
if ( ! function_exists( 'new_archive_title' ) ) { 
	function new_archive_title($content){
		// getting rid of Archive | in the title
		$content = str_replace('Archive | ','',$content);
		// replacing words with icon for rss
		$child_images_dir =  get_stylesheet_directory_uri() . '/images/';
		$content = str_replace('RSS feed for this section','<img src="'.$child_images_dir.'ico-rss.png" />',$content);
		return $content;
	}
}

// Option 2 - reroll the entire function (probably do NOT need this)
// Simplify the UGLY Woo Archive Titles - rerolls the function of the same name in theme-functions
function woo_archive_title ( $before = '', $after = '', $echo = true ) {
	global $wp_query;
	if ( is_category() || is_tag() || is_tax() ) {

		$taxonomy_obj = $wp_query->get_queried_object();
		$term_id = $taxonomy_obj->term_id;
		$taxonomy_short_name = $taxonomy_obj->taxonomy;

		$taxonomy_raw_obj = get_taxonomy( $taxonomy_short_name );

	} // End IF Statement

	$title = '';
	$delimiter = ' | ';
	$date_format = get_option( 'date_format' );

	// Category Archive
	if ( is_category() ) {

		$title = '<span class="fl cat">'.single_cat_title( '', false ) . '</span> 
		<span class="fr catrss">';
		$cat_obj = $wp_query->get_queried_object();
		$cat_id = $cat_obj->cat_ID;
		$title .= '<a href="' . get_term_feed_link( $term_id, $taxonomy_short_name, '' ) . '" class="icon-rss icon-large" ></a></span>';
		$has_title = true;
	}

	// Day Archive
	if ( is_day() ) {
		$title = __( 'Archive', 'woothemes' ) . $delimiter . get_the_time( $date_format );
	}

	// Month Archive
	if ( is_month() ) {
		$date_format = apply_filters( 'woo_archive_title_date_format', 'F, Y' );
		$title = __( 'Archive', 'woothemes' ) . $delimiter . get_the_time( $date_format );
	}

	// Year Archive
	if ( is_year() ) {
		$date_format = apply_filters( 'woo_archive_title_date_format', 'Y' );
		$title = __( 'Archive', 'woothemes' ) . $delimiter . get_the_time( $date_format );
	}

	// Author Archive
	if ( is_author() ) {
		$title = __( 'Author: ', 'woothemes' ) . $delimiter . get_the_author_meta( 'display_name', get_query_var( 'author' ) );
	}

	// Tag Archive
	if ( is_tag() ) {
		$title = __( 'Tag: ', 'woothemes' ) . $delimiter . single_tag_title( '', false );
	}

	// Post Type Archive
	if ( function_exists( 'is_post_type_archive' ) && is_post_type_archive() ) {
		/* Get the post type object. */
		$post_type_object = get_post_type_object( get_query_var( 'post_type' ) );
		$title = $post_type_object->labels->name;
	}

	// Post Format Archive
	if ( get_query_var( 'taxonomy' ) == 'post_format' ) {
		$post_format = str_replace( 'post-format-', '', get_query_var( 'post_format' ) );
		$title = get_post_format_string( $post_format ) . ' ' . __( '', 'woothemes' );
	}

	// General Taxonomy Archive
	if ( is_tax() ) {
		$title = sprintf( __( '', 'woothemes' ), $taxonomy_raw_obj->labels->name, $taxonomy_obj->name );

	}

	if ( strlen($title) == 0 )
	return;

	$title = $before . $title . $after;

	// Allow for external filters to manipulate the title value.
	$title = apply_filters( 'woo_archive_title', $title, $before, $after );

	if ( $echo )
		echo $title;
	else
		return $title;
} // End woo_archive_title()

// MESSIN WITH COMMENT FIELDS AND FORMAT - comment out the filters if not in use

// filters the default set of comment fields along with the basic output of each field  - woo canvas filters this at theme-actions
// rerolled here

function woo_comment_form_fields ( $fields ) {
	$commenter = wp_get_current_commenter();

$req = get_option( 'require_name_email' );
$aria_req = ( $req ? " aria-required='true'" : '' );

	$fields =  array(
	'author' => '<p class="comment-form-author"><input id="author" name="author" type="text" class="txt" tabindex="1" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' />' .
				'<label for="author">' . __( 'Name', 'woothemes' ) . ( $req ? ' <span class="required">' . __( '*', 'woothemes' ) . '</span>' : '' ) . '</label> ' . '</p>',
	'email'  => '<p class="comment-form-email"><input id="email" name="email" type="text" class="txt" tabindex="2" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /> ' .
				'<label for="email">' . __( 'Email', 'woothemes' ) . ( $req ? ' <span class="required">' . __( '*', 'woothemes' ) . '</span>' : '' ) . '</label> ' . '</p>',
	'url'    => '<p class="comment-form-url"><input id="url" name="url" type="text" class="txt" tabindex="3" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" />' .
	            '<label for="url">' . __( 'Website', 'woothemes' ) . '</label></p>',
);
	return $fields;
} // End woo_comment_form_fields()

add_filter( 'comment_form_default_fields', 'woo_comment_form_fields', 10 );


// filters the output of each field after the filter above is done - includes lots of additional labels - woo canvas filters this at theme-actions
// [must_log_in]
// [logged_in_as]
//  [comment_notes_before]
// [comment_notes_after]
//  [id_form
// [id_submit]
//  [title_reply]
// [title_reply_to]
// [cancel_reply_link]
//  [label_submit]
function woo_comment_form_args ( $args ) {
//print_r('<pre style="padding: 10px; border: 1px solid #000; margin: 10px">'); print_r($args ); print_r('</pre>');
	// Add tabindex of "field count + 1" to the comment textarea. This lets us cater for additional fields and have a dynamic tab index.
	$tabindex = count( $args['fields'] ) + 1;
//	$args['comment_field'] = str_replace( '<textarea ', '<textarea tabindex="' . $tabindex . '" ', $args['comment_field'] );

	// Adjust tabindex for "submit" button.
	$tabindex++;

	$args['label_submit'] = __( 'Post Comment', 'woothemes' );
	$args['comment_notes_before'] = 'Your email address will not be published. Required fields are marked *';
	$args['comment_notes_after'] = '';
	$args['cancel_reply_link'] = __( 'Click here to cancel reply.', 'woothemes' );

	return $args;
} // End woo_comment_form_args()

add_filter( 'comment_form_defaults', 'woo_comment_form_args', 10 );

// add a clear both under the stinking submit button
function arrgghh_comment_form_bottom() {
	echo '<div class="fix"></div>';
}
add_action('comment_form','arrgghh_comment_form_bottom');

// change output of one field
// - woo canvas filters this at theme-actions
function woo_comment_form_comment ( $field ) {
	//$field = str_replace( '<label ', '<label class="hide" ', $field );
	//$field = str_replace( 'cols="45"', 'cols="50"', $field );
	$field = str_replace( 'rows="8"', 'rows="10"', $field );
	return $field;
} // End woo_comment_form_comment()
add_filter( 'comment_form_field_comment', 'woo_comment_form_comment', 10 );



// MESSIN WITH POST-PREV AND POST-NEXT LINKS
// Mess with Post Previous on single post bottom links. Usually these have the title in them, we just want our own label there
function change_my_previous_post_link($link){
	if('mbt_book' == get_post_type()){
		$contents = '&laquo; Previous Book';
	} else { 
		$contents = '&laquo; Previous Blog Post';
	}
	$str = preg_replace('#(<a.*?>).*?(</a>)#', '$1'.$contents.'$2', $link);
	return $str;
}
add_filter( 'previous_post_link', 'change_my_previous_post_link' ); 


// Mess with Post next on single post. Usually these have the title in them, we just want our own label there
function change_my_next_post_link($link){
	if('mbt_book' == get_post_type()){
		$contents = 'Next Book &raquo;';
	} elseif('post' == get_post_type()) { 
		$contents = 'Next Blog Post &raquo;';
	}
	$str = preg_replace('#(<a.*?>).*?(</a>)#', '$1'.$contents.'$2', $link);
	return $str;
}
add_filter( 'next_post_link', 'change_my_next_post_link' );


////////////////////////////                                                      ////////////////////////////                              
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////    END COMMON       ///////////////////////////////////////////
///////////////////////////	STUFF BELOW THIS LINE IS ALL SPECIFIC TO THE SITE //////////////////////////////
// CHECK IT CAREFULLY SO YOU ARE NOT LOADING FUNCTIONS REQUIRED BY THE PREVIOUS WEBSITE RATHER THAN THIS ONE
////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////                                                      ////////////////////////////

// Full Width Tweaks
///////////////////////
//     HEADER        //
///////////////////////


// SITE TITLE

function add_site_title() {
	global $woo_options;
	
	 $site_title = ( isset($woo_options['woo_second_title']) && !empty($woo_options['woo_second_title']) ? $woo_options['woo_second_title'] : get_bloginfo( 'name' ));
	 $site_description = ( isset($woo_options['woo_second_subtitle']) && !empty($woo_options['woo_second_subtitle']) ? $woo_options['woo_second_subtitle'] : get_bloginfo( 'description' ));
	 $site_url = home_url( '/' );

	 ?>   
<div id="logo">
	<?php
		// Website heading/logo and description text.
		if ( isset($woo_options['woo_logo_show']) && $woo_options['woo_logo_show'] == "true" && isset($woo_options['woo_logo']) && $woo_options['woo_logo'] ) {
			echo '<img src="' . $woo_options['woo_logo'] . '" alt="' . $site_title . '" />' . "\n";
		} // End IF logo
		// then the site name
		if ( isset($woo_options['woo_sitename_show']) && $woo_options['woo_sitename_show']=="true") { // if use sitename is clicked, 
			if ( isset($site_title) && !empty($site_title) )	{ // if site title
				echo '<h1 class="sitetitle"><a href="/">' . $site_title . '</a></h1>' . "\n";
			} // end if site title
		}
		
		// then the site Description
		if ( isset($woo_options['woo_sitedescription_show']) && $woo_options['woo_sitedescription_show']=="true") { // if use sitename is clicked, 
			if ( isset($site_description) && !empty($site_description) )	{ // if site title
				echo '<h2 class="sitedescription">' . $site_description . '</h2>' . "\n";
			} // end if site title
		}		

	?>
</div><!-- /#logo -->	
	  <?php 
}

// change header layout
// determine page layout - this will override some theme options like socmed placement
function site_layout(){
	global $woo_options;
	remove_action( 'woo_header_inside', 'woo_logo', 10 ); // get rid of lame woo_logo function
	add_action('woo_header_inside', 'add_site_title', 50);  // hook in more versatile logo function
	add_action( 'woo_header_inside', 'woo_add_socmed_to_banner' ,40 );
	add_action( 'woo_header_inside', 'add_nav_search', 50 );
	add_action('woo_head','liketweet_position');
	remove_action( 'woo_header_after','woo_nav', 10 );
	// hook menu to woo_header_before(); 
	add_action( 'woo_header_before','woo_nav', 10 );
	
}
add_action('wp_head','site_layout',10);

// additional home title addon
function woo_options_add_secondtitle($options){
	$shortname = "woo";	   
	// More stuff
	$options[] = array( "name" => __( 'Secondary Site Title', 'woothemes' ),
						"icon" => "layout",
						"type" => "heading"); 

	$options[] = array( "name" => __( 'Secondary Site Title', 'woothemes' ),
						"desc" => __( 'If you want to have a site title show in the homepage different than the one that shows in searches, add it here', 'woothemes' ),
						"id" => $shortname."_second_title",
						"std" => "",
						"type" => "text");
						
	$options[] = array( "name" => __( 'Secondary Site Subtitle', 'woothemes' ),
						"desc" => __( 'If you want to have a site subtitle show in the homepage different than the one that shows in searches, add it here', 'woothemes' ),
						"id" => $shortname."_second_subtitle",
						"std" => "",
						"type" => "text");											
	return $options;
}
add_filter('yet_more_woo_options','woo_options_add_secondtitle',10);

///////////////////////
//     HOME PAGE     //
///////////////////////


// Vanilla Widgets for the home page

// extra cta box
function villa_trip_advisor_reviews(){
	global $woo_options;
	$output = '<div id="trip_advisor_reviews">';
	$output .= cmg_trip_review_feed('homewidget');
	$output .= '</div>';
	echo $output;
}

function villa_home_blog_intro($heading=true){
	global $woo_options;
	$output = '<div id="home_blogintro">';
		// Heading
		if( ( isset( $woo_options['woo_homeblogintro_heading']) && !empty($woo_options['woo_homeblogintro_heading']) ) && $heading==true  ){
			$output .= '<h2 class="heading">'.$woo_options['woo_homeblogintro_heading'].'</h2>';
		}
		// intro
		if(isset($woo_options['woo_homeblogintro_text']) && !empty($woo_options['woo_homeblogintro_text']) ){
			$output .= '<div class="heading">'.$woo_options['woo_homeblogintro_text'].'</div>';
		}
		
		// category label
		/*
		$catlabel=(isset($woo_options['woo_homeblogintro_catlabel']) && !empty($woo_options['woo_homeblogintro_catlabel']) ? $woo_options['woo_homeblogintro_catlabel'] : 'Blog Categories ');
		$output .= '<div class="catbox">';
			$output .= '<span class="catlabel">'.$catlabel.'</span>';
			$output .= '<ul class="blogcats">'.wp_list_categories('orderby=id&hide_empty=0&exclude=1&title_li=&echo=0').'</ul>';
		$output .= '</div>';
		*/
		echo '<div class="fix"></div>';
	$output .= '</div>';
	echo $output;
}

// Theme Options 

// additional home widgets
function woo_options_add_homewidgets($options){
	$shortname = "woo";
			   
	$options[] = array( "name" => __( 'Home Page and Widget Options', 'woothemes' ),
						"icon" => "layout",
						"type" => "heading"); 
												
// Homepage Blog Intro

	$options[] = array( "name" => __( 'Home Blog Intro', 'woothemes' ),
						"type" => "subheading");
			
	$options[] = array( "name" => __( 'Home Blog Intro Heading', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_homeblogintro_heading",
						"std" => "",
						"type" => "text");		
						
	$options[] = array( "name" => __( 'Home Blog Intro Text', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_homeblogintro_text",
						"std" => "",
						"type" => "text");	
						
	$options[] = array( "name" => __( 'Home Blog Intro Category Label', 'woothemes' ),
						"desc" => __( '', 'woothemes' ),
						"id" => $shortname."_homeblogintro_catlabel",
						"std" => "",
						"type" => "text");					
	return $options;
}
add_filter('yet_more_woo_options','woo_options_add_homewidgets',25);

///////////////////////
//    INNER PAGES    //
///////////////////////
// Silly RSS Badge thing
function get_rss_thing(){
	global $woo_options;
	$url = ( isset($woo_options['woo_feed_url']) & !empty($woo_options['woo_feed_url']) ? $woo_options['woo_feed_url'] : '' );
	$imgsrc = get_stylesheet_directory_uri().'/images/rssbadge.png';
	$rss = '<div class="rssbadge"><a href="'.$url.'"><img src="'.$imgsrc.'" /></a></div>';
	return $rss;
}

///////////////////////
//     FOOTER        //
///////////////////////

// sidebar placement
if ( ! function_exists( 'move_footer_widgets' ) ) {
	function move_footer_widgets() {
		remove_action( 'woo_footer_top', 'woo_footer_sidebars', 30 );
		add_action( 'woo_footer_inside', 'woo_footer_sidebars', 10 );
	}
}
add_action( 'woo_head', 'move_footer_widgets', 10);


// Footer top button
function footer_top_button(){
	$output = '<div id="footer-top-button">';
	$output .= '<a href="#top"><img src="'.get_stylesheet_directory_uri().'/images/foot-top.png" /></a>';
	$output .= '</div>';
	echo $output;
}
add_action('woo_footer_inside','footer_top_button');

// control the copyright line
// Default is &copy; Sitename All Rights Reserved. -- eg -- " � 2014 Villa des Parfums, Grasse. All Rights Reserved "
// change behavior with filter below
function change_copyright_line($cright){
	global $woo_options;
	// swap out with the themeopts footer left text - default
	//$cright = $woo_options['woo_footer_left_text'];
	// alternative custom text  - great if you need to add variables
	$cright = 'Copyright &copy; '.date('Y').' '.get_bloginfo( 'name' ).'. &nbsp; All Rights Reserved. &nbsp; Site by <a target="_blank" href="http://www.castlemediagroup.com"> Castle Media Group</a>';
	return $cright;
}
add_filter( 'woo_shortcode_site_copyright','change_copyright_line');

///////////////////////
//     PLUGINs       //
///////////////////////


?>