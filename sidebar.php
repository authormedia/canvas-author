<?php
/**
* Sidebar Template
*
* If a `primary` widget area is active and has widgets, display the sidebar.
*
* @package WooFramework
* @subpackage Template
*/
 
global $woo_options;
$settings = array(
                                'layout' => 'two-col-left',
                                'portfolio_layout' => 'one-col'
                                );

$settings = woo_get_dynamic_values( $settings );

// Homepage Sidebar
if ( isset($woo_options['woo_homesidebar_enable']) && $woo_options['woo_homesidebar_enable'] == 'true' && is_front_page() ) {
        $is_homesidebar = 'true';
        $layout == 'one-col';
}

//print_r($is_homesidebar); exit;

$layout = $settings['layout'];
// Cater for custom portfolio gallery layout option.
if ( is_tax( 'portfolio-gallery' ) || is_post_type_archive( 'portfolio' ) ) {
        if ( '' != $settings['portfolio_layout'] ) { $layout = $settings['portfolio_layout']; }
}

if ( 'one-col' != $layout ) {
        if ( woo_active_sidebar( 'primary' ) ) {
                woo_sidebar_before();
                ?>
                <aside id="sidebar">
                <?php
                        woo_sidebar_inside_before();
                        if ($is_homesidebar == 'true' && isset($woo_options['woo_homesidebar_choice']) && $woo_options['woo_homesidebar_choice'] == 'homepage'){
                                woo_sidebar('homepage');
                        } else {
                                woo_sidebar('primary');
                        }
                        woo_sidebar_inside_after();
                ?>
                </aside><!-- /#sidebar -->
                <?php
                woo_sidebar_after();
        }
}
?>