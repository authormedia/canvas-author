<?php
/**********************************************************/
/*  Additional WooThemes FUNCTIONS                        */
/**********************************************************/

// For many themes, having the woo feedback (testimonials) and portfolio need to be disabled.

// replace the add feedback function that loads in Canvas functions with this one that does nothing. Since the one in Canvas only loads if the function does not already exist, this one loads first and kills the function by returning NULL.
function woo_add_feedback(){
	return NULL;
}

// knock out portfolio too

function woo_add_portfolio(){
	return NULL;
}

// no custom woo styling
//function woo_custom_styling(){
//	return NULL;
//}

// no slides - for now at least
//function woo_add_slides() {
//	return NULL;
//}

// remove the woo framework popup notices - for non supers
function no_woo_framework_notices(){
	//if ( !is_super_admin()){
	remove_action( 'wooframework_container_inside', 'wooframework_add_static_front_page_banner' );
	remove_action( 'wooframework_container_inside', 'wooframework_add_woodojo_banner' );
	remove_action( 'wooframework_container_inside', 'wooframework_add_wooseosbm_banner' );
	remove_action( 'wooframework_wooframeworksettings_container_inside', 'wooframework_add_wooseosbm_banner' );

	//}
}
//add_action('admin_head','no_woo_framework_notices');

// remove breadcrumbs sometimes
add_action( 'wp_head', 'control_woo_breadcrumbs' );
function control_woo_breadcrumbs() {
global $woo_options;
	if ( isset( $woo_options['woo_breadcrumbs_show'] ) && $woo_options['woo_breadcrumbs_show'] == 'true' ) {
		// remove if this is the homwpage
		if( (is_home() || is_front_page()) ){
			remove_action( 'woo_loop_before', 'woo_breadcrumbs', 10 );	
		}
	} // End IF Statement 
}

// don't show the rss link
function woo_nav_subscribe() {
	return null;
	}
	
// dont show credits in footer
function woo_shortcode_site_credit() {
	return null;
	}

/*--------------------------------------------*/
/*             Yoast SEO Stuff                */
/*--------------------------------------------*/

/* filter to change where yoast gets the google plus account info for rel=author*/
add_filter('wpseo_author_link','change_rel_author');
function change_rel_author($gplus){
	global $woo_options;
	
	//print_r($mygplus); print_r('<br>'); print_r($mygplus); 	
	//exit;
	// the gplus account stored in the woo theme options
	$woo_gplus = $woo_options['woo_connect_googleplus'];
	// $gplus -- the default gplus account setting to use is the one on the user profile page for the user set in the WP SEO settings.
	// if the  woo themeoptions gplus account is set up, then use it, otherwise if the gplus account is in the in yoast and profile, use it
	
	if (isset($woo_gplus) && !empty($woo_gplus) ) {
			$mygplus = $woo_gplus;
		} elseif (isset($gplus) && !empty($gplus) ) {
			$mygplus = $gplus;
		}
	 
	if (isset($mygplus) && !empty($mygplus) ) {
		return $mygplus; 
	} else {
		return false;
	}
}
/*--------------------------------------------*/
/*                                            */
/*             Utility Functions              */
/*                                            */
/*--------------------------------------------*/

if (  is_user_logged_in() && current_user_can('manage_options') ) {
add_action('wp_footer', 'show_template');
}
 
// change gravity forms email address
function set_forms_email() {
	global $woo_options;
	if ( isset($woo_options['woo_contactform_email']) && !empty($woo_options['woo_contactform_email']) ) {
		$email_address = $woo_options['woo_contactform_email']; 
	}
//	if ($_GET['iam'] == 'testing'){
//		print_r($email_address);
//	}
}
//add_filter("gform_notification_email", "set_forms_email", 10, 2);

//add_action('wp_footer','set_forms_email');

function show_template() {
    global $woo_options, $template;
	print_r('<pre style="background-color: white; padding: 5px;">');
    print_r($template);
	print_r('<br />'.set_forms_email());
	print_r('</pre>');
}

// add additional upload types
add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {
	// add the file extension to the array
	$existing_mimes['ico'] = 'mime/type';
    // call the modified list of extensions
	return $existing_mimes;
}

// Return info about actions attacthed to any hook
function what_hooks_here($hook, $return='number'){
	global $wp_filter;
	//print_r($wp_filter); exit;
	$actions = $wp_filter[$hook];
	if(is_array($actions)){
		$count = count($actions);
		if ($return=='number'){
			return $count;
		}else{
			return $actions;
		}
	}
	return false;
}


/* Hyper controlled excerpt and title and excerpt lengths - measures number of chars */
function ttruncat($text,$numb) {
    if (strlen($text) > $numb) {
        $text = substr($text, 0, $numb);
        $text = substr($text,0,strrpos($text," "));
        $etc = "&hellip;";
        $text = $text.$etc;
    }
    return $text;
}

// Add change body class to make full width -- remove two-col-left  -- add  one-col 
function kill_sidebar($classes) {
	global $woo_options;
	// list the classes to omit
	$blacklist = array( 'two-col-left', 'two-col-right', 'two-col-left-980' );
	$classes = array_diff( $classes, $blacklist );
	// add 'class-name' to the $classes array
	$classes[] = 'one-col ';
	// return the $classes array
	return $classes;
}

// Image Finding functions, used mainly for sites that do not user feature images.

// Grab first image attached to a post - rerolled from woo_image

function get_first_attached_image($id,$imgs=1){
	global $woo_options;
	$attachments = get_children( array(	
		'post_parent' => $id,
		'numberposts' => $imgs,
		'post_type' => 'attachment',
		'post_mime_type' => 'image',
		'order' => 'DESC',
		'orderby' => 'menu_order date')
	);

	// Search for and get the post attachment
	if ( ! empty( $attachments ) ) {
		foreach ( $attachments as $att_id => $attachment ) {
				$width = null;
				$height = null;

			if ( get_option( 'woo_post_image_support' ) == 'true' && get_option( 'woo_pis_resize' ) == 'true' ) {
				// Dynamically resize the post thumbnail
				$vt_crop = get_option( 'woo_pis_hard_crop' );
				if ( $vt_crop == 'true' ) $vt_crop = true; else $vt_crop = false;
				$vt_image = vt_resize( $att_id, '', $width, $height, $vt_crop );
				
				// Set fields for output
				$custom_field = esc_url( $vt_image['url'] );
				$width = $vt_image['width'];
				$height = $vt_image['height'];
			} else {
				$src = wp_get_attachment_image_src( $att_id, 'large', true );
				$custom_field = esc_url( $src[0] );
				$attachment_id[] = $att_id;
				$src_arr[] = $custom_field;
			}
			$thumb_id = $att_id;
			
		}
	return $custom_field;
	//print_r($src); exit;
	}
}



// Grab first image displayed in a post - rerolled from woo_image
// this method is more clunky codewise but more reliable for getting the first image that ACTUALLY appears in the_content

function get_first_displayed_image($id){
	$first_img = '';
	$post = get_post( $id );
	ob_start();
	ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches [1] [0];	
 // print_r($first_img); exit;
	if(empty($first_img)){ //Defines a default image
		$first_img = "/images/default.jpg";
	}
	return $first_img;

}

/*------------------------------------------*/
/* Clear off the widgets that 
no one needs to see                         */
/*------------------------------------------*/
//	 
add_action( 'widgets_init', 'my_unregister_some_widgets', 20 );
function my_unregister_some_widgets() {	
	// Default WP Widgets
//	unregister_widget('WP_Widget_Pages');
//	unregister_widget('WP_Widget_Calendar');
//	unregister_widget('WP_Widget_Archives');
	unregister_widget('WP_Widget_Links');
//	unregister_widget('WP_Widget_Meta');
	unregister_widget('WP_Widget_Search');
//	unregister_widget('WP_Widget_Text');
//	unregister_widget('WP_Widget_Categories');
//	unregister_widget('WP_Widget_Recent_Posts');
//	unregister_widget('WP_Widget_Recent_Comments');
//	unregister_widget('WP_Widget_RSS');
//	unregister_widget('WP_Widget_Tag_Cloud');
//	unregister_widget('WP_Nav_Menu_Widget');
//	unregister_widget('Business_Contact_Info'); // contact info
//	unregister_widget('Tribe_Image_Widget');   // shows special offers
	
	unregister_widget('Akismet_Widget');
//	unregister_widget('WP_Widget_Black_Studio_TinyMCE');
	unregister_widget('NewsletterSignUpWidget');
	
	// WOO Widgets
//	unregister_widget('Woo_Widget_AdSpace');
//	unregister_widget('Woo_Widget_BlogAuthorInfo');
//	unregister_widget('Woo_Widget_Embed');  // embed a viedo feed
	unregister_widget('Woo_Widget_Feedback');  // show feedback / testimonials.. if it works
	unregister_widget('Woo_Widget_Flickr');
//	unregister_widget('Woo_Widget_Search');
	unregister_widget('Woo_Widget_Subscribe');
//	unregister_widget('Woo_Widget_WooTabs');
//    unregister_widget('Woo_Widget_Twitter');            
}

include(STYLESHEETPATH . '/includes/site-specific.php');

// Include other functions
//include(STYLESHEETPATH . '/includes/castle_widgets.php');
include(STYLESHEETPATH . '/includes/membership_functions.php'); // support for filtering site display according to site level
include(STYLESHEETPATH . '/includes/template-sections.php');
include(STYLESHEETPATH . '/includes/extend_theme_options.php');
include(STYLESHEETPATH . '/includes/dynamic_styles.php');
//include(STYLESHEETPATH . '/includes/widget-subscribe.php');
include(STYLESHEETPATH . '/includes/social-media-profile-widget.php');
//include(STYLESHEETPATH . '/includes/widget-mbt-featured-books.php');

// load helper functions if MyBookTable is active
if( function_exists('mbt_init')){ 
	include(STYLESHEETPATH . '/includes/book_table_extras.php');
}

// Mailchimp Signup API integration and widgets
include(STYLESHEETPATH . '/includes/mailchimp_signup/castle_canvas_mailchimp_signup.php');

/*****************************/
/*     Woo Slider            */
/*****************************/

// remove canvas sliders completely
function woo_slider() {
	return false;
}

function woo_slider_magazine() {
	return false;
}

function woo_slider_biz() {
	return false;
}

function woo_add_slides() {
	return NULL;
}

// load the helper functions if wooslider plugin is active
if (class_exists('WooSlider')) {
	include(STYLESHEETPATH . '/includes/woo-slider-extras.php');
}

include(STYLESHEETPATH . '/includes/castle-vanilla-widget.php');
include(STYLESHEETPATH . '/includes/post-extras.php'); 

?>